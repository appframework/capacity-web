# Capacity planning tools
The Capacity planning tools provide a simple web UI to for organizing the work
for the employees in a large software development project or company.

## Supported and tested containers

These tools rely on a LTS version of Java. As of now this is Java 21 and relies
on Quarkus 3 LTS. Therefore it can run in a container like Docker as well.

## Supported Databases

The project is using hibernate for data access. This means that theoretically
every database supported by hibernate is usable. But as of now only PostgreSQL
14 and 16 are used for development an testing.

### Setting up your PostgreSQL database

    createuser -l -P -R -S -D --interactive capacity
    createdb -O capacity capacity

### Creating a favicon.ico file for the browser

    inkscape -w 16 -h 16 -o 16.png master.svg
    inkscape -w 32 -h 32 -o 32.png master.svg
    inkscape -w 48 -h 48 -o 48.png master.svg
    convert 16.png 32.png 48.png favicon.ico