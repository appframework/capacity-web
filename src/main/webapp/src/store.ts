import { createGlobalState, useStorage } from '@vueuse/core'
import type { Label } from './appframework'

export const appState = createGlobalState(() => {
  return useStorage(
    'state-capacity',
    {
      user: {
        username: '',
        authdata: ''
      },
      permissions: [] as string[],
      locale: navigator.language.split('-')[0],
      exportconfig: {
        abilitiesX: [] as Label[],
        abilitiesY: [{ index: 1, name: '', abilities: [] as Label[] }]
      }
    },
    localStorage,
    { mergeDefaults: true }
  )
})

export const allPermissions = createGlobalState(async () => {
  const state = appState()
  const response = await fetch('/rest/permissions', {
    headers: {
      Authorization: 'Basic ' + state.value.user.authdata
    }
  })
  return (await response.json()) as string[]
})

export const isLoggedIn = function () {
  const state = appState()
  return !!state.value.user.username
}
