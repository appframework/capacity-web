/* tslint:disable */
/* eslint-disable */

export interface AbstractDto {
    id: number;
}

export interface BinaryData extends AbstractDto {
    filename: string;
    size: number;
    contentType: string;
}

export interface Credentials {
    username: string;
    password: string;
}

export interface DeletionAction {
    effect: string;
    action: string;
    affected: number;
}

export interface ImportResult {
    skipped: number;
    created: number;
    updated: number;
    deleted: number;
}

export interface Problem {
    title: string;
    status: number;
    detail: string;
}

export interface Progress<T> {
    key: string;
    result: T;
    value: number;
    max: number;
    message: string;
    completed: boolean;
    success: boolean;
}

export interface Role extends AbstractDto {
    name: string;
    permissions: string[];
}

export interface User extends AbstractDto {
    name: string;
    login: string;
    password: string;
    email: string;
    /**
     * @deprecated
     */
    roleIds: number[];
    roles: string[];
    pictureId: number;
}

export interface VersionInformation {
    maven: string;
    git: string;
    buildTimestamp: Date;
}

export interface Absence extends AbstractDto, Event {
    employeeId: number;
    start: Date;
    end: Date;
    reason: string;
    externalId: string;
    imported: boolean;
}

export interface Column {
    index: number;
    label: string;
}

export interface Contract {
    start: Date;
    end: Date;
    vacationDaysPerYear: number;
    workingHours: WorkingHours[];
    workingHoursPerWeek: number;
}

export interface Employee extends AbstractDto {
    name: string;
    email: string;
    abilities: Label[];
    contract: Contract;
    locationId: number;
    color: string;
    velocity: number;
}

export interface EmployeeEpisode extends AbstractDto {
    employeeId: number;
    velocity: number;
}

export interface Episode extends AbstractDto {
    start: Date;
    end: Date;
    name: string;
    employeeEpisodes: EmployeeEpisode[];
}

export interface Event {
}

export interface ExportConfiguration {
    abilitiesX: string[];
    abilitiesY: { [index: string]: string[] };
}

export interface Holiday extends AbstractDto, Event {
    date: Date;
    name: string;
    hoursReduction: number;
    locationIds: number[];
    externalId: string;
    icalImportId: number;
}

export interface IcalImport extends AbstractDto {
    type: Type;
    name: string;
    url: string;
    lastImported: Date;
    auth: Auth;
    username: string;
    password: string;
}

export interface ImportConfiguration {
    columns: Column[];
    configuration: string;
    identifierColumn: number;
    identifierType: string;
    sheet: number;
}

export interface Label extends AbstractDto {
    name: string;
    color: string;
    alias: string[];
    precedence: boolean;
}

export interface Location extends AbstractDto {
    name: string;
}

export interface Reference {
    poolId: number;
    referenceKey: string;
}

export interface ReferencePool extends AbstractDto {
    name: string;
    templateUrl: string;
}

export interface RequiredAbility extends AbstractDto {
    percentage: number;
    skills: Label[];
}

export interface Requirement extends AbstractDto {
    name: string;
    clusterId: number;
    priority: Priority;
    references: Reference[];
    effort: number;
    remaining: number;
    requiredAbilities: RequiredAbility[];
    desiredStart: Date;
    due: Date;
}

export interface RequirementCluster extends AbstractDto {
    name: string;
    references: Reference[];
}

export interface Widget extends AbstractDto {
    name: string;
    type: string;
    configuration: string;
}

export interface WorkingHours {
    dayOfWeek: number;
    start: Date;
    end: Date;
}

export interface WorkingHoursDetails {
    date: Date;
    amount: number;
}

export interface WorkingHoursList {
    from: Date;
    until: Date;
    amount: number;
    details: WorkingHoursDetails[];
}

export interface WorkingHoursPerEmployee {
    employee: Employee;
    workingHours: WorkingHoursList;
    velocity: number;
    abilities: number[][];
}

export interface WorkingHoursRequest {
    useVelocity: boolean;
    episodeId: number;
    start: Date;
    end: Date;
    filter: number[][];
    units: Unit;
}

export type Priority = "LOW" | "MED_LOW" | "MEDIUM" | "MED_HIGH" | "HIGH";

export type Type = "HOLIDAYS" | "ABSENCES";

export type Auth = "NONE" | "BASIC";

export type Unit = "hours" | "days" | "weeks";
