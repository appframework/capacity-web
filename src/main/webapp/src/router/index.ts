import { createRouter, createWebHistory } from 'vue-router'
import { search } from '@/App.vue'
import { isLoggedIn } from '../store'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('../views/HomeView.vue')
    },
    {
      path: '/home/edit',
      component: () => import('../views/HomeEditView.vue')
    },
    {
      path: '/login',
      component: () => import('../appframework/views/LoginView.vue')
    },
    {
      path: '/logout',
      component: () => import('../appframework/views/LogoutView.vue')
    },
    {
      path: '/roles',
      component: () => import('../appframework/views/RolesView.vue')
    },
    {
      path: '/roles/:id',
      component: () => import('../appframework/views/RoleView.vue')
    },
    {
      path: '/roles/clone/:id',
      component: () => import('../appframework/views/RoleView.vue')
    },
    {
      path: '/users',
      component: () => import('../appframework/views/UsersView.vue')
    },
    {
      path: '/users/:id',
      component: () => import('../appframework/views/UserView.vue')
    },
    {
      path: '/users/clone/:id',
      component: () => import('../appframework/views/UserView.vue')
    },
    {
      path: '/profile',
      component: () => import('../appframework/views/ProfileView.vue')
    },
    {
      path: '/employees',
      component: () => import('../views/EmployeesView.vue')
    },
    {
      path: '/employees/export',
      component: () => import('../views/EmployeeExportView.vue')
    },
    {
      path: '/employees/upload',
      component: () => import('../views/EmployeeUploadView.vue')
    },
    {
      path: '/employees/:id',
      component: () => import('../views/EmployeeView.vue')
    },
    {
      path: '/employees/clone/:id',
      component: () => import('../views/EmployeeView.vue')
    },
    {
      path: '/labels',
      component: () => import('../views/LabelsView.vue')
    },
    {
      path: '/labels/:id',
      component: () => import('../views/LabelView.vue')
    },
    {
      path: '/labels/clone/:id',
      component: () => import('../views/LabelView.vue')
    },
    {
      path: '/locations',
      component: () => import('../views/LocationsView.vue')
    },
    {
      path: '/locations/:id',
      component: () => import('../views/LocationView.vue')
    },
    {
      path: '/locations/clone/:id',
      component: () => import('../views/LocationView.vue')
    },
    {
      path: '/episodes',
      component: () => import('../views/EpisodesView.vue')
    },
    {
      path: '/episodes/:id',
      component: () => import('../views/EpisodeView.vue')
    },
    {
      path: '/episodes/clone/:id',
      component: () => import('../views/EpisodeView.vue')
    },
    {
      path: '/referencepools',
      component: () => import('../views/ReferencePoolsView.vue')
    },
    {
      path: '/referencepools/:id',
      component: () => import('../views/ReferencePoolView.vue')
    },
    {
      path: '/referencepools/clone/:id',
      component: () => import('../views/ReferencePoolView.vue')
    },
    {
      path: '/requirementclusters',
      component: () => import('../views/RequirementClustersView.vue')
    },
    {
      path: '/requirementclusters/:id',
      component: () => import('../views/RequirementClusterView.vue')
    },
    {
      path: '/requirementclusters/clone/:id',
      component: () => import('../views/RequirementClusterView.vue')
    },
    {
      path: '/requirements',
      component: () => import('../views/RequirementsView.vue')
    },
    {
      path: '/requirements/upload',
      component: () => import('../views/RequirementUploadView.vue')
    },
    {
      path: '/requirements/:id',
      component: () => import('../views/RequirementView.vue')
    },
    {
      path: '/requirements/clone/:id',
      component: () => import('../views/RequirementView.vue')
    },
    {
      path: '/ical_imports',
      component: () => import('../views/IcalImportsView.vue')
    },
    {
      path: '/ical_imports/:id',
      component: () => import('../views/IcalImportView.vue')
    },
    {
      path: '/ical_imports/clone/:id',
      component: () => import('../views/IcalImportView.vue')
    },
    {
      path: '/absence',
      component: () => import('../views/AbsenceView.vue')
    },
    {
      path: '/capacity',
      component: () => import('../views/CapacityView.vue')
    },
    {
      path: '/planning',
      component: () => import('../views/PlanningView.vue')
    },
    {
      path: '/calendar',
      component: () => import('../views/CalendarView.vue')
    }
  ]
})

router.beforeEach((to, from) => {
  // Not logged in, go to login page
  if (!isLoggedIn() && to.path !== '/login') {
    return { path: '/login' }
  }

  // Clear search between modules
  if (to.path === from.path) {
    return
  }
  const currentModule = from.path.split('/')[1]
  const nextModule = to.path.split('/')[1]
  if (currentModule !== nextModule) {
    search.value = ''
  }
  return true
})

export default router
