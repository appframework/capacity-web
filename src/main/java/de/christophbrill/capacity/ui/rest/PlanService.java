package de.christophbrill.capacity.ui.rest;

import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.appframework.ui.rest.AbstractService;
import de.christophbrill.capacity.ui.dto.Requirement;
import de.christophbrill.capacity.ui.dto.WorkingHoursPerEmployee;
import de.christophbrill.capacity.ui.dto.WorkingHoursRequest;
import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import java.util.List;

@Path("/plan")
public class PlanService extends AbstractService {

    @Inject
    CapacityService capacityService;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<WorkingHoursPerEmployee> plan(Requirement requirement) {

        if (requirement == null || requirement.requiredAbilities == null) {
            throw new BadArgumentException("Requirement with abilities needed");
        }

        WorkingHoursRequest workingHoursRequest = new WorkingHoursRequest();
        workingHoursRequest.useVelocity = true;
        workingHoursRequest.units = WorkingHoursRequest.Unit.days;
        workingHoursRequest.filter = requirement.requiredAbilities
                .stream()
                .map(x -> x.skills.stream().map(y -> y.id).toList())
                .toList();
        return capacityService.getWorkingHours(workingHoursRequest);

    }
}
