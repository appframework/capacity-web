package de.christophbrill.capacity.ui.rest;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.appframework.ui.exceptions.NotAuthorizedException;
import de.christophbrill.appframework.ui.rest.AbstractService;
import de.christophbrill.capacity.persistence.model.EmployeeEntity;
import de.christophbrill.capacity.persistence.model.Permission;
import de.christophbrill.capacity.persistence.model.WidgetEntity;
import de.christophbrill.capacity.persistence.selector.EmployeeSelector;
import de.christophbrill.capacity.persistence.selector.WidgetSelector;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Response;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Path("/abilities")
public class AbilityService extends AbstractService {

	private static final int PADDING = 10;
	private static final int COLUMN_WIDTH = 250;
	private static final int ROW_HEIGHT = 70;

	private static final int MAX_COLUMNS = 4;

	private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

	@GET
	@Path("/export/{widgetId}.svg")
	@Produces("image/svg+xml")
	public Response exportSvg(@PathParam("widgetId") Long widgetId) {

		if (!identity.hasRole(Permission.SHOW_EMPLOYEES.name())) {
			throw new NotAuthorizedException();
		}

		EmployeeSelector employeeSelector = new EmployeeSelector(em);
		LocalDate date = LocalDate.now();
		employeeSelector.withActiveContract(date, date);

		WidgetEntity widget = new WidgetSelector(em).withId(widgetId).find();
		if (widget == null) {
			throw new BadArgumentException("Widget for " + widgetId + " not found");
		}

		JsonNode configuration;
		try {
			configuration = OBJECT_MAPPER.readTree(widget.configuration);
		} catch (IOException e) {
			return Response.serverError().entity(e.getMessage()).build();
		}

		String color = configuration.get("color").asText();
		if (color.length() == 6) {
			color = "#" + color;
		}
		String ability = configuration.get("ability").asText();

		StringBuilder builder = new StringBuilder();
		int maxRows = determineMaxRows(configuration, employeeSelector, ability);
		builder.append("<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"").append(columnsToWidth(MAX_COLUMNS) + (2 * PADDING)).append("\" height=\"").append(rowsToHeight(maxRows, maxRows) + (2 * PADDING)).append("\" viewBox=\"0 0 ").append(columnsToWidth(MAX_COLUMNS) + (2 * PADDING)).append(" ").append(rowsToHeight(maxRows, maxRows) + (2 * PADDING)).append("\">");
		builder.append("<defs />\n");
		builder.append("<rect style=\"opacity:1;fill:none;stroke:");
		builder.append(color);
		builder.append(";stroke-width:2;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4\" width=\"").append(columnsToWidth(4) + 16).append("\" height=\"").append(rowsToHeight(maxRows, maxRows) + 16).append("\" x=\"2\" y=\"2\" ry=\"1.4285716\" rx=\"1.4285713\" />\n");

		renderEmployeeBox(builder, 0.3f, color, 1, 1, 0, 0, maxRows, employeeSelector, ability, configuration.get("topleft").asText());
		renderEmployeeBox(builder, 0.4f, color, 1, 2, 0, 1, maxRows, employeeSelector, ability, configuration.get("topmiddle").asText());
		renderEmployeeBox(builder, 0.4f, color, 1, 1, 0, 3, maxRows, employeeSelector, ability, configuration.get("topright").asText());

		renderEmployeeBox(builder, 0.1f, color, maxRows - 2, 1, 1, 0, maxRows, employeeSelector, ability, configuration.get("middleleft").asText());
		renderEmployeeBox(builder, 0.2f, color, maxRows - 2, 3, 1, 1, maxRows, employeeSelector, ability, configuration.get("middleright").asText());

		renderEmployeeBox(builder, 0.3f, color, 1, 4, maxRows - 1, 0, maxRows, employeeSelector, ability, configuration.get("bottom").asText());

		builder.append("</svg>");
		return Response.ok().entity(builder.toString()).build();
	}

	private void renderEmployeeBox(StringBuilder builder, float opacity, String color, int numRows, int numColumns,
								   int row, int column, int maxRows, EmployeeSelector employeeSelector, String ability,
								   String position) {

		// Render the rectangle as background. It's the base color with the given opacity, to create different shades
		// of the same base color
		rect(builder, opacity, color, numRows, numColumns, row, column, maxRows);

		// Then determine the employees per ability
		Map<String, List<EmployeeEntity>> perAbility = new LinkedHashMap<>();
		for (String m : position.split("\\|")) {
			var employees = employeeSelector.withAbilities(Arrays.asList(ability, m)).findAll();
			if (!employees.isEmpty()) {
				perAbility.computeIfAbsent(m, k -> new ArrayList<>())
						.addAll(employees);
			}
		}

		int currentRow = 0;
		int currentColumn = 0;
		for (var entry : perAbility.entrySet()) {
			// If we are starting a new ability, and have enough columns left, start in a new column
			if (currentRow != 0 && entry.getValue().size() <= numRows && (currentColumn + 1) < numColumns) {
				currentRow = 0;
				currentColumn++;
			}
			int colspan = numRows == 1 && perAbility.entrySet().size() == 1 ? (numColumns / entry.getValue().size()) : 1;

			for (var employee : entry.getValue()) {
				text(builder, row + currentRow, column + currentColumn, abbreviate(employee.name), entry.getKey(), colspan, maxRows);
				currentRow++;
				// If the current column is full, go to the next column
				if (currentRow >= numRows) {
					currentRow = 0;
					currentColumn++;
				}
			}
		}
	}

	private String abbreviate(String name) {
		String[] parts = name.split(" ");
		if (parts.length < 3) {
			return name;
		}
		return parts[0] + " " + parts[parts.length - 1];
	}

	private int determineMaxRows(JsonNode configuration, EmployeeSelector employeeSelector, String ability) {
		// Initialize to 3 rows minimum
		int maxRows = 3;

		// We have one column for middleleft
        String[] columns = configuration.get("middleleft").asText().split("\\|");
        int middleLeftRows = 0;
        for (String m : columns) {
            middleLeftRows += employeeSelector.withAbilities(Arrays.asList(ability, m)).count().intValue();
        }
        maxRows = Math.max(maxRows, middleLeftRows);

        // We have 3 columns for middleright
        columns = configuration.get("middleright").asText().split("\\|");
        int middleRightRows = 0;
        for (String m : columns) {
            middleRightRows += employeeSelector.withAbilities(Arrays.asList(ability, m)).count().intValue();
        }
        maxRows = Math.max(maxRows, middleRightRows / 3);

		// One for the header, one for the footer, but at least 5 lines
		return maxRows + 2;
	}

	private int clamp(int value, int min, int max) {
		if (value < min) {
			value = min;
		}
		if (value > max) {
			value = max;
		}
		return value;
	}

	private int rowsToHeight(int rows, int maxRows) {
		rows = clamp(rows, 1, maxRows);
		return rows * ROW_HEIGHT + Math.max(rows - 1, 0) * 6;
	}

	private int columnsToWidth(int columns) {
		columns = clamp(columns, 1, MAX_COLUMNS);
		return columns * COLUMN_WIDTH + Math.max(columns - 1, 0) * 6;
	}

	private int rowToY(int row, int maxRows) {
		row = clamp(row, 0, maxRows - 1);
		return row * ROW_HEIGHT + row * 6 + PADDING;
	}

	private int columnToX(int column) {
		column = clamp(column, 0, MAX_COLUMNS);
		return column * COLUMN_WIDTH + column * 6 + PADDING;
	}

	private void rect(StringBuilder builder, float opacity, String color, int rows, int columns, int row, int column, int maxRows) {
		builder.append("<rect style=\"opacity:");
		builder.append(opacity);
		builder.append(";fill:");
		builder.append(color);
		builder.append(";fill-opacity:1\" width=\"");
		builder.append(columnsToWidth(columns));
		builder.append("\" height=\"");
		builder.append(rowsToHeight(rows, maxRows));
		builder.append("\" x=\"");
		builder.append(columnToX(column));
		builder.append("\" y=\"");
		builder.append(rowToY(row, maxRows));
		builder.append("\" />\n");
	}

	private void text(StringBuilder builder, int row, int column, String headline, String subheadline, int colspan, int maxRows) {
		int top = rowToY(row, maxRows) + (ROW_HEIGHT / 2) - 5 /* - 5 was chosen by fair dice roll */;
		int left = columnToX(column) + ((COLUMN_WIDTH * colspan) / 2) + 2 /* + 2 was is just a random value */;
		builder.append("<text style=\"font-size:22px;line-height:125%;font-family:sans-serif;text-align:center;text-anchor:middle;fill:#000000;fill-opacity:1\" x=\"");
		builder.append(left);
		builder.append("\" y=\"");
		builder.append(top);
		builder.append("\">\n");
		builder.append("<tspan x=\"");
		builder.append(left);
		builder.append("\" y=\"");
		builder.append(top);
		builder.append("\">");
		builder.append(headline);
		builder.append("</tspan>\n");
		builder.append("<tspan x=\"");
		builder.append(left);
		builder.append("\" y=\"");
		builder.append((top + 28));
		builder.append("\" style=\"font-size:22px;font-style:italic\">");
		builder.append(subheadline);
		builder.append("</tspan>\n");
		builder.append("</text>\n");
	}

}
