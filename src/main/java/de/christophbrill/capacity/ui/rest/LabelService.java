package de.christophbrill.capacity.ui.rest;

import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.appframework.ui.rest.AbstractResourceService;
import de.christophbrill.capacity.persistence.model.EmployeeEntity;
import de.christophbrill.capacity.persistence.model.LabelEntity;
import de.christophbrill.capacity.persistence.model.Permission;
import de.christophbrill.capacity.persistence.model.WidgetEntity;
import de.christophbrill.capacity.persistence.selector.EmployeeSelector;
import de.christophbrill.capacity.persistence.selector.LabelSelector;
import de.christophbrill.capacity.ui.dto.Label;
import de.christophbrill.capacity.util.mappers.LabelMapper;
import jakarta.annotation.Nonnull;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Path("/label")
public class LabelService extends AbstractResourceService<Label, LabelEntity> {

    @Override
    protected Class<LabelEntity> getEntityClass() {
        return LabelEntity.class;
    }

    @Override
    protected LabelSelector getSelector() {
        return new LabelSelector(em);
    }

    @Override
    protected LabelMapper getMapper() {
        return LabelMapper.INSTANCE;
    }

    @Override
    protected String getCreatePermission() {
        return Permission.ADMIN_LABELS.name();
    }

    @Override
    protected String getReadPermission() {
        return Permission.SHOW_LABELS.name();
    }

    @Override
    protected String getUpdatePermission() {
        return Permission.ADMIN_LABELS.name();
    }

    @Override
    protected String getDeletePermission() {
        return Permission.ADMIN_LABELS.name();
    }


    @Override
    @Transactional
    public void update(@Nonnull Long id, Label dto) {
        LabelEntity existing = findById(id);
        if (existing == null) {
            super.update(id, dto);
        } else {
            var name = existing.name;
            super.update(id, dto);
            if (!name.equals(dto.name)) {
                // Rename in widget configuration
                for (var widget : WidgetEntity.<WidgetEntity>findAll().list()) {
                    widget.configuration = widget.configuration
                            .replace("\"" + name, "\"" + dto.name)
                            .replace(name + "\"", dto.name + "\"")
                            .replace("|" + name, "|" + dto.name);
                    widget.persist();
                }
            }
        }
    }

    @GET
    @Path("/colors")
    @Produces(MediaType.APPLICATION_JSON)
    public List<String> getColors() {
        return em.createQuery("select distinct color from Label order by color", String.class)
                .getResultList();
    }

    @GET
    @Path("/merge/{from}/into/{to}")
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    public Label merge(@PathParam("from") long fromId, @PathParam("to") long toId) {
        checkUpdatePermission();
        if (fromId == toId) {
            throw new BadArgumentException("'from' and 'to' must be different");
        }

        LabelEntity from = LabelEntity.findById(fromId);
        LabelEntity to = LabelEntity.findById(toId);

        if (to.alias == null) {
            to.alias = new ArrayList<>();
        }
        to.alias.add(from.name);
        if (from.alias != null) {
            for (var alias : from.alias) {
                if (!to.alias.contains(alias)) {
                    to.alias.add(alias);
                }
            }
        }

        List<EmployeeEntity> employees = new EmployeeSelector(em)
                .withAbilities(Collections.singleton(from.name))
                .findAll();
        for (var employee : employees) {
            employee.abilities.remove(from);
            employee.abilities.add(to);
            employee.persist();
        }

        to.persist();

        from.delete();

        return LabelMapper.INSTANCE.mapEntityToDto(to);
    }

}
