package de.christophbrill.capacity.ui.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.christophbrill.appframework.ui.async.ProgressCache;
import de.christophbrill.appframework.ui.async.ProgressEndpoint;
import de.christophbrill.appframework.ui.dto.ImportResult;
import de.christophbrill.appframework.ui.dto.Progress;
import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.appframework.ui.rest.AbstractResourceService;
import de.christophbrill.capacity.persistence.model.ImportProcessEntity;
import de.christophbrill.capacity.persistence.model.Permission;
import de.christophbrill.capacity.persistence.model.RequirementEntity;
import de.christophbrill.capacity.persistence.selector.RequirementSelector;
import de.christophbrill.capacity.ui.dto.ImportConfiguration;
import de.christophbrill.capacity.ui.dto.Requirement;
import de.christophbrill.capacity.util.XlsxImportTools;
import de.christophbrill.capacity.util.importer.RequirementsUpload;
import de.christophbrill.capacity.util.mappers.RequirementMapper;
import io.smallrye.mutiny.Uni;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import org.eclipse.microprofile.context.ManagedExecutor;
import org.jboss.resteasy.reactive.PartType;
import org.jboss.resteasy.reactive.RestForm;
import org.jboss.resteasy.reactive.multipart.FileUpload;
import java.util.UUID;

@Path("/requirement")
public class RequirementService extends AbstractResourceService<Requirement, RequirementEntity> {

    @Inject
    RequirementsUpload requirementsUpload;
    @Inject
    ManagedExecutor executor;
    @Inject
    ProgressCache progressCache;
    @Override
    protected Class<RequirementEntity> getEntityClass() {
        return RequirementEntity.class;
    }

    @Override
    protected RequirementSelector getSelector() {
        return new RequirementSelector(em);
    }

    @Override
    protected RequirementMapper getMapper() {
        return RequirementMapper.INSTANCE;
    }

    @Override
    protected String getCreatePermission() {
        return Permission.ADMIN_REQUIREMENTS.name();
    }

    @Override
    protected String getReadPermission() {
        return Permission.SHOW_REQUIREMENTS.name();
    }

    @Override
    protected String getUpdatePermission() {
        return Permission.ADMIN_REQUIREMENTS.name();
    }

    @Override
    protected String getDeletePermission() {
        return Permission.ADMIN_REQUIREMENTS.name();
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/analyzeupload")
    public ImportConfiguration analyzeUpload(@RestForm FileUpload file,
                                              @RestForm("filename") String filename,
                                              @RestForm Integer sheet) {
        return XlsxImportTools.analyzeUpload(em, file, filename, sheet);
    }

    @POST
    @Path("/import")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.TEXT_PLAIN)
    @Transactional
    public String upload(@RestForm FileUpload file,
                         @RestForm("filename") String filename,
                         @RestForm Integer sheet,
                         @RestForm Boolean removeExisting,
                         @RestForm String identifierType,
                         @RestForm("identifier") int identifierColumn,
                         @RestForm("columns") @PartType(MediaType.APPLICATION_JSON) RequirementsUpload.Columns columns) {
        filename = filename.toLowerCase();

        ObjectMapper mapper = new ObjectMapper();

        ImportProcessEntity importProcess = new ImportProcessEntity();
        importProcess.filename = filename;
        try {
            importProcess.columns = mapper.writeValueAsString(columns);
        } catch (JsonProcessingException e) {
            throw new BadArgumentException("Could not store columns as JSON");
        }
        importProcess.identifierType = identifierType;
        importProcess.identifierColumn = identifierColumn;
        importProcess.sheet = sheet;
        importProcess.persist();

        Progress<ImportResult> progress = progressCache.createProgress();

        Uni.createFrom()
                .item(() -> progress.key)
                .emitOn(executor)
                .subscribe()
                .with(uuid -> requirementsUpload.runImport(progress, file, columns, identifierType, identifierColumn, sheet), Throwable::printStackTrace);

        return progress.key;
    }
}
