package de.christophbrill.capacity.ui.rest;

import de.christophbrill.appframework.ui.exceptions.NotAuthorizedException;
import de.christophbrill.appframework.ui.rest.AbstractService;
import de.christophbrill.capacity.persistence.model.EmployeeEntity;
import de.christophbrill.capacity.persistence.model.Permission;
import de.christophbrill.capacity.persistence.selector.EmployeeSelector;
import de.christophbrill.capacity.ui.dto.Employee;
import de.christophbrill.capacity.util.mappers.EmployeeMapper;

import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import java.time.LocalDate;
import java.util.List;

@Path("/absent")
public class AbsentService extends AbstractService {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Employee> getEmployees(@DefaultValue("") @QueryParam("date") LocalDate date,
                                       @DefaultValue("") @QueryParam("search") String search) {

        if (!identity.hasRole(Permission.SHOW_EMPLOYEES.name())) {
            throw new NotAuthorizedException();
        }

        if (date == null) {
            date = LocalDate.now();
        }
        List<EmployeeEntity> absentEmployees = new EmployeeSelector(em)
                .withAbsence(date)
                .withSearch(search)
                .withActiveContract(date, date)
                .findAll();
        return EmployeeMapper.INSTANCE.mapEntitiesToDtos(absentEmployees);
    }

}
