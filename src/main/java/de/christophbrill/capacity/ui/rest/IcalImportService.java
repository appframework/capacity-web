package de.christophbrill.capacity.ui.rest;

import biweekly.ICalendar;
import de.christophbrill.appframework.ui.async.ProgressCache;
import de.christophbrill.appframework.ui.dto.Credentials;
import de.christophbrill.appframework.ui.dto.ImportResult;
import de.christophbrill.appframework.ui.dto.Progress;
import de.christophbrill.appframework.ui.rest.AbstractResourceService;
import de.christophbrill.capacity.persistence.model.IcalImportEntity;
import de.christophbrill.capacity.persistence.model.Permission;
import de.christophbrill.capacity.persistence.selector.IcalImportSelector;
import de.christophbrill.capacity.ui.dto.IcalImport;
import de.christophbrill.capacity.util.importer.IcalImporter;
import de.christophbrill.capacity.util.mappers.IcalImportMapper;
import io.quarkus.logging.Log;
import io.smallrye.mutiny.Uni;
import jakarta.annotation.Nullable;
import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import org.eclipse.microprofile.context.ManagedExecutor;
import org.jboss.resteasy.reactive.RestForm;
import org.jboss.resteasy.reactive.multipart.FileUpload;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.UUID;

@Path("/ical_import")
public class IcalImportService extends AbstractResourceService<IcalImport, IcalImportEntity> {

    @Inject
    IcalImporter icalImporter;
    @Inject
    ManagedExecutor executor;
    @Inject
    ProgressCache progressCache;

    @Override
    protected Class<IcalImportEntity> getEntityClass() {
        return IcalImportEntity.class;
    }

    @Override
    protected IcalImportSelector getSelector() {
        return new IcalImportSelector(em);
    }

    @Override
    protected IcalImportMapper getMapper() {
        return IcalImportMapper.INSTANCE;
    }

    @POST
    @Path("/import/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public String importIcal(@PathParam("id") final Long icalImportId,
                             @Nullable Credentials credentials) {
        final Progress<ImportResult> progress = progressCache.createProgress();

        Uni.createFrom()
                .item(() -> progress.key)
                .invoke(() -> icalImporter.runImport(progress, icalImportId, credentials))
                .emitOn(executor)
                .subscribe()
                .with(done -> Log.infov("Completed import of {0} ({1})", icalImportId, done), Throwable::printStackTrace);

        return progress.key;
    }

    @POST
    @Path("/upload/{id}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.TEXT_PLAIN)
    public String uploadFile(@PathParam("id") final Long icalImportId,
                             @RestForm FileUpload file) {
        final Progress<ImportResult> progress = progressCache.createProgress();

        final ICalendar calendar;
        try (InputStream stream = new ByteArrayInputStream(Files.readAllBytes(file.uploadedFile()))) {
            calendar = icalImporter.readCalendar(stream);
        } catch (IOException | RuntimeException e) {
            progress.completed = true;
            progress.success = false;
            progress.message = e.getMessage();
            progress.result = new ImportResult();
            return progress.key;
        }

        Uni.createFrom()
                .item(() -> progress.key)
                .invoke(() -> icalImporter.runImport2(progress, icalImportId, calendar))
                .emitOn(executor)
                .subscribe()
                .with(done -> Log.infov("Completed upload of {0} ({1})", icalImportId, done), Throwable::printStackTrace);

        return progress.key;
    }

    @Override
    protected String getCreatePermission() {
        return Permission.ADMIN_ICAL_IMPORTS.name();
    }

    @Override
    protected String getReadPermission() {
        return Permission.SHOW_ICAL_IMPORTS.name();
    }

    @Override
    protected String getUpdatePermission() {
        return Permission.ADMIN_ICAL_IMPORTS.name();
    }

    @Override
    protected String getDeletePermission() {
        return Permission.ADMIN_ICAL_IMPORTS.name();
    }

}
