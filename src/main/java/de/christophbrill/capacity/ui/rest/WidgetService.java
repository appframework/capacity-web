package de.christophbrill.capacity.ui.rest;

import de.christophbrill.appframework.ui.rest.AbstractResourceService;
import de.christophbrill.capacity.persistence.model.Permission;
import de.christophbrill.capacity.persistence.model.WidgetEntity;
import de.christophbrill.capacity.persistence.selector.WidgetSelector;
import de.christophbrill.capacity.ui.dto.Widget;
import de.christophbrill.capacity.util.mappers.WidgetMapper;

import jakarta.ws.rs.Path;

@Path("/widget")
public class WidgetService extends AbstractResourceService<Widget, WidgetEntity> {

	@Override
	protected Class<WidgetEntity> getEntityClass() {
		return WidgetEntity.class;
	}

	@Override
	protected WidgetSelector getSelector() {
		return new WidgetSelector(em);
	}

	@Override
	protected WidgetMapper getMapper() {
		return WidgetMapper.INSTANCE;
	}

	@Override
	protected String getCreatePermission() {
		return Permission.ADMIN_WIDGETS.name();
	}

	@Override
	protected String getReadPermission() {
		return Permission.SHOW_WIDGETS.name();
	}

	@Override
	protected String getUpdatePermission() {
		return Permission.ADMIN_WIDGETS.name();
	}

	@Override
	protected String getDeletePermission() {
		return Permission.ADMIN_WIDGETS.name();
	}

}
