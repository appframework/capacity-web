package de.christophbrill.capacity.ui.rest;

import biweekly.Biweekly;
import biweekly.ICalVersion;
import biweekly.ICalendar;
import biweekly.component.VEvent;
import biweekly.property.CalendarScale;
import biweekly.property.ProductId;
import biweekly.property.Uid;
import de.christophbrill.appframework.persistence.model.DbObject;
import de.christophbrill.appframework.ui.exceptions.NotAuthorizedException;
import de.christophbrill.appframework.ui.exceptions.NotFoundException;
import de.christophbrill.appframework.ui.rest.AbstractService;
import de.christophbrill.capacity.persistence.model.AbsenceEntity;
import de.christophbrill.capacity.persistence.model.HolidayEntity;
import de.christophbrill.capacity.persistence.model.Permission;
import de.christophbrill.capacity.persistence.selector.AbsenceSelector;
import de.christophbrill.capacity.persistence.selector.HolidaySelector;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Response;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

@Path("/calendar")
public class CalendarService extends AbstractService {

	@ConfigProperty(name = "quarkus.application.version")
	String version;

	private Uid generateUid(DbObject entity) {
		String b = entity.getClass().getSimpleName() + '-' + entity.id;
		return new Uid(b);
	}

	@GET
	@Path("/{id}.ics")
	@Produces("text/calendar")
	public Response getIcal(@PathParam("id") String id) {
		ICalendar calendar = new ICalendar();
		calendar.setProductId(new ProductId("-//Christoph Brill//capacity "
						+ version
						+ "//EN"));
		calendar.setVersion(ICalVersion.V2_0);
		calendar.setCalendarScale(CalendarScale.gregorian());
		if ("holidays".equals(id)) {
			if (!identity.hasRole(Permission.SHOW_HOLIDAYS.name())) {
				throw new NotAuthorizedException();
			}

			List<HolidayEntity> holidays = new HolidaySelector(em).findAll();
			for (HolidayEntity holiday : holidays) {
				VEvent event = new VEvent();
				event.setDateStart(toIcalDate(holiday.date));
				event.setSummary(holiday.name);
				event.setUid(generateUid(holiday));
				event.setCreated(toIcalDate(holiday.created));
				calendar.addEvent(event);
			}
		} else if ("absences".equals(id)) {
			if (!identity.hasRole(Permission.SHOW_ABSENCES.name())) {
				throw new NotAuthorizedException();
			}
			List<AbsenceEntity> absences = new AbsenceSelector(em).findAll();
			for (AbsenceEntity absence : absences) {
				VEvent event = new VEvent();
				event.setDateStart(toIcalDate(absence.start));
				event.setDateEnd(toIcalDate(absence.end));
				event.setSummary(absence.reason + ": " + absence.employee.name);
				event.setUid(generateUid(absence));
				event.setCreated(toIcalDate(absence.created));
				calendar.addEvent(event);
			}
		} else {
			throw new NotFoundException("Calendar with id " + id + " not found");
		}

		return Response.ok(Biweekly.write(calendar).go()).build();
	}

	private Date toIcalDate(LocalDate date) {
		return Date.from(date.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
	}

	private Date toIcalDate(LocalDateTime dateTime) {
		return Date.from(dateTime.atZone(ZoneId.systemDefault()).toInstant());
	}

}
