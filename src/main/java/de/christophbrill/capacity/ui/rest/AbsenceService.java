package de.christophbrill.capacity.ui.rest;

import de.christophbrill.appframework.mapping.ResourceMapper;
import de.christophbrill.appframework.ui.rest.AbstractResourceService;
import de.christophbrill.capacity.persistence.model.AbsenceEntity;
import de.christophbrill.capacity.persistence.model.Permission;
import de.christophbrill.capacity.persistence.selector.AbsenceSelector;
import de.christophbrill.capacity.ui.dto.Absence;
import de.christophbrill.capacity.util.mappers.AbsenceMapper;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import java.util.List;

@Path("/absence")
public class AbsenceService extends AbstractResourceService<Absence, AbsenceEntity> {

	@GET
	@Path("/employee/{employeeId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<Absence> getByEmployee(@PathParam("employeeId") Integer employeeId) {
		List<AbsenceEntity> absences = getSelector()
				.withEmployeeId(employeeId)
				.withSortColumn("start", true)
				.findAll();
		return AbsenceMapper.INSTANCE.mapEntitiesToDtos(absences);
	}

	@Override
	protected Class<AbsenceEntity> getEntityClass() {
		return AbsenceEntity.class;
	}

	@Override
	protected AbsenceSelector getSelector() {
		return new AbsenceSelector(em);
	}

	@Override
	protected ResourceMapper<Absence, AbsenceEntity> getMapper() {
		return AbsenceMapper.INSTANCE;
	}

	@Override
	protected String getCreatePermission() {
		return Permission.ADMIN_ABSENCES.name();
	}

	@Override
	protected String getReadPermission() {
		return Permission.SHOW_ABSENCES.name();
	}

	@Override
	protected String getUpdatePermission() {
		return Permission.ADMIN_ABSENCES.name();
	}

	@Override
	protected String getDeletePermission() {
		return Permission.ADMIN_ABSENCES.name();
	}

}
