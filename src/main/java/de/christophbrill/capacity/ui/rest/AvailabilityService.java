package de.christophbrill.capacity.ui.rest;

import de.christophbrill.capacity.persistence.model.EmployeeEntity;
import de.christophbrill.capacity.persistence.selector.EmployeeSelector;
import de.christophbrill.capacity.ui.dto.Employee;
import de.christophbrill.capacity.util.mappers.EmployeeMapper;

import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import java.time.LocalDate;
import java.util.List;

@Path("/available")
public class AvailabilityService {

	@Inject
	EntityManager entityManager;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Employee> getEmployees(@DefaultValue("") @QueryParam("date") LocalDate date,
									   @DefaultValue("") @QueryParam("search") String search) {
		if (date == null) {
			date = LocalDate.now();
		}
		List<EmployeeEntity> availableEmployees = new EmployeeSelector(entityManager)
				.withAvailability(date)
				.withSearch(search)
				.withActiveContract(date, date)
				.findAll();
		return EmployeeMapper.INSTANCE.mapEntitiesToDtos(availableEmployees);
	}

}
