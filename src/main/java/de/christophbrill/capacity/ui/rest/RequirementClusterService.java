package de.christophbrill.capacity.ui.rest;

import de.christophbrill.appframework.ui.rest.AbstractResourceService;
import de.christophbrill.capacity.persistence.model.Permission;
import de.christophbrill.capacity.persistence.model.RequirementClusterEntity;
import de.christophbrill.capacity.persistence.selector.RequirementClusterSelector;
import de.christophbrill.capacity.ui.dto.RequirementCluster;
import de.christophbrill.capacity.util.mappers.RequirementClusterMapper;

import jakarta.ws.rs.Path;

@Path("/requirementcluster")
public class RequirementClusterService extends AbstractResourceService<RequirementCluster, RequirementClusterEntity> {

    @Override
    protected Class<RequirementClusterEntity> getEntityClass() {
        return RequirementClusterEntity.class;
    }

    @Override
    protected RequirementClusterSelector getSelector() {
        return new RequirementClusterSelector(em);
    }

    @Override
    protected RequirementClusterMapper getMapper() {
        return RequirementClusterMapper.INSTANCE;
    }

    @Override
    protected String getCreatePermission() {
        return Permission.ADMIN_REQUIREMENTCLUSTERS.name();
    }

    @Override
    protected String getReadPermission() {
        return Permission.SHOW_REQUIREMENTCLUSTERS.name();
    }

    @Override
    protected String getUpdatePermission() {
        return Permission.ADMIN_REQUIREMENTCLUSTERS.name();
    }

    @Override
    protected String getDeletePermission() {
        return Permission.ADMIN_REQUIREMENTCLUSTERS.name();
    }

}
