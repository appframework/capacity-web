package de.christophbrill.capacity.ui.rest;

import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.appframework.ui.exceptions.NullArgumentException;
import de.christophbrill.capacity.persistence.model.AbsenceEntity;
import de.christophbrill.capacity.persistence.model.EmployeeEntity;
import de.christophbrill.capacity.persistence.model.EpisodeEntity;
import de.christophbrill.capacity.persistence.model.HolidayEntity;
import de.christophbrill.capacity.persistence.model.LabelEntity;
import de.christophbrill.capacity.persistence.model.WorkingHoursEntity;
import de.christophbrill.capacity.persistence.selector.EmployeeSelector;
import de.christophbrill.capacity.persistence.selector.EpisodeSelector;
import de.christophbrill.capacity.persistence.selector.HolidaySelector;
import de.christophbrill.capacity.persistence.selector.LabelSelector;
import de.christophbrill.capacity.ui.dto.Employee;
import de.christophbrill.capacity.ui.dto.WorkingHoursDetails;
import de.christophbrill.capacity.ui.dto.WorkingHoursList;
import de.christophbrill.capacity.ui.dto.WorkingHoursPerEmployee;
import de.christophbrill.capacity.ui.dto.WorkingHoursRequest;
import de.christophbrill.capacity.ui.dto.WorkingHoursRequest.Unit;
import de.christophbrill.capacity.util.mappers.EmployeeMapper;
import org.apache.commons.collections4.CollectionUtils;

import jakarta.annotation.Nonnull;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import java.text.NumberFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

@Path("/capacity")
public class CapacityService {

	@Inject
	EntityManager entityManager;

	@POST
	@Path("/workinghours")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<WorkingHoursPerEmployee> getWorkingHours(WorkingHoursRequest request) {

		final Map<Long, Integer> velocities = new HashMap<>();
		final Map<Long, List<List<Long>>> skills = new HashMap<>();
		LocalDate start;
		LocalDate end;
		List<Long> employeeIds;
		if (request.episodeId != null) {
			EpisodeEntity episode = new EpisodeSelector(entityManager).withId(request.episodeId).find();
			if (episode == null) {
				throw new BadArgumentException("Episode with ID " + request.episodeId + " not found");
			}
			start = episode.start;
			end = episode.end;
			employeeIds = episode.employeeEpisodes.stream().map(employee -> {
				Integer velocity = employee.velocity;
				if (velocity != null) {
					velocities.put(employee.employee.id, velocity);
				}
				return employee.employee.id;
			}).toList();
		} else {

			// Check if valid dates are passed, otherwise look for 10 days into
			// the future and into the past
			if (request.start == null) {
				start = LocalDate.now().minusDays(10);
			} else {
				start = request.start;
			}
			if (request.end == null) {
				end = LocalDate.now().plusDays(10);
			} else {
				end = request.end;
			}
			employeeIds = null;
		}

		// Load all employees that have an active contract in the timespan
		List<EmployeeEntity> employees = new EmployeeSelector(entityManager)
				.withActiveContract(start, end)
				.withIds(employeeIds)
				.findAll();
		
		// Remove all employees not matching the search query
		if (CollectionUtils.isNotEmpty(request.filter)) {
			Set<EmployeeEntity> tmpEmployees = new HashSet<>(employees.size());
			for (List<Long> f : request.filter) {
				if (CollectionUtils.isEmpty(f)) {
					continue;
				}
				outer: for (EmployeeEntity employee : employees) {
					for (Long a : f) {
						boolean found = false;
						for (LabelEntity ability : employee.abilities) {
							if (a.equals(ability.id)) {
								found = true;
								break;
							}
						}
						if (!found) {
							continue outer;
						}
					}
					List<List<Long>> employeeSkills = skills.computeIfAbsent(employee.id, k -> new ArrayList<>());
					employeeSkills.add(f);
					tmpEmployees.add(employee);
				}
			}
			employees = new ArrayList<>(tmpEmployees);
		}

		// Put velocities into lookup (when they were not overridden in the
		// episode)
		employees
			.forEach(employee -> velocities.putIfAbsent(employee.id, employee.velocity));

		// Calculate the capatcity per employee
		List<WorkingHoursPerEmployee> result = new ArrayList<>(employees.size());
		for (EmployeeEntity employee : employees) {
			int velocity = velocities.get(employee.id);
			if (request.useVelocity && velocity == 0) {
				continue;
			}
			int actualVelocity = request.useVelocity ? velocity : 100;
			Employee dto = EmployeeMapper.INSTANCE.mapEntityToDto(employee);
			result.add(new WorkingHoursPerEmployee(dto,
					getWorkingHoursForEmployee(employee, start, end, actualVelocity, request.units), actualVelocity, skills.getOrDefault(employee.id, Collections.emptyList())));
		}

		return result;
	}

	@GET
	@Path("/workinghours/{employeeId}")
	@Produces(MediaType.APPLICATION_JSON)
	public WorkingHoursList getWorkingHoursForEmployee(@PathParam("employeeId") Long employeeId,
			@DefaultValue("") @QueryParam("start") LocalDate start, @DefaultValue("") @QueryParam("end") LocalDate end,
			@DefaultValue("false") @QueryParam("useVelocity") boolean useVelocity) {

		// Check for valid date ranges
		if (start == null || end == null) {
			throw new NullArgumentException("start and end");
		}
		if (!start.isBefore(end)) {
			throw new BadArgumentException("start must be before end");
		}

		// Check for valid employee IDs
		if (employeeId == null) {
			throw new NullArgumentException("employeeId");
		}

		EmployeeEntity employee = EmployeeEntity.findById(employeeId);
		if (employee == null) {
			throw new BadArgumentException("Employee with ID " + employeeId + " not found");
		}

		return getWorkingHoursForEmployee(employee, start, end, useVelocity ? employee.velocity : 100, WorkingHoursRequest.Unit.hours);
	}

	private WorkingHoursList getWorkingHoursForEmployee(@Nonnull EmployeeEntity employee, @Nonnull LocalDate start,
			@Nonnull LocalDate end, int velocity, WorkingHoursRequest.Unit units) {
		Map<LocalDate, Integer> reductions = new HashMap<>();

		List<HolidayEntity> holidays = new HolidaySelector(entityManager).withStartInclusive(start).withEndInclusive(end)
				.withIncludingLocation(employee.location).findAll();

		holidays.stream().filter(holiday -> holiday.date.isBefore(end) && holiday.date.isAfter(start))
				.forEach(holiday -> reductions.put(holiday.date, holiday.hoursReduction));

		Map<DayOfWeek, Integer> durations = getWorkingHourDurations(employee);

		List<AbsenceEntity> absences = employee.absences;
		for (AbsenceEntity absence : absences) {
			LocalDate date = absence.start;
			while (!date.isAfter(end) && !date.isAfter(absence.end)) {
				if (!date.isBefore(start)) {
					Integer hours = durations.get(date.getDayOfWeek());
					reductions.put(date, hours != null ? hours : 0);
				}
				date = date.plusDays(1);
			}
		}

		return getWorkingHoursForEmployee(start, end, employee, reductions, velocity, units);
	}

	private WorkingHoursList getWorkingHoursForEmployee(LocalDate start, LocalDate end, EmployeeEntity employee,
			Map<LocalDate, Integer> reductions, int velocity, Unit units) {
		double workinghours = 0;
		List<WorkingHoursDetails> details = new ArrayList<>();

		Map<DayOfWeek, Integer> durations = getWorkingHourDurations(employee);

		LocalDate date = start;
		while (!date.isAfter(end)) {
			double workinghoursOfDay;
			if (employee.contract.start != null && date.isBefore(employee.contract.start)
					|| employee.contract.end != null && date.isAfter(employee.contract.end)) {
				workinghoursOfDay = 0;
			} else {
				DayOfWeek dayOfWeek = date.getDayOfWeek();
				Integer hours = durations.get(dayOfWeek);
				workinghoursOfDay = hours != null ? hours : 0;

				if (reductions.containsKey(date)) {
					workinghoursOfDay -= reductions.get(date);
				}

				workinghoursOfDay = Math.max(workinghoursOfDay, 0);
				if (velocity != 100) {
					workinghoursOfDay *= velocity / 100d;
				}
			}
			
			if (units == Unit.hours) {
				// No calculation necessary
			} else if (units == Unit.days) {
				workinghoursOfDay /= 8;
			} else if (units == Unit.weeks) {
				workinghoursOfDay /= 40;
			}

			
			workinghours += workinghoursOfDay;
			details.add(new WorkingHoursDetails(date, workinghoursOfDay));
			date = date.plusDays(1);
		}
		return new WorkingHoursList(start, end, workinghours, details);
	}

	private Map<DayOfWeek, Integer> getWorkingHourDurations(EmployeeEntity employee) {
		Map<DayOfWeek, Integer> durations = new EnumMap<>(DayOfWeek.class);
		for (WorkingHoursEntity workingHours : employee.contract.workingHours) {
			durations.put(DayOfWeek.of(workingHours.dayOfWeek),
					(int) workingHours.start.until(workingHours.end, ChronoUnit.HOURS));
		}
		return durations;
	}

	@GET
	@Path("/export/confluence")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String exportConfluence(@QueryParam("episode") String episodeLike,
								   @QueryParam("label") String labelLike,
								   @QueryParam("skills") String skills) {
		List<EpisodeEntity> episodes = new EpisodeSelector(entityManager)
				.withNameLike(episodeLike)
				.findAll();
		List<LabelEntity> teams = new LabelSelector(entityManager)
				.withSearch(labelLike)
				.findAll().stream()
				.toList();
		List<LabelEntity> abilities = new LabelSelector(entityManager)
			.withNames(skills.split(","))
			.findAll();
		StringBuilder result = new StringBuilder();
		result.append("|| Team || Skill ");
		for (EpisodeEntity episode : episodes) {
			result.append("||").append(episode.name);
		}
		result.append("||\n");
		NumberFormat format = NumberFormat.getInstance(Locale.GERMANY);
		format.setMaximumFractionDigits(1);
		for (LabelEntity team : teams) {
			for (LabelEntity ability : abilities) {
				List<Double> amount = new ArrayList<>(episodes.size());
				for (EpisodeEntity episode : episodes) {
					WorkingHoursRequest request = new WorkingHoursRequest();
					request.useVelocity = true;
					request.episodeId = episode.id;
					request.filter = Collections.singletonList(Arrays.asList(team.id, ability.id));
					request.units = Unit.hours;
					List<WorkingHoursPerEmployee> workingHours = getWorkingHours(request);
					double value = 0;
					for (WorkingHoursPerEmployee workingHoursPerEmployee : workingHours) {
						value += workingHoursPerEmployee.workingHours.amount;
					}
					amount.add(value);
				}
				if (amount.stream().mapToDouble(Double::doubleValue).sum() > 0) {
					result.append('|').append(team.name).append('|').append(ability.name).append('|');
					for (Double value : amount) {
						result.append(format.format(value / 8)).append(" d|");
					}
					result.append('\n');
				}
			}
		}
		return result.toString();
	}
}
