package de.christophbrill.capacity.ui.rest;

import de.christophbrill.appframework.ui.rest.AbstractResourceService;
import de.christophbrill.capacity.persistence.model.HolidayEntity;
import de.christophbrill.capacity.persistence.model.Permission;
import de.christophbrill.capacity.persistence.selector.HolidaySelector;
import de.christophbrill.capacity.ui.dto.Holiday;
import de.christophbrill.capacity.util.mappers.HolidayMapper;

import jakarta.ws.rs.Path;

@Path("/holiday")
public class HolidayService extends AbstractResourceService<Holiday, HolidayEntity> {

	@Override
	protected Class<HolidayEntity> getEntityClass() {
		return HolidayEntity.class;
	}

	@Override
	protected HolidaySelector getSelector() {
		return new HolidaySelector(em);
	}

	@Override
	protected HolidayMapper getMapper() {
		return HolidayMapper.INSTANCE;
	}

	@Override
	protected String getCreatePermission() {
		return Permission.ADMIN_HOLIDAYS.name();
	}

	@Override
	protected String getReadPermission() {
		return Permission.SHOW_HOLIDAYS.name();
	}

	@Override
	protected String getUpdatePermission() {
		return Permission.ADMIN_HOLIDAYS.name();
	}

	@Override
	protected String getDeletePermission() {
		return Permission.ADMIN_HOLIDAYS.name();
	}

}
