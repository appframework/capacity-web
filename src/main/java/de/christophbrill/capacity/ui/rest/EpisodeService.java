package de.christophbrill.capacity.ui.rest;

import de.christophbrill.appframework.ui.rest.AbstractResourceService;
import de.christophbrill.capacity.persistence.model.EpisodeEntity;
import de.christophbrill.capacity.persistence.model.Permission;
import de.christophbrill.capacity.persistence.selector.EpisodeSelector;
import de.christophbrill.capacity.ui.dto.Episode;
import de.christophbrill.capacity.util.mappers.EpisodeMapper;

import jakarta.ws.rs.Path;

@Path("/episode")
public class EpisodeService extends AbstractResourceService<Episode, EpisodeEntity> {
	
	@Override
	protected Class<EpisodeEntity> getEntityClass() {
		return EpisodeEntity.class;
	}

	@Override
	protected EpisodeSelector getSelector() {
		return new EpisodeSelector(em);
	}

	@Override
	protected EpisodeMapper getMapper() {
		return EpisodeMapper.INSTANCE;
	}

	@Override
	protected String getCreatePermission() {
		return Permission.ADMIN_EPISODES.name();
	}

	@Override
	protected String getReadPermission() {
		return Permission.SHOW_EPISODES.name();
	}

	@Override
	protected String getUpdatePermission() {
		return Permission.ADMIN_EPISODES.name();
	}

	@Override
	protected String getDeletePermission() {
		return Permission.ADMIN_EPISODES.name();
	}

}
