package de.christophbrill.capacity.ui.rest;

import de.christophbrill.appframework.ui.rest.AbstractResourceService;
import de.christophbrill.capacity.persistence.model.EmployeeEntity;
import de.christophbrill.capacity.persistence.model.HolidayEntity;
import de.christophbrill.capacity.persistence.model.LocationEntity;
import de.christophbrill.capacity.persistence.model.Permission;
import de.christophbrill.capacity.persistence.selector.EmployeeSelector;
import de.christophbrill.capacity.persistence.selector.HolidaySelector;
import de.christophbrill.capacity.persistence.selector.LocationSelector;
import de.christophbrill.capacity.ui.dto.Employee;
import de.christophbrill.capacity.ui.dto.Holiday;
import de.christophbrill.capacity.ui.dto.Location;
import de.christophbrill.capacity.util.mappers.EmployeeMapper;
import de.christophbrill.capacity.util.mappers.HolidayMapper;
import de.christophbrill.capacity.util.mappers.LocationMapper;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

@Path("/location")
public class LocationService extends AbstractResourceService<Location, LocationEntity> {

    @Override
    protected Class<LocationEntity> getEntityClass() {
        return LocationEntity.class;
    }

    @Override
    protected LocationSelector getSelector() {
        return new LocationSelector(em);
    }

    @Override
    protected LocationMapper getMapper() {
        return LocationMapper.INSTANCE;
    }

    @Override
    protected String getCreatePermission() {
        return Permission.ADMIN_LOCATIONS.name();
    }

    @Override
    protected String getReadPermission() {
        return Permission.SHOW_LOCATIONS.name();
    }

    @Override
    protected String getUpdatePermission() {
        return Permission.ADMIN_LOCATIONS.name();
    }

    @Override
    protected String getDeletePermission() {
        return Permission.ADMIN_LOCATIONS.name();
    }

    @GET
    @Path("/{id}/employees")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Employee> employees(
            @PathParam("id") Long id
    ) {
        EmployeeSelector employeeSelector = new EmployeeSelector(em);

        if (!identity.hasRole(Permission.VIEW_WITHOUT_CONTRACT.name())) {
            LocalDate date = LocalDate.now();
            employeeSelector.withActiveContract(date, date);
        }

        List<EmployeeEntity> employees = employeeSelector
                .withLocationId(id)
                .findAll();

        return EmployeeMapper.INSTANCE.mapEntitiesToDtos(employees);
    }

    @GET
    @Path("/{id}/holidays")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Holiday> holidays(
            @PathParam("id") Long id
    ) {

        LocationEntity location = LocationEntity.<LocationEntity>findById(id);
        if (location == null) {
            return Collections.emptyList();
        }

        HolidaySelector holidaySelector = new HolidaySelector(em);

        List<HolidayEntity> holidays = holidaySelector
                .withIncludingLocation(location)
                .withDate(LocalDate.now().minusMonths(1))
                .withSortColumn("date", true)
                .findAll();

        return HolidayMapper.INSTANCE.mapEntitiesToDtos(holidays);
    }
}
