package de.christophbrill.capacity.ui.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.christophbrill.appframework.ui.async.ProgressCache;
import de.christophbrill.appframework.ui.async.ProgressEndpoint;
import de.christophbrill.appframework.ui.dto.ImportResult;
import de.christophbrill.appframework.ui.dto.Progress;
import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.appframework.ui.rest.AbstractResourceService;
import de.christophbrill.capacity.persistence.model.EmployeeEntity;
import de.christophbrill.capacity.persistence.model.ImportProcessEntity;
import de.christophbrill.capacity.persistence.model.Permission;
import de.christophbrill.capacity.persistence.selector.EmployeeSelector;
import de.christophbrill.capacity.ui.dto.Employee;
import de.christophbrill.capacity.ui.dto.ExportConfiguration;
import de.christophbrill.capacity.ui.dto.ImportConfiguration;
import de.christophbrill.capacity.util.XlsxImportTools;
import de.christophbrill.capacity.util.importer.EmployeesUpload;
import de.christophbrill.capacity.util.mappers.EmployeeMapper;
import io.quarkus.logging.Log;
import io.smallrye.mutiny.Uni;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.eclipse.microprofile.context.ManagedExecutor;
import org.jboss.resteasy.reactive.PartType;
import org.jboss.resteasy.reactive.RestForm;
import org.jboss.resteasy.reactive.multipart.FileUpload;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;
import java.util.stream.Collectors;

@Path("/employee")
public class EmployeeService extends AbstractResourceService<Employee, EmployeeEntity> {

	@Inject
	EmployeesUpload employeeUpload;
	@Inject
	ManagedExecutor executor;
	@Inject
	ProgressCache progressCache;

	@Override
	protected Class<EmployeeEntity> getEntityClass() {
		return EmployeeEntity.class;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/active")
	public List<Employee> getActive() {
		checkReadPermission();
		LocalDate date = LocalDate.now();

		List<EmployeeEntity> entities = getSelector()
				.withActiveContract(date, date)
				.findAll();

		return getMapper().mapEntitiesToDtos(entities);
	}

	@Override
	protected EmployeeSelector getSelector() {
		EmployeeSelector employeeSelector = new EmployeeSelector(em);
		if (!identity.hasRole(Permission.VIEW_WITHOUT_CONTRACT.name())) {
			LocalDate date = LocalDate.now();
			employeeSelector.withActiveContract(date, date);
		}
		return employeeSelector;
	}

	@Override
	protected EmployeeMapper getMapper() {
		return EmployeeMapper.INSTANCE;
	}

	@GET
	@Path("/location/{locationId}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Employee> getByLocation(@PathParam("locationId") Long locationId) {
		List<EmployeeEntity> employees = getSelector()
				.withLocationId(locationId)
				.findAll();
		return EmployeeMapper.INSTANCE.mapEntitiesToDtos(employees);
	}

	@POST
	@Path("/export/confluence")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String exportConfluence(ExportConfiguration exportConfiguration) {
		if (CollectionUtils.isEmpty(exportConfiguration.abilitiesX) || MapUtils.isEmpty(exportConfiguration.abilitiesY)) {
			return "";
		}

		// Load all employees
		LocalDate date = LocalDate.now();
		List<EmployeeEntity> employees = getSelector()
				.withActiveContract(date, date)
				.withSortColumn("email", true)
				.findAll();
		Map<EmployeeEntity, Set<String>> employeeAbilities = new HashMap<>();
		for (EmployeeEntity employee : employees) {
			employeeAbilities.put(employee, employee.abilities.stream().map(a -> a.name).collect(Collectors.toSet()));
		}

		// Build the table header
		StringBuilder builder = new StringBuilder();
		builder.append("|| Ability ||");
		for (String ability : exportConfiguration.abilitiesX) {
			builder.append(' ');
			builder.append(ability);
			builder.append(" ||");
		}
		builder.append("\n");

		// Build the rows
		for (Entry<String, List<String>> entry : exportConfiguration.abilitiesY.entrySet()) {
			builder.append("|| ").append(entry.getKey()).append(" | ");

			for (String abilityX : exportConfiguration.abilitiesX) {
				Set<EmployeeEntity> employeesWithAbilities = new TreeSet<>(Comparator.comparing(EmployeeEntity::getEmail));
				for (String abilityY : entry.getValue()) {
					for (EmployeeEntity employee : employees) {
						Set<String> abilities = employeeAbilities.get(employee);
						if (abilities.contains(abilityX) && abilities.contains(abilityY)) {
							employeesWithAbilities.add(employee);
						}
					}
				}
				if (employeesWithAbilities.isEmpty()) {
					builder.append(' ');
				} else {
					boolean first = true;
					for (EmployeeEntity employee : employeesWithAbilities) {
						if (!first) {
							builder.append("\\\\ ");
						}
						builder.append("[~").append(employee.getEmail()).append("] ");
						first = false;
					}
				}
				builder.append("| ");
			}
			builder.append("\n");
		}

		return builder.toString();
	}

	@Override
	protected String getCreatePermission() {
		return Permission.ADMIN_EMPLOYEES.name();
	}

	@Override
	protected String getReadPermission() {
		return Permission.SHOW_EMPLOYEES.name();
	}

	@Override
	protected String getUpdatePermission() {
		return Permission.ADMIN_EMPLOYEES.name();
	}

	@Override
	protected String getDeletePermission() {
		return Permission.ADMIN_EMPLOYEES.name();
	}

	@GET
	@Path("/colors")
	@Produces(MediaType.APPLICATION_JSON)
	public List<String> getColors() {
		return em.createQuery("select distinct color from Employee order by color", String.class)
				.getResultList();
	}

	@POST
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/analyzeupload")
	public ImportConfiguration analyzeUpload(@RestForm FileUpload file,
											  @RestForm("filename") String filename,
											  @RestForm Integer sheet) {
		return XlsxImportTools.analyzeUpload(em, file, filename, sheet);
	}

	@POST
	@Path("/import")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.TEXT_PLAIN)
	@Transactional
	public String upload(@RestForm FileUpload file,
						 @RestForm("filename") String filename,
						 @RestForm int sheet,
						 @RestForm("identifier") int identifierColumn,
						 @RestForm("columns") @PartType(MediaType.APPLICATION_JSON) EmployeesUpload.Columns columns) {
		filename = filename.toLowerCase();

		ObjectMapper mapper = new ObjectMapper();

		ImportProcessEntity importProcess = new ImportProcessEntity();
		importProcess.filename = filename;
		try {
			importProcess.columns = mapper.writeValueAsString(columns);
		} catch (JsonProcessingException e) {
			throw new BadArgumentException("Could not store columns as JSON");
		}
		importProcess.identifierType = "email";
		importProcess.identifierColumn = identifierColumn;
		importProcess.sheet = sheet;
		importProcess.persist();

		Progress<ImportResult> progress = progressCache.createProgress();

		Uni.createFrom()
				.item(() -> progress.key)
				.invoke(uuid -> employeeUpload.runImport(progress, file, columns, identifierColumn, sheet))
				.emitOn(executor)
				.subscribe()
				.with(done -> Log.infov("Completed import of employees ({1})", importProcess.id, done), Throwable::printStackTrace);

		return progress.key;
	}
}
