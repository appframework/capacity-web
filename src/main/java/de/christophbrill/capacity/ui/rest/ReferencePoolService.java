package de.christophbrill.capacity.ui.rest;

import de.christophbrill.appframework.ui.rest.AbstractResourceService;
import de.christophbrill.capacity.persistence.model.Permission;
import de.christophbrill.capacity.persistence.model.ReferencePoolEntity;
import de.christophbrill.capacity.persistence.selector.ReferencePoolSelector;
import de.christophbrill.capacity.ui.dto.ReferencePool;
import de.christophbrill.capacity.util.mappers.ReferencePoolMapper;

import jakarta.ws.rs.Path;

@Path("/referencepool")
public class ReferencePoolService extends AbstractResourceService<ReferencePool, ReferencePoolEntity> {

    @Override
    protected Class<ReferencePoolEntity> getEntityClass() {
        return ReferencePoolEntity.class;
    }

    @Override
    protected ReferencePoolSelector getSelector() {
        return new ReferencePoolSelector(em);
    }

    @Override
    protected ReferencePoolMapper getMapper() {
        return ReferencePoolMapper.INSTANCE;
    }

    @Override
    protected String getCreatePermission() {
        return Permission.ADMIN_REFERENCEPOOLS.name();
    }

    @Override
    protected String getReadPermission() {
        return Permission.SHOW_REFERENCEPOOLS.name();
    }

    @Override
    protected String getUpdatePermission() {
        return Permission.ADMIN_REFERENCEPOOLS.name();
    }

    @Override
    protected String getDeletePermission() {
        return Permission.ADMIN_REFERENCEPOOLS.name();
    }

}
