package de.christophbrill.capacity.ui.dto;

import de.christophbrill.appframework.ui.dto.AbstractDto;

import java.util.List;

/**
 * The abilities required to complete a requirement
 */
public class RequiredAbility extends AbstractDto {

    /**
     * How much of the skill is required to complete the requirement
     */
    public int percentage;
    /**
     * IDs of the skills that are required to complete the requirement (logical AND)
     */
    public List<Label> skills;

}
