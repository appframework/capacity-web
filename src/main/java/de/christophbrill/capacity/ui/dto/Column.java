package de.christophbrill.capacity.ui.dto;

import java.util.Objects;

public class Column {
    public int index;
    public String label;

    public Column(int index, String label) {
        this.index = index;
        this.label = label;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Column column = (Column) o;
        return index == column.index &&
                Objects.equals(label, column.label);
    }

    @Override
    public int hashCode() {
        return Objects.hash(index, label);
    }

    @Override
    public String toString() {
        return index + " -> '" + label + '\'';
    }
}