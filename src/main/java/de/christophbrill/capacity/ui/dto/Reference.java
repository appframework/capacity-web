package de.christophbrill.capacity.ui.dto;

public class Reference {

    /**
     * ID of the {@link ReferencePool}
     */
    public Long poolId;
    /**
     * The key to inject into the template URL of the pool
     *
     * This is for example the JIRA issue key
     */
    public String referenceKey;

}
