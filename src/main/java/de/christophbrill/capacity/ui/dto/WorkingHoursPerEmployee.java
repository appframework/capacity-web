package de.christophbrill.capacity.ui.dto;

import java.util.List;

public class WorkingHoursPerEmployee {

    public Employee employee;
    public WorkingHoursList workingHours;
    public int velocity;
    public List<List<Long>> abilities;

    // For Jackson
    public WorkingHoursPerEmployee() {
    }

    public WorkingHoursPerEmployee(Employee employee, WorkingHoursList workingHours, int velocity, List<List<Long>> abilities) {
        this.employee = employee;
        this.workingHours = workingHours;
        this.velocity = velocity;
        this.abilities = abilities;
    }

}
