package de.christophbrill.capacity.ui.dto;

import java.time.LocalDate;
import java.util.List;

import de.christophbrill.appframework.ui.dto.AbstractDto;

public class Holiday extends AbstractDto implements Event {

    public LocalDate date;
    public String name;
    public int hoursReduction;
    public List<Long> locationIds;
    public String externalId;
    public Long icalImportId;
}
