package de.christophbrill.capacity.ui.dto;

import java.time.LocalDate;

public class WorkingHoursDetails {

    public LocalDate date;
    public double amount;

    // For Jackson
    public WorkingHoursDetails() {
    }

    public WorkingHoursDetails(LocalDate date, double amount) {
        this.date = date;
        this.amount = amount;
    }

}
