package de.christophbrill.capacity.ui.dto;

import java.time.LocalDate;

import de.christophbrill.appframework.ui.dto.AbstractDto;

public class Absence extends AbstractDto implements Event {

    public long employeeId;
    public LocalDate start;
    public LocalDate end;
    public String reason;
    public String externalId;
    public boolean imported;

}
