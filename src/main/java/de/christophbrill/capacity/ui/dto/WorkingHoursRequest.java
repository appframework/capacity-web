package de.christophbrill.capacity.ui.dto;

import java.time.LocalDate;
import java.util.List;

public class WorkingHoursRequest {

    public boolean useVelocity;
    public Long episodeId;
    public LocalDate start;
    public LocalDate end;
    /**
     * Outer list is logical OR, inner list is logical AND
     */
    public List<List<Long>> filter;
    public Unit units;

    public enum Unit {
        hours,
        days,
        weeks
    }

}
