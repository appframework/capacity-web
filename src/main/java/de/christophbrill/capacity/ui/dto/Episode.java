package de.christophbrill.capacity.ui.dto;

import java.time.LocalDate;
import java.util.List;

import de.christophbrill.appframework.ui.dto.AbstractDto;

public class Episode extends AbstractDto {

    public LocalDate start;
    public LocalDate end;
    public String name;
    public List<EmployeeEpisode> employeeEpisodes;

}
