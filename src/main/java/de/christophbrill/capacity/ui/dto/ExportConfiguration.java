package de.christophbrill.capacity.ui.dto;

import java.util.List;
import java.util.Map;

public class ExportConfiguration {

    public List<String> abilitiesX;
    public Map<String, List<String>> abilitiesY;

}
