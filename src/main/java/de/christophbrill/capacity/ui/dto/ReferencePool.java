package de.christophbrill.capacity.ui.dto;

import de.christophbrill.appframework.ui.dto.AbstractDto;

/**
 * A reference pool is a place where links in Requirements or RequirementClusters can point to.
 */
public class ReferencePool extends AbstractDto {

    /**
     * Human-readable name of this reference pool
     */
    public String name;

    /**
     * The URL to inject the reference ID into.
     *
     * In example of JIRA this can be {@code https://YOUR_JIRA_INSTALLATION/browse/$1}
     */
    public String templateUrl;

}
