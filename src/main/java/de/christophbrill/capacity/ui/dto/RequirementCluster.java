package de.christophbrill.capacity.ui.dto;

import de.christophbrill.appframework.ui.dto.AbstractDto;

import java.util.List;

/**
 * A requirement cluster is an arbitrary collection of {@link Requirement}s.
 *
 * If you think of SAFe this could be a program initiative, which groups epics.
 */
public class RequirementCluster extends AbstractDto {

    public String name;
    public List<Reference> references;

}
