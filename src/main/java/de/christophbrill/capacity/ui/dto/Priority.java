package de.christophbrill.capacity.ui.dto;

/**
 * Priority of a {@link Requirement}
 */
public enum Priority {

    LOW,
    MED_LOW,
    MEDIUM,
    MED_HIGH,
    HIGH

}
