package de.christophbrill.capacity.ui.dto;

import de.christophbrill.appframework.ui.dto.AbstractDto;

public class Widget extends AbstractDto {

    public String name;
    public String type;
    public String configuration;

}
