package de.christophbrill.capacity.ui.dto;

import java.time.LocalDateTime;

import de.christophbrill.appframework.ui.dto.AbstractDto;

public class IcalImport extends AbstractDto {

    public enum Auth {
        NONE, BASIC
    }

    public enum Type {
        HOLIDAYS, ABSENCES
    }

    public Type type;
    public String name;
    public String url;
    public LocalDateTime lastImported;
    public Auth auth;
    public String username;
    public String password;

}
