package de.christophbrill.capacity.ui.dto;

import java.time.LocalDate;
import java.util.List;

public class Contract {

    public LocalDate start;
    public LocalDate end;
    public int vacationDaysPerYear;
    public List<WorkingHours> workingHours;
    public Integer workingHoursPerWeek;

}