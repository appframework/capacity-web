package de.christophbrill.capacity.ui.dto;

import de.christophbrill.appframework.ui.dto.AbstractDto;

public class EmployeeEpisode extends AbstractDto {

    public Long employeeId;
    public Integer velocity;

}
