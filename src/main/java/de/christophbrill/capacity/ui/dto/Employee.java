package de.christophbrill.capacity.ui.dto;

import java.util.Set;

import de.christophbrill.appframework.ui.dto.AbstractDto;

public class Employee extends AbstractDto {

    public String name;
    public String email;
    public Set<Label> abilities;
    public Contract contract;
    public Long locationId;
    public String color;
    public int velocity;

}
