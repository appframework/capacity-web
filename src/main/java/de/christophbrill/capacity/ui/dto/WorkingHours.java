package de.christophbrill.capacity.ui.dto;

import java.time.LocalTime;

public class WorkingHours {

    public int dayOfWeek;
    public LocalTime start;
    public LocalTime end;

}
