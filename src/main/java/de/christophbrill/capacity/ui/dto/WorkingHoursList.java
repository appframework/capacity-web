package de.christophbrill.capacity.ui.dto;

import java.time.LocalDate;
import java.util.List;

public class WorkingHoursList {
    public LocalDate from;
    public LocalDate until;
    public double amount;
    public List<WorkingHoursDetails> details;

    // For Jackson
    public WorkingHoursList() {
    }

    public WorkingHoursList(LocalDate from, LocalDate until, double amount, List<WorkingHoursDetails> details) {
        this.from = from;
        this.until = until;
        this.amount = amount;
        this.details = details;
    }

}