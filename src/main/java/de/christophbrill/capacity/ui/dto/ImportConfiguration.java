package de.christophbrill.capacity.ui.dto;

import java.util.List;

public class ImportConfiguration {

    public List<Column> columns;
    public String configuration;
    public int identifierColumn;
    public String identifierType;
    public Integer sheet;

}
