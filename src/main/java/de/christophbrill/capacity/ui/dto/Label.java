package de.christophbrill.capacity.ui.dto;

import de.christophbrill.appframework.ui.dto.AbstractDto;
import java.util.List;

public class Label extends AbstractDto {

    public String name;
    public String color;
    public List<String> alias;
    public boolean precedence;

}
