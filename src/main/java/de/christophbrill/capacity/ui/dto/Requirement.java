package de.christophbrill.capacity.ui.dto;

import de.christophbrill.appframework.ui.dto.AbstractDto;

import java.time.LocalDate;
import java.util.List;

/**
 * A requirement that will consume available capacity.
 *
 * If you think of SAFe this would be an epic
 */
public class Requirement extends AbstractDto {

    /**
     * A human readable title of this requirement
     */
    public String name;
    /**
     * ID of the {@link RequirementCluster}
     */
    public long clusterId;
    /**
     * Priority of this requirement
     */
    public Priority priority;
    /**
     * List of references in foreign systems (e.g. a JIRA installation)
     */
    public List<Reference> references;
    /**
     * Necessary effort (in days)
     */
    public int effort;
    /**
     * Remaining effort (in days)
     */
    public Integer remaining;
    /**
     * Abilities required to complete this requirement
     *
     * The sum must be 100%
     */
    public List<RequiredAbility> requiredAbilities;
    /**
     * Date when this should be started
     */
    public LocalDate desiredStart;
    /**
     * Date when this must be delivered
     */
    public LocalDate due;

}
