package de.christophbrill.capacity.util;

import static de.christophbrill.capacity.util.XlsxImportTools.getFirstRowIndexHavingMaxWidth;
import static de.christophbrill.capacity.util.XlsxImportTools.getSheetNumber;

import de.christophbrill.appframework.ui.dto.ImportResult;
import de.christophbrill.appframework.ui.dto.Progress;
import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.capacity.persistence.model.ReferenceEntity;
import de.christophbrill.capacity.persistence.model.ReferencePoolEntity;
import de.christophbrill.capacity.persistence.model.RequirementClusterEntity;
import de.christophbrill.capacity.persistence.model.RequirementEntity;
import de.christophbrill.capacity.ui.dto.Priority;
import io.quarkus.logging.Log;
import jakarta.annotation.Nonnull;
import jakarta.annotation.Nullable;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.WebApplicationException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import java.io.ByteArrayInputStream;
import java.io.IOException;

@ApplicationScoped
@Named
public class RequirementImportXlsx {

    @Inject
    EntityManager entityManager;

    @Transactional
    public void upload(Progress<ImportResult> progress, byte[] data, String identifierType,
                       int clusterIdColumn, int clusterNameColumn, int identifierColumn, int nameColumn,
                       int priorityColumn, int remainingColumn, int effortColumn, Integer givenSheetNumber) {
    	
    	ReferencePoolEntity referencePool = ReferencePoolEntity.find("name", identifierType).firstResult();
    	
        try (ByteArrayInputStream stream = new ByteArrayInputStream(data);
             XSSFWorkbook wb = new XSSFWorkbook(stream)) {
            XSSFSheet sheet = wb.getSheetAt(getSheetNumber(givenSheetNumber));

            progress.max = sheet.getLastRowNum();
            progress.message = "Importing requirements ...";

            for (int i = getFirstRowIndexHavingMaxWidth(sheet) + 1; i < sheet.getLastRowNum(); i++) {
                Row row = sheet.getRow(i);
                progress.value = i;

                if (i % 100 == 0) {
                    entityManager.flush();
                    entityManager.getTransaction().commit();
                    entityManager.clear();
                    entityManager.getTransaction().begin();
                    Log.debugv("Imported {0} requirements", i);
                    progress.message = "Importing requirement " + i;
                }

                // No row, cannot continue
                if (row == null) {
                    continue;
                }

                RequirementEntity requirement;
                {
                	String identifier = getValue(row, identifierColumn, "identifier");
                    requirement = getRequirementEntity(referencePool, identifier);
                    if (requirement == null) {
                    	continue;
                    }
                }

                requirement.name = getValue(row, nameColumn, "title");
                
                RequirementClusterEntity cluster;
                {
                	String clusterId = getValue(row, clusterIdColumn, "clusterId");
                	String clusterName = getValue(row, clusterNameColumn, "clusterName");
                	if (clusterId == null || clusterName == null) {
                		throw new BadArgumentException("No cluster provided");
                	}
                    cluster = getRequirementClusterEntity(referencePool, clusterId, clusterName);
                }
                requirement.cluster = cluster;
                
                String priority = getValue(row, priorityColumn, "priority");
                Priority p = null;
                if (priority != null) {
                	p = Priority.valueOf(priority);
                }
				requirement.priority = p;
                String effort = getValue(row, effortColumn, "effort");
				requirement.effort = effort != null ? Integer.parseInt(effort) : 0;
                String remaining = getValue(row, remainingColumn, "remaining");
				requirement.remaining = remaining != null ? Integer.valueOf(remaining) : null;

                requirement.persist();
            }
            progress.message = "Import completed";
            progress.completed = true;
            progress.success = true;
        } catch (IOException | RuntimeException e) {
            Log.error(e.getMessage(), e);
            progress.message = (e.getMessage());
            progress.completed = true;
            progress.success = false;
            if (e instanceof WebApplicationException) {
                throw (WebApplicationException) e;
            }
            throw new BadArgumentException(e.getMessage());
        }
    }

    private String getValue(@Nonnull Row row, @Nullable Integer column, @Nonnull String columnName) {
        if (column == null) {
            return null;
        }

        Cell cell = row.getCell(column);
        if (cell == null) {
            return null;
        }

        CellType columnType = cell.getCellType();
        if (columnType == CellType.STRING ||
                columnType == CellType.BLANK) {
            return cell.getStringCellValue();
        } else if (columnType == CellType.NUMERIC) {
            return Integer.toString((int) cell.getNumericCellValue());
        } else {
            Log.warnv("Skipping {0} {1}", columnName, columnType);
        }
        return null;
    }

    RequirementEntity getRequirementEntity(ReferencePoolEntity referencePool, @Nonnull String value) {
        if (value.isEmpty()) {
            return null;
        } else {
            RequirementEntity requirement = RequirementEntity.findByReference(referencePool, value);
            if (requirement == null) {
                requirement = new RequirementEntity();
                ReferenceEntity reference = new ReferenceEntity();
                reference.pool = referencePool;
                reference.referenceKey = value;
				requirement.references.add(reference);
            }
            return requirement;
        }
    }

    RequirementClusterEntity getRequirementClusterEntity(ReferencePoolEntity referencePool, @Nonnull String value, @Nonnull String name) {
        if (value.isEmpty()) {
            return null;
        } else {
            RequirementClusterEntity requirementCluster = RequirementClusterEntity.findByReference(referencePool, value);
            if (requirementCluster == null) {
                requirementCluster = new RequirementClusterEntity();
                ReferenceEntity reference = new ReferenceEntity();
                reference.pool = referencePool;
                reference.referenceKey = value;
				requirementCluster.references.add(reference);
				requirementCluster.name = name;
            }
            return requirementCluster;
        }
    }

}
