package de.christophbrill.capacity.util;

import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.capacity.persistence.model.ImportProcessEntity;
import de.christophbrill.capacity.persistence.selector.ImportProcessSelector;
import de.christophbrill.capacity.ui.dto.Column;
import de.christophbrill.capacity.ui.dto.ImportConfiguration;
import io.quarkus.logging.Log;
import jakarta.persistence.EntityManager;
import jakarta.ws.rs.WebApplicationException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jboss.resteasy.reactive.multipart.FileUpload;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class XlsxImportTools {

    private static class ValueComparator<K, V extends Comparable<V>> implements Comparator<K> {
        private final Map<K, V> map;

        public ValueComparator(Map<K, V> base) {
            this.map = base;
        }

        @Override
        public int compare(K o1, K o2) {
            return map.get(o2).compareTo(map.get(o1));
        }
    }

    public static ImportConfiguration analyzeUpload(EntityManager em,
                                                    FileUpload file,
                                                    String filename,
                                                    Integer sheet) {
        try {
            filename = filename.toLowerCase();

            List<Column> columns = new ArrayList<>();
            XlsxImportTools.checkForUpload(Files.newInputStream(file.uploadedFile()), columns, sheet);

            ImportProcessEntity importprocess = new ImportProcessSelector(em)
                    .withFilename(filename)
                    .withSortColumn("created", false)
                    .withLimit(1)
                    .find();

            ImportConfiguration result = new ImportConfiguration();
            result.columns = columns;
            if (importprocess != null) {
                result.configuration = importprocess.columns;
                result.identifierColumn = importprocess.identifierColumn;
                result.identifierType = importprocess.identifierType;
                result.sheet = importprocess.sheet;
            }

            return result;
        } catch (IOException | RuntimeException e) {
            if (e instanceof WebApplicationException) {
                throw (WebApplicationException) e;
            }
            Log.warn(e.getMessage(), e);
            throw new BadArgumentException(e.getMessage());
        }
    }

    private static Row getFirstRowHavingMaxWidth(XSSFSheet sheet) {
        int mostused = getMostUsedWidth(sheet);

        for (Row row : sheet) {
            if (row == null) {
                continue;
            }
            if (row.getLastCellNum() >= mostused) {
                return row;
            }
        }
        return null;
    }

    private static int getMostUsedWidth(XSSFSheet sheet) {
        Map<Short, Integer> widths = new HashMap<>();
        for (Row row : sheet) {
            Short lastCellNum = row.getLastCellNum();
            widths.merge(lastCellNum, 1, Integer::sum);
        }

        ValueComparator<Short, Integer> comparator = new ValueComparator<>(widths);
        Map<Short, Integer> sortedMap = new TreeMap<>(comparator);
        sortedMap.putAll(widths);
        List<Short> sortedList = new ArrayList<>(sortedMap.keySet());
        return sortedList.getFirst();
    }

    public static int getFirstRowIndexHavingMaxWidth(XSSFSheet sheet) {
        int mostused = getMostUsedWidth(sheet);

        for (int i = 0; i < sheet.getLastRowNum(); i++) {
            Row row = sheet.getRow(i);
            if (row == null) {
                continue;
            }
            if (row.getLastCellNum() >= mostused) {
                return i;
            }
        }
        return -1;
    }

    protected static int getSheetNumber(Integer sheet) {
        if (sheet == null) {
            sheet = 1;
        }
        return sheet - 1;
    }

    public static void checkForUpload(InputStream stream, List<Column> result, Integer givenSheetNumber) throws IOException {
        try (XSSFWorkbook wb = new XSSFWorkbook(stream)) {
            XSSFSheet sheet = wb.getSheetAt(getSheetNumber(givenSheetNumber));
            Row row = getFirstRowHavingMaxWidth(sheet);
            if (row == null) {
                throw new BadArgumentException("No content found, make sure to have the first and second column filled");
            }
            for (int i = 0; i < row.getLastCellNum(); i++) {
                Cell cell = row.getCell(i);
                if (cell == null) {
                    continue;
                }
                CellType type = cell.getCellType();
                if (type == CellType.BLANK) {
                    continue;
                }
                if (type == CellType.STRING) {
                    if (!cell.getStringCellValue().isEmpty()) {
                        result.add(new Column(i, cell.getStringCellValue()));
                    }
                } else if (type == CellType.NUMERIC) {
                    Log.warn("Skipping column ...");
                } else {
                    throw new BadArgumentException("Not handled " + type);
                }
            }
        }
    }

}
