/*
 * Copyright 2013  Christoph Brill <egore911@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.christophbrill.capacity.util.listener;

import de.christophbrill.appframework.persistence.model.UserEntity;
import de.christophbrill.appframework.util.CascadeDeletionRegistry;
import de.christophbrill.appframework.util.listener.AbstractStartupService;
import de.christophbrill.capacity.persistence.deletion.EmployeeEpisodeDeletion;
import de.christophbrill.capacity.persistence.deletion.LabelEmployeeDeletion;
import de.christophbrill.capacity.persistence.deletion.LocationHolidayDeletion;
import de.christophbrill.capacity.persistence.deletion.UserAbsenceDeletion;
import de.christophbrill.capacity.persistence.deletion.UserEmployeeDeletion;
import de.christophbrill.capacity.persistence.deletion.UserWidgetDeletion;
import de.christophbrill.capacity.persistence.model.EmployeeEntity;
import de.christophbrill.capacity.persistence.model.LabelEntity;
import de.christophbrill.capacity.persistence.model.LocationEntity;
import de.christophbrill.capacity.persistence.model.Permission;

import jakarta.enterprise.context.ApplicationScoped;

/**
 * Listener executed during startup, responsible for setting up the
 * java.util.Logging->SLF4J bridge and updating the database.
 *
 * @author Christoph Brill &lt;egore911@gmail.com&gt;
 */
@ApplicationScoped
public class StartupListener extends AbstractStartupService {

	@Override
	protected Enum<?>[] getPermissions() {
		return Permission.values();
	}

	@Override
	protected void initCascadeDeletions() {
		CascadeDeletionRegistry.addCascadeDeletion(EmployeeEntity.class, new EmployeeEpisodeDeletion());
		CascadeDeletionRegistry.addCascadeDeletion(UserEntity.class, new UserEmployeeDeletion());
		CascadeDeletionRegistry.addCascadeDeletion(UserEntity.class, new UserAbsenceDeletion());
		CascadeDeletionRegistry.addCascadeDeletion(UserEntity.class, new UserWidgetDeletion());
		CascadeDeletionRegistry.addCascadeDeletion(LocationEntity.class, new LocationHolidayDeletion());
		CascadeDeletionRegistry.addCascadeDeletion(LabelEntity.class, new LabelEmployeeDeletion());
	}

}
