package de.christophbrill.capacity.util.mappers;

import de.christophbrill.appframework.mapping.ResourceMapper;
import de.christophbrill.capacity.persistence.model.ReferenceEntity;
import de.christophbrill.capacity.persistence.model.ReferencePoolEntity;
import de.christophbrill.capacity.ui.dto.Reference;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;
import java.util.Set;

@Mapper
public interface ReferenceMapper extends ResourceMapper<Reference, ReferenceEntity> {

    ReferenceMapper INSTANCE = Mappers.getMapper(ReferenceMapper.class);

    @Override
    @Mapping(target = "poolId", ignore = true)
    Reference mapEntityToDto(ReferenceEntity referenceEntity);

    @AfterMapping
    default void customEntityToDto(ReferenceEntity entity, @MappingTarget Reference dto) {
        if (entity.pool != null) {
            dto.poolId = entity.pool.id;
        } else {
            dto.poolId = null;
        }
    }

    @Override
    @Mapping(target = "pool", ignore = true)
    ReferenceEntity mapDtoToEntity(Reference reference);

    @AfterMapping
    default void customDtoToEntity(Reference dto, @MappingTarget ReferenceEntity entity) {
        if (dto.poolId != null) {
            entity.pool = ReferencePoolEntity.findById(dto.poolId);
        } else {
            entity.pool = null;
        }
    }

    default void mapEntitiesToDtos(Set<ReferenceEntity> entities, @MappingTarget List<Reference> dtos) {
        dtos.clear();
        for (ReferenceEntity entity : entities) {
            dtos.add(mapEntityToDto(entity));
        }
    }

    default void mapDtosToEntities(List<Reference> dtos, @MappingTarget Set<ReferenceEntity> entities) {
        entities.clear();
        for (Reference dto : dtos) {
            entities.add(mapDtoToEntity(dto));
        }
    }

}
