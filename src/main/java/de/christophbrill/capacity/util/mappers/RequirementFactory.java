package de.christophbrill.capacity.util.mappers;

import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.capacity.persistence.model.RequirementEntity;
import de.christophbrill.capacity.ui.dto.Requirement;
import org.mapstruct.ObjectFactory;

public class RequirementFactory {

    @ObjectFactory
    public RequirementEntity createEntity(Requirement dto) {
        if (dto != null && dto.id != null) {
            RequirementEntity entity = RequirementEntity.findById(dto.id);
            if (entity == null) {
                throw new BadArgumentException("Requirement with ID " + dto.id + " not found");
            }
            return entity;
        }
        return new RequirementEntity();
    }

}
