package de.christophbrill.capacity.util.mappers;

import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.capacity.persistence.model.LabelEntity;
import de.christophbrill.capacity.ui.dto.Label;
import org.mapstruct.ObjectFactory;

public class LabelFactory {

    @ObjectFactory
    public LabelEntity createEntity(Label dto) {
        if (dto != null && dto.id != null) {
            LabelEntity entity = LabelEntity.findById(dto.id);
            if (entity == null) {
                throw new BadArgumentException("Label with ID " + dto.id + " not found");
            }
            return entity;
        }
        return new LabelEntity();
    }

}
