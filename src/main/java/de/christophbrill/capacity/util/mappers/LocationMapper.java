package de.christophbrill.capacity.util.mappers;

import de.christophbrill.appframework.mapping.ResourceMapper;
import de.christophbrill.capacity.persistence.model.LocationEntity;
import de.christophbrill.capacity.ui.dto.Location;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(uses = LocationFactory.class)
public interface LocationMapper extends ResourceMapper<Location, LocationEntity> {

    LocationMapper INSTANCE = Mappers.getMapper(LocationMapper.class);

    @Override
    @Mapping(target = "modifiedBy", ignore = true)
    @Mapping(target = "modified", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "created", ignore = true)
    @Mapping(target = "holidays", ignore = true)
    @Mapping(target = "employees", ignore = true)
    LocationEntity mapDtoToEntity(Location location);

}
