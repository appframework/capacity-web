package de.christophbrill.capacity.util.mappers;

import de.christophbrill.appframework.mapping.ResourceMapper;
import de.christophbrill.capacity.persistence.model.HolidayEntity;
import de.christophbrill.capacity.persistence.model.IcalImportEntity;
import de.christophbrill.capacity.persistence.model.LocationEntity;
import de.christophbrill.capacity.ui.dto.Holiday;
import org.apache.commons.collections4.CollectionUtils;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Mapper(uses = HolidayFactory.class)
public interface HolidayMapper extends ResourceMapper<Holiday, HolidayEntity> {

    HolidayMapper INSTANCE = Mappers.getMapper(HolidayMapper.class);

    @Override
    @Mapping(target = "locationIds", ignore = true)
    @Mapping(target = "icalImportId", ignore = true)
    Holiday mapEntityToDto(HolidayEntity holidayEntity);

    @AfterMapping
    default void customEntityToDto(HolidayEntity entity, @MappingTarget Holiday dto) {
        if (entity.locations != null) {
            dto.locationIds = entity.locations.stream().map((l) -> l.id).collect(Collectors.toList());
        } else {
            dto.locationIds = Collections.emptyList();
        }
        dto.icalImportId = entity.icalImport != null ? entity.icalImport.id : null;
    }

    @Override
    @Mapping(target = "modifiedBy", ignore = true)
    @Mapping(target = "modified", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "created", ignore = true)
    @Mapping(target = "locations", ignore = true)
    @Mapping(target = "icalImport", ignore = true)
    HolidayEntity mapDtoToEntity(Holiday holiday);

    @AfterMapping
    default void customDtoToEntity(Holiday dto, @MappingTarget HolidayEntity entity) {
        if (CollectionUtils.isNotEmpty(dto.locationIds)) {
            List<LocationEntity> projects = LocationEntity.findByIds(dto.locationIds);
            if (entity.locations == null) {
                entity.locations = projects;
            } else {
                entity.locations.clear();
                entity.locations.addAll(projects);
            }
        } else {
            if (entity.locations != null) {
                entity.locations.clear();
            }
        }
        if (dto.icalImportId != null) {
            entity.icalImport = IcalImportEntity.findById(dto.icalImportId);
        } else {
            entity.icalImport = null;
        }
    }

}
