package de.christophbrill.capacity.util.mappers;

import de.christophbrill.appframework.mapping.ResourceMapper;
import de.christophbrill.capacity.persistence.model.RequirementClusterEntity;
import de.christophbrill.capacity.persistence.model.RequirementEntity;
import de.christophbrill.capacity.ui.dto.Requirement;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.HashSet;

@Mapper(uses = {RequiredAbilityMapper.class, ReferenceMapper.class, RequirementFactory.class})
public interface RequirementMapper extends ResourceMapper<Requirement, RequirementEntity> {

    RequirementMapper INSTANCE = Mappers.getMapper(RequirementMapper.class);

    @Override
    @Mapping(target = "clusterId", ignore = true)
    Requirement mapEntityToDto(RequirementEntity requirementEntity);

    @AfterMapping
    default void customEntityToDto(RequirementEntity entity, @MappingTarget Requirement dto) {
        dto.clusterId = entity.cluster.id;
    }

    @Override
    @Mapping(target = "created", ignore = true)
    @Mapping(target = "modified", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "modifiedBy", ignore = true)
    @Mapping(target = "requiredAbilities", ignore = true)
    @Mapping(target = "cluster", ignore = true)
    RequirementEntity mapDtoToEntity(Requirement dto);

    @AfterMapping
    default void customDtoToEntity(Requirement dto, @MappingTarget RequirementEntity entity) {
        entity.cluster = RequirementClusterEntity.findById(dto.clusterId);

        if (entity.requiredAbilities == null) {
            entity.requiredAbilities = new HashSet<>();
        }
        RequiredAbilityMapper.INSTANCE.mapDtosToEntities(dto.requiredAbilities, entity.requiredAbilities);
        entity.requiredAbilities
                .forEach((s) -> s.requirement = entity);
    }

}
