package de.christophbrill.capacity.util.mappers;

import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.capacity.persistence.model.LocationEntity;
import de.christophbrill.capacity.ui.dto.Location;
import org.mapstruct.ObjectFactory;

public class LocationFactory {

    @ObjectFactory
    public LocationEntity createEntity(Location dto) {
        if (dto != null && dto.id != null) {
            LocationEntity entity = LocationEntity.findById(dto.id);
            if (entity == null) {
                throw new BadArgumentException("Location with ID " + dto.id + " not found");
            }
            return entity;
        }
        return new LocationEntity();
    }

}
