package de.christophbrill.capacity.util.mappers;

import de.christophbrill.appframework.mapping.ResourceMapper;
import de.christophbrill.capacity.persistence.model.IcalImportEntity;
import de.christophbrill.capacity.persistence.model.Permission;
import de.christophbrill.capacity.ui.dto.IcalImport;
import io.quarkus.security.identity.CurrentIdentityAssociation;
import jakarta.enterprise.inject.spi.CDI;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

@Mapper(uses = IcalImportFactory.class)
public interface IcalImportMapper extends ResourceMapper<IcalImport, IcalImportEntity> {

    IcalImportMapper INSTANCE = Mappers.getMapper(IcalImportMapper.class);

    @Override
    @Mapping(target = "modifiedBy", ignore = true)
    @Mapping(target = "modified", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "created", ignore = true)
    IcalImportEntity mapDtoToEntity(IcalImport icalImport);

    default void afterEntityToDto(@MappingTarget IcalImport dto, IcalImportEntity entity) {
        CurrentIdentityAssociation identityAssociation = CDI.current().select(CurrentIdentityAssociation.class).get();
        var identity = identityAssociation.getIdentity();
        if (!identity.hasRole(Permission.ADMIN_ICAL_IMPORTS.name())) {
            dto.password = null;
        }
    }

}
