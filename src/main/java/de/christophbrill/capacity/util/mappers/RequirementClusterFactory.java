package de.christophbrill.capacity.util.mappers;

import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.capacity.persistence.model.RequirementClusterEntity;
import de.christophbrill.capacity.ui.dto.RequirementCluster;
import org.mapstruct.ObjectFactory;

public class RequirementClusterFactory {

    @ObjectFactory
    public RequirementClusterEntity createEntity(RequirementCluster dto) {
        if (dto != null && dto.id != null) {
            RequirementClusterEntity entity = RequirementClusterEntity.findById(dto.id);
            if (entity == null) {
                throw new BadArgumentException("RequirementCluster with ID " + dto.id + " not found");
            }
            return entity;
        }
        return new RequirementClusterEntity();
    }

}
