package de.christophbrill.capacity.util.mappers;

import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.capacity.persistence.model.ReferencePoolEntity;
import de.christophbrill.capacity.ui.dto.ReferencePool;
import org.mapstruct.ObjectFactory;

public class ReferencePoolFactory {

    @ObjectFactory
    public ReferencePoolEntity createEntity(ReferencePool dto) {
        if (dto != null && dto.id != null) {
            ReferencePoolEntity entity = ReferencePoolEntity.findById(dto.id);
            if (entity == null) {
                throw new BadArgumentException("ReferencePool with ID " + dto.id + " not found");
            }
            return entity;
        }
        return new ReferencePoolEntity();
    }

}
