package de.christophbrill.capacity.util.mappers;

import de.christophbrill.appframework.mapping.ResourceMapper;
import de.christophbrill.capacity.persistence.model.AbsenceEntity;
import de.christophbrill.capacity.persistence.model.EmployeeEntity;
import de.christophbrill.capacity.ui.dto.Absence;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

@Mapper(uses = AbsenceFactory.class)
public interface AbsenceMapper extends ResourceMapper<Absence, AbsenceEntity> {

    AbsenceMapper INSTANCE = Mappers.getMapper(AbsenceMapper.class);

    @Override
    @Mapping(target = "modifiedBy", ignore = true)
    @Mapping(target = "modified", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "created", ignore = true)
    @Mapping(target = "employee", ignore = true)
    @Mapping(target = "icalImport", ignore = true)
    AbsenceEntity mapDtoToEntity(Absence absence);

    @AfterMapping
    default void customDtoToEntity(Absence dto, @MappingTarget AbsenceEntity entity) {
        entity.employee = EmployeeEntity.findById(dto.employeeId);
    }

    @Override
    @Mapping(target = "employeeId", ignore = true)
    @Mapping(target = "imported", ignore = true)
    Absence mapEntityToDto(AbsenceEntity absenceEntity);

    @AfterMapping
    default void customEntityToDto(AbsenceEntity entity, @MappingTarget Absence dto) {
        dto.employeeId = entity.employee.id;
        dto.imported = entity.icalImport != null;
    }

}
