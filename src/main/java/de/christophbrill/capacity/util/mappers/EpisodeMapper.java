package de.christophbrill.capacity.util.mappers;

import de.christophbrill.appframework.mapping.ResourceMapper;
import de.christophbrill.capacity.persistence.model.EmployeeEpisodeEntity;
import de.christophbrill.capacity.persistence.model.EpisodeEntity;
import de.christophbrill.capacity.ui.dto.Episode;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.ArrayList;

@Mapper(uses = {EmployeeEpisodeMapper.class, EpisodeFactory.class})
public interface EpisodeMapper extends ResourceMapper<Episode, EpisodeEntity> {

    EpisodeMapper INSTANCE = Mappers.getMapper(EpisodeMapper.class);

    @Override
    @Mapping(target = "modifiedBy", ignore = true)
    @Mapping(target = "modified", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "created", ignore = true)
    @Mapping(target = "employeeEpisodes", ignore = true)
    EpisodeEntity mapDtoToEntity(Episode episode);

    @AfterMapping
    default void customDtoToEntity(Episode dto, @MappingTarget EpisodeEntity entity) {
        if (entity.employeeEpisodes == null) {
            entity.employeeEpisodes = new ArrayList<>();
        }
        EmployeeEpisodeMapper.INSTANCE.mapDtosToEntities(dto.employeeEpisodes, entity.employeeEpisodes);
        for (EmployeeEpisodeEntity employeeEpisodeEntity : entity.employeeEpisodes) {
            employeeEpisodeEntity.episode = entity;
        }
    }

}
