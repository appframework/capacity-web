package de.christophbrill.capacity.util.mappers;

import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.capacity.persistence.model.EmployeeEntity;
import de.christophbrill.capacity.ui.dto.Employee;
import org.mapstruct.ObjectFactory;

public class EmployeeFactory {

    @ObjectFactory
    public EmployeeEntity createEntity(Employee dto) {
        if (dto != null && dto.id != null) {
            EmployeeEntity entity = EmployeeEntity.findById(dto.id);
            if (entity == null) {
                throw new BadArgumentException("Employee with ID " + dto.id + " not found");
            }
            return entity;
        }
        return new EmployeeEntity();
    }

}
