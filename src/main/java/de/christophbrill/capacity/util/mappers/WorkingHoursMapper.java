package de.christophbrill.capacity.util.mappers;

import de.christophbrill.appframework.mapping.ResourceMapper;
import de.christophbrill.capacity.persistence.model.WorkingHoursEntity;
import de.christophbrill.capacity.ui.dto.WorkingHours;
import org.mapstruct.Mapper;

@Mapper
public interface WorkingHoursMapper  extends ResourceMapper<WorkingHours, WorkingHoursEntity> {
}
