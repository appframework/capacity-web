package de.christophbrill.capacity.util.mappers;

import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.capacity.persistence.model.WidgetEntity;
import de.christophbrill.capacity.ui.dto.Widget;
import org.mapstruct.ObjectFactory;

public class WidgetFactory {

    @ObjectFactory
    public WidgetEntity createEntity(Widget dto) {
        if (dto != null && dto.id != null) {
            WidgetEntity entity = WidgetEntity.findById(dto.id);
            if (entity == null) {
                throw new BadArgumentException("Widget with ID " + dto.id + " not found");
            }
            return entity;
        }
        return new WidgetEntity();
    }

}
