package de.christophbrill.capacity.util.mappers;

import de.christophbrill.appframework.mapping.ResourceMapper;
import de.christophbrill.capacity.persistence.model.ReferencePoolEntity;
import de.christophbrill.capacity.ui.dto.ReferencePool;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(uses = ReferencePoolFactory.class)
public interface ReferencePoolMapper extends ResourceMapper<ReferencePool, ReferencePoolEntity> {

    ReferencePoolMapper INSTANCE = Mappers.getMapper(ReferencePoolMapper.class);

    @Override
    @Mapping(target = "modifiedBy", ignore = true)
    @Mapping(target = "modified", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "created", ignore = true)
    ReferencePoolEntity mapDtoToEntity(ReferencePool referencePool);
}
