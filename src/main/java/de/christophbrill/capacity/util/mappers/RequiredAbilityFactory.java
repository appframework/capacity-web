package de.christophbrill.capacity.util.mappers;

import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.capacity.persistence.model.RequiredAbilityEntity;
import de.christophbrill.capacity.ui.dto.RequiredAbility;
import org.mapstruct.ObjectFactory;

public class RequiredAbilityFactory {

    @ObjectFactory
    public RequiredAbilityEntity createEntity(RequiredAbility dto) {
        if (dto != null && dto.id != null) {
            RequiredAbilityEntity entity = RequiredAbilityEntity.findById(dto.id);
            if (entity == null) {
                throw new BadArgumentException("RequiredAbility with ID " + dto.id + " not found");
            }
            return entity;
        }
        return new RequiredAbilityEntity();
    }

}
