package de.christophbrill.capacity.util.mappers;

import de.christophbrill.appframework.mapping.ResourceMapper;
import de.christophbrill.capacity.persistence.model.RequirementClusterEntity;
import de.christophbrill.capacity.ui.dto.RequirementCluster;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(uses = {ReferenceMapper.class, RequirementClusterFactory.class})
public interface RequirementClusterMapper extends ResourceMapper<RequirementCluster, RequirementClusterEntity> {

    RequirementClusterMapper INSTANCE = Mappers.getMapper(RequirementClusterMapper.class);

    @Override
    @Mapping(target = "modifiedBy", ignore = true)
    @Mapping(target = "modified", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "created", ignore = true)
    @Mapping(target = "requirements", ignore = true)
    RequirementClusterEntity mapDtoToEntity(RequirementCluster requirementCluster);

}
