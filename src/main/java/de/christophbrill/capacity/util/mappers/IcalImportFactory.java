package de.christophbrill.capacity.util.mappers;

import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.capacity.persistence.model.IcalImportEntity;
import de.christophbrill.capacity.ui.dto.IcalImport;
import org.mapstruct.ObjectFactory;

public class IcalImportFactory {

    @ObjectFactory
    public IcalImportEntity createEntity(IcalImport dto) {
        if (dto != null && dto.id != null) {
            IcalImportEntity entity = IcalImportEntity.findById(dto.id);
            if (entity == null) {
                throw new BadArgumentException("IcalImport with ID " + dto.id + " not found");
            }
            return entity;
        }
        return new IcalImportEntity();
    }

}
