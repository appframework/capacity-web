package de.christophbrill.capacity.util.mappers;

import de.christophbrill.appframework.mapping.ResourceMapper;
import de.christophbrill.capacity.persistence.model.ContractEntity;
import de.christophbrill.capacity.persistence.model.WorkingHoursEntity;
import de.christophbrill.capacity.ui.dto.Contract;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.time.temporal.ChronoUnit;

@Mapper(uses = WorkingHoursMapper.class)
public interface ContractMapper extends ResourceMapper<Contract, ContractEntity> {

    @Override
    @Mapping(target = "workingHoursPerWeek", ignore = true)
    Contract mapEntityToDto(ContractEntity contractEntity);

    @AfterMapping
    default void customEntityToDto(ContractEntity entity, @MappingTarget Contract dto) {
        if (entity.workingHours != null) {
            int workingHoursPerWeek = 0;
            for (WorkingHoursEntity workingHoursEntity : entity.workingHours) {
                workingHoursPerWeek += (int) workingHoursEntity.start
                        .until(workingHoursEntity.end, ChronoUnit.HOURS);
            }
            dto.workingHoursPerWeek = workingHoursPerWeek;
        }
    }

}
