package de.christophbrill.capacity.util.mappers;

import de.christophbrill.appframework.mapping.ResourceMapper;
import de.christophbrill.capacity.persistence.model.WidgetEntity;
import de.christophbrill.capacity.ui.dto.Widget;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(uses = {WidgetFactory.class, RequirementFactory.class})
public interface WidgetMapper extends ResourceMapper<Widget, WidgetEntity> {

    WidgetMapper INSTANCE = Mappers.getMapper(WidgetMapper.class);

    @Override
    @Mapping(target = "created", ignore = true)
    @Mapping(target = "modified", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "modifiedBy", ignore = true)
    WidgetEntity mapDtoToEntity(Widget widget);
}
