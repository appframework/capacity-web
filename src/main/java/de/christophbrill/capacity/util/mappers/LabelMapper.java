package de.christophbrill.capacity.util.mappers;

import de.christophbrill.appframework.mapping.ResourceMapper;
import de.christophbrill.capacity.persistence.model.LabelEntity;
import de.christophbrill.capacity.ui.dto.Label;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValueCheckStrategy;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.factory.Mappers;

@Mapper(uses = LabelFactory.class)
public interface LabelMapper extends ResourceMapper<Label, LabelEntity> {

    LabelMapper INSTANCE = Mappers.getMapper(LabelMapper.class);

    @Override
    @Mapping(target = "modifiedBy", ignore = true)
    @Mapping(target = "modified", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "created", ignore = true)
    @Mapping(target = "color", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS, nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    @Mapping(target = "name", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS, nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    LabelEntity mapDtoToEntity(Label label);

}
