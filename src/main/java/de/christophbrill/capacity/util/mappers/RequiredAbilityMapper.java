package de.christophbrill.capacity.util.mappers;

import de.christophbrill.appframework.mapping.ResourceMapper;
import de.christophbrill.capacity.persistence.model.RequiredAbilityEntity;
import de.christophbrill.capacity.ui.dto.RequiredAbility;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;
import java.util.Set;

@Mapper(uses = {RequiredAbilityFactory.class, LabelMapper.class})
public interface RequiredAbilityMapper extends ResourceMapper<RequiredAbility, RequiredAbilityEntity> {

    RequiredAbilityMapper INSTANCE = Mappers.getMapper(RequiredAbilityMapper.class);

    @Override
    RequiredAbility mapEntityToDto(RequiredAbilityEntity entity);

    void mapDtosToEntities(List<RequiredAbility> dtos, @MappingTarget Set<RequiredAbilityEntity> entities);

    @Override
    @Mapping(target = "modifiedBy", ignore = true)
    @Mapping(target = "modified", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "created", ignore = true)
    @Mapping(target = "requirement", ignore = true)
    RequiredAbilityEntity mapDtoToEntity(RequiredAbility dto);

}
