package de.christophbrill.capacity.util.mappers;

import de.christophbrill.appframework.mapping.ResourceMapper;
import de.christophbrill.capacity.persistence.model.EmployeeEntity;
import de.christophbrill.capacity.persistence.model.EmployeeEpisodeEntity;
import de.christophbrill.capacity.ui.dto.EmployeeEpisode;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

@Mapper(uses = EmployeeEpisodeFactory.class)
public interface EmployeeEpisodeMapper extends ResourceMapper<EmployeeEpisode, EmployeeEpisodeEntity> {

    EmployeeEpisodeMapper INSTANCE = Mappers.getMapper(EmployeeEpisodeMapper.class);

    @Mapping(target = "employeeId", ignore = true)
    @Override
    EmployeeEpisode mapEntityToDto(EmployeeEpisodeEntity entity);

    @AfterMapping
    default void customEntityToDto(EmployeeEpisodeEntity entity, @MappingTarget EmployeeEpisode dto) {
        dto.employeeId = entity.employee.id;
    }

    @Override
    @Mapping(target = "modifiedBy", ignore = true)
    @Mapping(target = "modified", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "created", ignore = true)
    @Mapping(target = "employee", ignore = true)
    @Mapping(target = "episode", ignore = true)
    EmployeeEpisodeEntity mapDtoToEntity(EmployeeEpisode dto);

    @AfterMapping
    default void customDtoToEntity(EmployeeEpisode dto, @MappingTarget EmployeeEpisodeEntity entity) {
        if (dto.employeeId != null) {
            if (entity.employee == null || !dto.employeeId.equals(entity.employee.id)) {
                entity.employee = EmployeeEntity.findById(dto.employeeId);
            }
        }
    }

}
