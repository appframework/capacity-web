package de.christophbrill.capacity.util.mappers;

import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.capacity.persistence.model.EpisodeEntity;
import de.christophbrill.capacity.ui.dto.Episode;
import org.mapstruct.ObjectFactory;

public class EpisodeFactory {

    @ObjectFactory
    public EpisodeEntity createEntity(Episode dto) {
        if (dto != null && dto.id != null) {
            EpisodeEntity entity = EpisodeEntity.findById(dto.id);
            if (entity == null) {
                throw new BadArgumentException("Episode with ID " + dto.id + " not found");
            }
            return entity;
        }
        return new EpisodeEntity();
    }

}
