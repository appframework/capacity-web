package de.christophbrill.capacity.util.mappers;

import de.christophbrill.appframework.mapping.ResourceMapper;
import de.christophbrill.capacity.persistence.model.EmployeeEntity;
import de.christophbrill.capacity.persistence.model.LabelEntity;
import de.christophbrill.capacity.persistence.model.LocationEntity;
import de.christophbrill.capacity.ui.dto.Employee;
import de.christophbrill.capacity.ui.dto.Label;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.LinkedHashSet;
import java.util.Set;

@Mapper(uses = {ContractMapper.class, EmployeeFactory.class, LabelMapper.class})
public interface EmployeeMapper extends ResourceMapper<Employee, EmployeeEntity> {

    EmployeeMapper INSTANCE = Mappers.getMapper(EmployeeMapper.class);

    @Override
    @Mapping(target = "locationId", ignore = true)
    Employee mapEntityToDto(EmployeeEntity entity);

    @AfterMapping
    default void customEntityToDto(EmployeeEntity entity, @MappingTarget Employee dto) {
        dto.locationId = entity.location != null ? entity.location.id : null;
    }

    @Override
    @Mapping(target = "modifiedBy", ignore = true)
    @Mapping(target = "modified", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "created", ignore = true)
    @Mapping(target = "location", ignore = true)
    @Mapping(target = "absences", ignore = true)
    EmployeeEntity mapDtoToEntity(Employee dto);

    @AfterMapping
    default void customDtoToEntity(Employee dto, @MappingTarget EmployeeEntity entity) {
        entity.location = dto.locationId != null ? LocationEntity.findById(dto.locationId) : null;
    }

    default Set<LabelEntity> labelSetToLabelEntitySet(Set<Label> set) {
        if (set == null) {
            return null;
        }

        var labelFactory = new LabelFactory();

        var entities = new LinkedHashSet<LabelEntity>();
        for (Label label : set) {
            entities.add(labelFactory.createEntity(label));
        }

        return entities;
    }
}
