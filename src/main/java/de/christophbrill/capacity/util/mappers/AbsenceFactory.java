package de.christophbrill.capacity.util.mappers;

import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.capacity.persistence.model.AbsenceEntity;
import de.christophbrill.capacity.ui.dto.Absence;
import org.mapstruct.ObjectFactory;

public class AbsenceFactory {

    @ObjectFactory
    public AbsenceEntity createEntity(Absence dto) {
        if (dto != null && dto.id != null) {
            AbsenceEntity entity = AbsenceEntity.findById(dto.id);
            if (entity == null) {
                throw new BadArgumentException("Absence with ID " + dto.id + " not found");
            }
            return entity;
        }
        return new AbsenceEntity();
    }

}
