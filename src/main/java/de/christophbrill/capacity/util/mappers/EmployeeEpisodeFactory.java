package de.christophbrill.capacity.util.mappers;

import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.capacity.persistence.model.EmployeeEpisodeEntity;
import de.christophbrill.capacity.ui.dto.EmployeeEpisode;
import org.mapstruct.ObjectFactory;

public class EmployeeEpisodeFactory {

    @ObjectFactory
    public EmployeeEpisodeEntity createEntity(EmployeeEpisode dto) {
        if (dto != null && dto.id != null) {
            EmployeeEpisodeEntity entity = EmployeeEpisodeEntity.findById(dto.id);
            if (entity == null) {
                throw new BadArgumentException("EmployeeEpisode with ID " + dto.id + " not found");
            }
            return entity;
        }
        return new EmployeeEpisodeEntity();
    }

}
