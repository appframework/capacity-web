package de.christophbrill.capacity.util.mappers;

import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.capacity.persistence.model.HolidayEntity;
import de.christophbrill.capacity.ui.dto.Holiday;
import org.mapstruct.ObjectFactory;

public class HolidayFactory {

    @ObjectFactory
    public HolidayEntity createEntity(Holiday dto) {
        if (dto != null && dto.id != null) {
            HolidayEntity entity = HolidayEntity.findById(dto.id);
            if (entity == null) {
                throw new BadArgumentException("Holiday with ID " + dto.id + " not found");
            }
            return entity;
        }
        return new HolidayEntity();
    }

}
