package de.christophbrill.capacity.util;

import static de.christophbrill.capacity.util.XlsxImportTools.getFirstRowIndexHavingMaxWidth;
import static de.christophbrill.capacity.util.XlsxImportTools.getSheetNumber;

import de.christophbrill.appframework.ui.dto.ImportResult;
import de.christophbrill.appframework.ui.dto.Progress;
import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.capacity.persistence.model.ContractEntity;
import de.christophbrill.capacity.persistence.model.EmployeeEntity;
import de.christophbrill.capacity.persistence.model.LabelEntity;
import de.christophbrill.capacity.persistence.model.LocationEntity;
import de.christophbrill.capacity.persistence.model.WorkingHoursEntity;
import de.christophbrill.capacity.util.importer.EmployeesUpload;
import io.quarkus.logging.Log;
import jakarta.annotation.Nonnull;
import jakarta.annotation.Nullable;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Named;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.WebApplicationException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@ApplicationScoped
@Named
public class EmployeeImportXlsx {

    private static final String DEFAULT_COLOR = "000000";

    @Transactional
    public void upload(Progress<ImportResult> progress, byte[] data, int emailColumn, EmployeesUpload.Columns columns,
                       Integer givenSheetNumber) {

        try (ByteArrayInputStream stream = new ByteArrayInputStream(data);
             XSSFWorkbook wb = new XSSFWorkbook(stream)) {
            XSSFSheet sheet = wb.getSheetAt(getSheetNumber(givenSheetNumber));
            ImportResult result = new ImportResult();

            progress.max = sheet.getLastRowNum();
            progress.message = "Importing employees ...";

            System.err.println(sheet.getLastRowNum());

            for (int i = getFirstRowIndexHavingMaxWidth(sheet) + 1; i <= sheet.getLastRowNum(); i++) {
                Row row = sheet.getRow(i);
                progress.value = i;

                if (i % 10 == 0) {
                    Log.tracev("Imported {0} employees", i);
                    progress.message = "Imported " + i + " employees";
                }

                // No row, cannot continue
                if (row == null) {
                    result.skipped++;
                    Log.debugv("Skipping employee (row {0}), no row", i + 1);
                    continue;
                }

                String identifier = getValue(row, emailColumn, "email");
                if (identifier == null || identifier.isEmpty()) {
                    Log.debugv("Skipping employee (row {0}), no identifier", i + 1);
                    result.skipped++;
                    continue;
                }

                var employees = getEmployees(identifier);
                if (employees.isEmpty()) {
                    if (columns.name != null && columns.name.length > 0) {
                        Log.infov("Skipping employee {0} ({1})", getValue(row, columns.name[0], "name"), i + 1);
                    }
                    result.skipped++;
                    continue;
                }


                String name = getNameFromSheet(columns, row);
                Set<LabelEntity> abilities = getAbilitiesFromSheet(columns, row);
                LocationEntity location = getLocationFromSheet(columns, row);
                float fteFloat = getFteFromSheet(columns, row);

                EmployeeEntity employee = null;
                if (isSame(fteFloat, 1.0f)) {
                    // An employee can only occur once if its FTE is 1.0 in one line
                    employee = employees.getFirst();
                } else {
                    // Otherwise it can occur multiple times, where its FTE sums up to 1.0 (or less for part-time employees)
                    for (var emp : employees) {
                        if (emp.abilities.containsAll(abilities) || emp.id == null) {
                            employee = emp;
                            break;
                        }
                    }
                    // None of the existing ones matched to the labels, we create an additional one
                    if (employee == null) {
                        employee = initEmployee(identifier);
                    }
                }
                if (employee.id == null) {
                    result.created++;
                } else {
                    result.updated++;
                }
                employee.name = name;
                employee.abilities.addAll(abilities);
                employee.location = location;
                if (employee.contract.workingHours == null) {
                    employee.contract.workingHours = new ArrayList<>();
                }
                for (int d = 1; d < 6; d++) {
                    WorkingHoursEntity workingHours = null;
                    for (var existing : employee.contract.workingHours) {
                        if (existing.dayOfWeek == d) {
                            workingHours = existing;
                            break;
                        }
                    }
                    if (workingHours == null) {
                        workingHours = new WorkingHoursEntity();
                        workingHours.dayOfWeek = d;
                        employee.contract.workingHours.add(workingHours);
                    }
                    workingHours.start = LocalTime.of(8, 0);
                    workingHours.end = workingHours.start.plusMinutes((long) (fteFloat * 480));
                }
                if (employee.color.equals(DEFAULT_COLOR)) {
                    var optionalPrecedenceLabel = abilities.stream()
                            .filter(l -> l.precedence)
                            .findFirst();
                    if (optionalPrecedenceLabel.isPresent()) {
                        employee.color = optionalPrecedenceLabel.get().color;
                    }
                }

                employee.persist();
            }
            progress.result = result;
            progress.message = "Import completed";
            progress.completed = true;
            progress.success = true;
        } catch (IOException | RuntimeException e) {
            Log.error(e.getMessage(), e);
            progress.completed = true;
            progress.success = false;
            progress.message = e.getMessage();
            progress.result = new ImportResult();
            if (e instanceof WebApplicationException we) {
                throw we;
            }
            throw new BadArgumentException(e.getMessage());
        }
    }

    private float getFteFromSheet(EmployeesUpload.Columns columns, Row row) {
        float fteFloat = 1.0f;
        if (columns.fte != null && columns.fte.length > 0) {
            String fte = getValue(row, columns.fte[0], "fte");
            if (fte != null && !fte.isEmpty()) {

                fteFloat = Float.parseFloat(fte);
            }
        }
        return fteFloat;
    }

    private LocationEntity getLocationFromSheet(EmployeesUpload.Columns columns, Row row) {
        LocationEntity location = null;
        if (columns.location != null && columns.location.length > 0) {
            String locationValue = getValue(row, columns.location[0], "location");
            if (locationValue != null && !locationValue.isEmpty()) {
                Optional<LocationEntity> optionalLocation = LocationEntity.<LocationEntity>find("name", locationValue)
                        .singleResultOptional();
                if (optionalLocation.isPresent()) {
                    location = optionalLocation.get();
                } else {
                    Log.warnv("Unknown location {0}", locationValue);
                }
            }
        }
        return location;
    }

    private Set<LabelEntity> getAbilitiesFromSheet(EmployeesUpload.Columns columns, Row row) {
        Set<LabelEntity> abilities = new HashSet<>();
        if (columns.abilities != null) {
            for (var abilityColumn : columns.abilities) {
                String abilityValue = getValue(row, abilityColumn, "ability");
                if (abilityValue != null && !abilityValue.isEmpty()) {
                    for (var skill : abilityValue.split(",")) {
                        skill = skill.trim();

                        var optionalLabel = LabelEntity.<LabelEntity>find("select l from Label l where l.name = ?1 or ?1 in elements(l.alias)", skill)
                                .singleResultOptional();
                        LabelEntity label;
                        if (optionalLabel.isEmpty()) {
                            label = new LabelEntity();
                            label.name = skill;
                            label.color = DEFAULT_COLOR;
                            label.persist();
                        } else {
                            label = optionalLabel.get();
                        }
                        abilities.add(label);
                    }
                }
            }
        }
        return abilities;
    }

    private String getNameFromSheet(EmployeesUpload.Columns columns, Row row) {
        String name = "";
        if (columns.name != null) {
            StringBuilder buffer = new StringBuilder();
            for (var nameIdx : columns.name) {
                var nameValue = getValue(row, nameIdx, "name");
                if (nameValue != null) {
                    if (!buffer.isEmpty()) {
                        buffer.append(" ");
                    }
                    buffer.append(nameValue);
                }
            }
            name = buffer.toString();
        }
        return name;
    }

    private String getValue(@Nonnull Row row, @Nullable Integer column, @Nonnull String columnName) {
        if (column == null) {
            return null;
        }

        Cell cell = row.getCell(column);
        if (cell == null) {
            return null;
        }

        CellType columnType = cell.getCellType();
        if (columnType == CellType.STRING ||
                columnType == CellType.BLANK) {
            return cell.getStringCellValue();
        } else if (columnType == CellType.NUMERIC) {
            double numericCellValue = cell.getNumericCellValue();
            double rounded = Math.round(numericCellValue);
            if (isSame(numericCellValue, rounded)) {
                return Integer.toString((int) numericCellValue);
            } else {
                return Double.toString(numericCellValue);
            }
        } else {
            Log.warnv("Skipping {0} {1}", columnName, columnType);
        }
        return null;
    }

    private static boolean isSame(double a, double b) {
        return Math.abs(a - b) <= 0.0001;
    }

    @Nonnull
    List<EmployeeEntity> getEmployees(@Nonnull String email) {
        if (email.isEmpty()) {
            return Collections.emptyList();
        } else {
            var employees = EmployeeEntity.<EmployeeEntity>find("email", email).list();
            if (employees.isEmpty()) {
                return Collections.singletonList(initEmployee(email));
            }
            return employees;
        }
    }

    private static EmployeeEntity initEmployee(String email) {
        var employee = new EmployeeEntity();
        employee.email = email;
        employee.color = DEFAULT_COLOR;
        employee.contract = new ContractEntity();
        employee.velocity = 66;
        return employee;
    }

}
