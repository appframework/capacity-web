package de.christophbrill.capacity.util.importer;

import de.christophbrill.appframework.ui.dto.ImportResult;
import de.christophbrill.appframework.ui.dto.Progress;
import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.appframework.ui.exceptions.BadStateException;
import de.christophbrill.capacity.util.RequirementImportXlsx;
import io.quarkus.logging.Log;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import org.jboss.resteasy.reactive.multipart.FileUpload;
import java.io.IOException;
import java.nio.file.Files;

@ApplicationScoped
public class RequirementsUpload {

    public static class Columns {
        public int clusterIdColumn;
        public int clusterNameColumn;
        public int nameColumn;
        public int priorityColumn;
        public int remainingColumn;
        public int effortColumn;
    }

    @Inject
    RequirementImportXlsx requirementImportXlsx;

    @Transactional
    public void runImport(Progress<ImportResult> progress, FileUpload file, Columns columns, String identifierType, int identifierColumn, int sheet) {
        byte[] datax;
        try {
            datax = Files.readAllBytes(file.uploadedFile());
        } catch (IOException e) {
            throw new BadArgumentException(e.getMessage());
        }
        try {
            requirementImportXlsx.upload(progress, datax, identifierType,
                    columns.clusterIdColumn, columns.clusterNameColumn,
                    identifierColumn, columns.nameColumn,
                    columns.priorityColumn, columns.remainingColumn, columns.effortColumn, sheet);
        } catch (RuntimeException e) {
            Log.warn(e.getMessage(), e);
            progress.success = false;
            progress.message = e.getMessage();
            throw new BadStateException(e.getMessage());
        }
    }

}
