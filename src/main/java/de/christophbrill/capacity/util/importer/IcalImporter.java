package de.christophbrill.capacity.util.importer;

import biweekly.Biweekly;
import biweekly.ICalendar;
import biweekly.component.VEvent;
import biweekly.property.Attendee;
import biweekly.property.Summary;
import biweekly.property.Uid;
import de.christophbrill.appframework.ui.dto.Credentials;
import de.christophbrill.appframework.ui.dto.ImportResult;
import de.christophbrill.appframework.ui.dto.Progress;
import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.appframework.ui.exceptions.BadStateException;
import de.christophbrill.capacity.persistence.model.AbsenceEntity;
import de.christophbrill.capacity.persistence.model.EmployeeEntity;
import de.christophbrill.capacity.persistence.model.HolidayEntity;
import de.christophbrill.capacity.persistence.model.IcalImportEntity;
import de.christophbrill.capacity.persistence.selector.EmployeeSelector;
import de.christophbrill.capacity.persistence.selector.HolidaySelector;
import de.christophbrill.capacity.persistence.selector.IcalImportSelector;
import de.christophbrill.capacity.ui.dto.IcalImport;
import io.quarkus.logging.Log;
import org.apache.commons.lang3.StringUtils;

import jakarta.annotation.Nonnull;
import jakarta.annotation.Nullable;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.transaction.HeuristicMixedException;
import jakarta.transaction.HeuristicRollbackException;
import jakarta.transaction.NotSupportedException;
import jakarta.transaction.RollbackException;
import jakarta.transaction.SystemException;
import jakarta.transaction.Transactional;
import jakarta.transaction.UserTransaction;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Base64;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

@ApplicationScoped
public class IcalImporter {

    @Inject
    EntityManager em;
    @Inject
    UserTransaction transaction;

    public void runImport2(Progress<ImportResult> progress, Long icalImportId, ICalendar calendar) {
        Log.debug("Consuming import event");
        try {
            transaction.begin();

            IcalImportEntity icalImportEntity = new IcalImportSelector(em).withId(icalImportId).find();
            if (icalImportEntity == null) {
                throw new BadArgumentException("Given ID not found in database");
            }
            if (StringUtils.isNotEmpty(icalImportEntity.url)) {
                throw new BadArgumentException("Import has URL configured, not allowed to upload");
            }
            // This method detaches icalImportEntity, therefore we need to call merge below
            if (icalImportEntity.type == IcalImport.Type.ABSENCES) {
                importAbsencesFromIcal(icalImportEntity, calendar, progress);
            } else {
                importHolidaysFromIcal(icalImportEntity, calendar, progress);
            }
            icalImportEntity.lastImported = LocalDateTime.now();
            em.merge(icalImportEntity);

            transaction.commit();
        } catch (Exception e) {
            Log.error(e.getMessage(), e);
            progress.success = false;
            progress.message = e.getMessage();
            try {
                if (transaction.getStatus() != jakarta.transaction.Status.STATUS_MARKED_ROLLBACK) {
                    transaction.rollback();
                }
            } catch (SystemException ex) {
                Log.error("Failed to roll back");
            }
            throw new BadStateException(e.getMessage());
        } finally {
            progress.completed = true;
        }
    }

    public void runImport(Progress<ImportResult> progress, Long icalImportId, Credentials credentials) {
        Log.debug("Consuming import event");
        try {
            transaction.begin();

            IcalImportEntity icalImportEntity = new IcalImportSelector(em).withId(icalImportId).find();
            if (icalImportEntity == null) {
                throw new BadArgumentException("Given ID not found in database");
            }
            ICalendar calendar = loadCalendar(progress, icalImportEntity, credentials);
            // This method detaches icalImportEntity, therefore we need to call merge below
            if (icalImportEntity.type == IcalImport.Type.ABSENCES) {
                importAbsencesFromIcal(icalImportEntity, calendar, progress);
            } else {
                importHolidaysFromIcal(icalImportEntity, calendar, progress);
            }
            icalImportEntity.lastImported = LocalDateTime.now();
            em.merge(icalImportEntity);

            transaction.commit();
        } catch (Exception e) {
            Log.error(e.getMessage(), e);
            progress.success = false;
            progress.message = e.getMessage();
            try {
                if (transaction.getStatus() != jakarta.transaction.Status.STATUS_MARKED_ROLLBACK) {
                    transaction.rollback();
                }
            } catch (SystemException ex) {
                Log.error("Failed to roll back");
            }
            throw new BadStateException(e.getMessage());
        } finally {
            progress.completed = true;
        }
    }

    @Transactional
    public void importAbsencesFromIcal(@Nonnull IcalImportEntity icalImport, @Nonnull ICalendar calendar,
                                       @Nonnull Progress<ImportResult> progress) throws HeuristicRollbackException, SystemException, HeuristicMixedException, RollbackException, NotSupportedException {
        ImportResult result = new ImportResult();
        try {

            Map<String, List<EmployeeEntity>> employeeLookup = new HashMap<>();
            progress.message = "Importing events";
            List<VEvent> components = calendar.getEvents();
            progress.max = components.size();
            int i = 0;

            LocalDate minStart = LocalDate.now().minusYears(2);

            Set<Long> absenceIds = new HashSet<>();
            for (VEvent component : components) {
                if (i % 100 == 0) {
                    employeeLookup.clear();
                    transaction.commit();
                    transaction.begin();
                }
                Uid uidProperty = component.getUid();
                String uid = uidProperty != null ? uidProperty.getValue() : null;
                if (uid == null) {
                    Log.warn("Event did not have an UID, skipping");
                    result.skipped++;
                    progress.value = ++i;
                    continue;
                }
                List<Attendee> emailProperty = component.getAttendees();
                List<String> emails = null;
                if (emailProperty != null) {
                    emails = emailProperty.stream()
                            .filter(Objects::nonNull)
                            .map(Attendee::getEmail)
                            .map(email -> {
                                if (email.startsWith("mailto:")) {
                                    email = email.substring("mailto:".length());
                                }
                                return email.toLowerCase();
                            }).toList();
                }
                Summary summaryProperty = component.getSummary();
                String reason = summaryProperty != null ? summaryProperty.getValue() : "Unnamed";
                LocalDate start = component.getDateStart().getValue().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                LocalDate end = component.getDateEnd().getValue().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().minusDays(1);
                if (emails == null || emails.isEmpty()) {
                    if (!start.isBefore(minStart)) {
                        Log.warnv("Event {0} ({1}, {2}) did not have an email", uid, start, reason);
                    }
                    result.skipped++;
                    progress.value = ++i;
                    continue;
                }
                boolean oneFound = false;
                EmployeeSelector employeeSelector = new EmployeeSelector(em);
                for (String email : emails) {
                    List<EmployeeEntity> employees = employeeLookup.get(email);
                    if (employees == null) {
                        employees = employeeSelector.withEmail(email).findAll();
                        employeeLookup.put(email, employees);
                    }
                    if (employees.isEmpty()) {
                        Log.warnv("Event {0} ({1}, {2}): Employee with e-mail {3} not found, skipping", uid, start, reason, email);
                        continue;
                    }
                    oneFound = true;
                    progress.max++;
                    for (EmployeeEntity employee : employees) {
                        AbsenceEntity absence = null;
                        for (AbsenceEntity existingAbsence : employee.absences) {
                            if (uid.equals(existingAbsence.externalId)
                                    && existingAbsence.icalImport != null
                                    && existingAbsence.icalImport.id.equals(icalImport.id)) {
                                absence = existingAbsence;
                                break;
                            }
                        }
                        if (absence == null) {
                            absence = new AbsenceEntity();
                            employee.absences.add(absence);
                            absence.employee = employee;
                            absence.externalId = uid;
                            absence.icalImport = icalImport;
                        } else {
                            if (start.isBefore(minStart)) {
                                employee.absences.remove(absence);
                                absence.delete();
                                result.deleted++;
                                progress.value = ++i;
                                continue;
                            }
                        }

                        absence.reason = reason;
                        absence.start = start;
                        absence.end = end;
                        if (absence.id != null) {
                            result.updated++;
                        } else {
                            result.created++;
                        }
                        absence.persist();
                        absenceIds.add(absence.id);
                    }
                    progress.value = ++i;
                }
                if (!oneFound) {
                    result.skipped++;
                }
                progress.value = ++i;
            }

            if (!absenceIds.isEmpty()) {
                result.deleted = AbsenceEntity.deleteFromIcalImportExcept(icalImport, absenceIds);
            }

            progress.value = progress.max;

            progress.success = true;
            progress.message = "Import completed";

        } catch (BadStateException e) {
            progress.success = false;
            Log.warn(e.getMessage(), e);
            throw e;
        } catch (RuntimeException e) {
            progress.success = false;
            progress.message = e.getMessage();
            Log.warn(e.getMessage(), e);
            throw new BadStateException(e.getMessage());
        } finally {
            progress.result = result;
            progress.completed = true;
        }
    }

    @Transactional
    public void importHolidaysFromIcal(@Nonnull IcalImportEntity icalImport, @Nonnull ICalendar calendar,
                                       @Nonnull Progress<ImportResult> progress) throws HeuristicRollbackException, SystemException, HeuristicMixedException, RollbackException, NotSupportedException {
        ImportResult result = new ImportResult();
        try {

            progress.message = "Importing events";
            List<VEvent> components = calendar.getEvents();
            progress.max = components.size();
            int i = 0;

            Set<Long> holidayIds = new HashSet<>();
            for (VEvent component : components) {
                if (i % 100 == 0) {
                    transaction.commit();
                    transaction.begin();
                }
                Uid uidProperty = component.getUid();
                String uid = uidProperty != null ? uidProperty.getValue() : null;
                if (uid == null) {
                    Log.warn("Event did not have an UID, skipping");
                    result.skipped++;
                    progress.value = ++i;
                    continue;
                }
                Summary summaryProperty = component.getSummary();
                String reason = summaryProperty != null ? summaryProperty.getValue() : "Unnamed";
                LocalDate start = component.getDateStart().getValue().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                LocalDate end = component.getDateEnd().getValue().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().minusDays(1);

                LocalDate current = start;
                do {
                    HolidayEntity holiday = new HolidaySelector(em)
                            .withIcalImport(icalImport)
                            .withName(reason)
                            .withDate(current)
                            .find();

                    if (holiday == null) {
                        holiday = new HolidayEntity();
                        holiday.externalId = uid;
                        holiday.icalImport = icalImport;
                        holiday.date = current;
                    }

                    holiday.name = reason;
                    if (holiday.id != null) {
                        result.updated++;
                    } else {
                        result.created++;
                    }
                    holiday.persist();
                    holidayIds.add(holiday.id);
                    current = current.plusDays(1);
                } while (!current.isAfter(end));

                progress.value = ++i;
            }

            if (!holidayIds.isEmpty()) {
                result.deleted = AbsenceEntity.deleteFromIcalImportExcept(icalImport, holidayIds);
            }

            progress.value = progress.max;

            progress.success = true;
            progress.message = "Import completed";

        } catch (BadStateException e) {
            progress.success = false;
            Log.warn(e.getMessage(), e);
            throw e;
        } catch (RuntimeException e) {
            progress.success = false;
            progress.message = e.getMessage();
            Log.warn(e.getMessage(), e);
            throw new BadStateException(e.getMessage());
        } finally {
            progress.result = result;
            progress.completed = true;
        }
    }

    public ICalendar loadCalendar(Progress<ImportResult> progress, IcalImportEntity icalImport,
                                  @Nullable Credentials credentials) throws IOException, URISyntaxException {
        if (StringUtils.isEmpty(icalImport.url)) {
            throw new BadArgumentException("Import has no URL configured");
        }
        progress.message = "Downloading calendar";
        URL validUrl = new URI(icalImport.url).toURL();
        URLConnection connection = validUrl.openConnection();

        if (icalImport.auth == IcalImport.Auth.BASIC) {
            String encoded = createBasicAuth(icalImport, credentials);
            connection.setRequestProperty("Authorization", "Basic " + encoded);
        }

        if (connection instanceof HttpURLConnection httpURLConnection) {
            int responseCode = httpURLConnection.getResponseCode();
            if (responseCode != 200) {
                progress.message = "Could not load data from URL, got HTTP code " + responseCode;
                throw new BadStateException("Got HTTP status " + responseCode + ", expected 200");
            }
        }
        progress.message = "Parsing calendar";
        try (InputStream inputStream = connection.getInputStream()) {
            return readCalendar(inputStream);
        }
    }

    protected static String createBasicAuth(@Nonnull IcalImportEntity icalImport, @Nullable Credentials credentials) {
        String username = icalImport.username;
        String password = icalImport.password;
        if (credentials != null) {
            if (StringUtils.isNotEmpty(credentials.username)) {
                username = credentials.username;
            }
            if (StringUtils.isNotEmpty(credentials.password)) {
                password = credentials.password;
            }
        }
        return Base64.getEncoder().encodeToString((username + ":" + password).getBytes(StandardCharsets.UTF_8));
    }

    public ICalendar readCalendar(InputStream inputStream) throws IOException {
        return Biweekly.parse(inputStream).first();
    }

}
