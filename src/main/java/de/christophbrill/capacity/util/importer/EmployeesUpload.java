package de.christophbrill.capacity.util.importer;

import de.christophbrill.appframework.ui.dto.ImportResult;
import de.christophbrill.appframework.ui.dto.Progress;
import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.appframework.ui.exceptions.BadStateException;
import de.christophbrill.capacity.util.EmployeeImportXlsx;
import io.quarkus.logging.Log;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import org.jboss.resteasy.reactive.multipart.FileUpload;
import java.io.IOException;
import java.nio.file.Files;

@ApplicationScoped
public class EmployeesUpload {

    public static class Columns {
        public int[] name;
        public int[] abilities;
        public int[] location;
        public int[] fte;
    }

    @Inject
    EmployeeImportXlsx employeeImportXlsx;

    @Transactional
    public void runImport(Progress<ImportResult> progress, FileUpload file, EmployeesUpload.Columns columns, int identifierColumn, int sheet) {
        byte[] datax;
        try {
            datax = Files.readAllBytes(file.uploadedFile());
        } catch (IOException e) {
            throw new BadArgumentException(e.getMessage());
        }
        try {
            employeeImportXlsx.upload(progress, datax, identifierColumn,
                    columns, sheet);
        } catch (RuntimeException e) {
            Log.warn(e.getMessage(), e);
            progress.success = false;
            progress.message = e.getMessage();
            throw new BadStateException(e.getMessage());
        }
    }

}
