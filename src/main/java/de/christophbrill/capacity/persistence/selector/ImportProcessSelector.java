package de.christophbrill.capacity.persistence.selector;

import java.util.List;

import jakarta.annotation.Nonnull;
import jakarta.persistence.EntityManager;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;

import de.christophbrill.appframework.persistence.selector.AbstractResourceSelector;
import de.christophbrill.capacity.persistence.model.ImportProcessEntity;
import de.christophbrill.capacity.persistence.model.ImportProcessEntity_;

public class ImportProcessSelector extends AbstractResourceSelector<ImportProcessEntity> {

    private String search;
    private String filename;
    
    public ImportProcessSelector(EntityManager em) {
        super(em);
    }

    @Override
    @Nonnull
    protected List<Predicate> generatePredicateList(@Nonnull CriteriaBuilder builder,
                                                    @Nonnull Root<ImportProcessEntity> from,
                                                    @Nonnull CriteriaQuery<?> criteriaQuery) {
        List<Predicate> predicates = super.generatePredicateList(builder, from, criteriaQuery);

        if (StringUtils.isNotEmpty(search)) {
            predicates.add(builder.like(from.get(ImportProcessEntity_.filename), '%' + search + '%'));
        }

        if (StringUtils.isNotEmpty(filename)) {
            predicates.add(builder.equal(from.get(ImportProcessEntity_.filename), filename));
        }

        return predicates;
    }

    @Override
    public ImportProcessSelector withSearch(String search) {
        this.search = search;
        return this;
    }

    @Override
    @Nonnull
    protected Class<ImportProcessEntity> getEntityClass() {
        return ImportProcessEntity.class;
    }

    public ImportProcessSelector withFilename(String filename) {
        this.filename = filename;
        return this;
    }

}
