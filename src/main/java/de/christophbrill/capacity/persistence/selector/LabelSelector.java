package de.christophbrill.capacity.persistence.selector;

import de.christophbrill.appframework.persistence.selector.AbstractResourceSelector;
import de.christophbrill.capacity.persistence.model.LabelEntity;
import de.christophbrill.capacity.persistence.model.LabelEntity_;
import jakarta.annotation.Nonnull;
import jakarta.persistence.EntityManager;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import java.util.Arrays;
import java.util.List;

public class LabelSelector extends AbstractResourceSelector<LabelEntity> {

    private String search;
    private List<String> names;

    public LabelSelector(EntityManager em) {
        super(em);
    }

    @Nonnull
    @Override
    protected Class<LabelEntity> getEntityClass() {
        return LabelEntity.class;
    }

    @Nonnull
    @Override
    protected List<Predicate> generatePredicateList(@Nonnull CriteriaBuilder builder,
                                                    @Nonnull Root<LabelEntity> from,
                                                    @Nonnull CriteriaQuery<?> criteriaQuery) {
        List<Predicate> predicates = super.generatePredicateList(builder, from, criteriaQuery);

        if (StringUtils.isNotEmpty(search)) {
            String term = search.toLowerCase();
            predicates.add(builder.like(builder.lower(from.get(LabelEntity_.name)), '%' + term + '%'));
        }

        if (CollectionUtils.isNotEmpty(names)) {
            predicates.add(from.get(LabelEntity_.name).in(names));
        }

        return predicates;
    }

    @Override
    public LabelSelector withSearch(String search) {
        this.search = search;
        return this;
    }

    public LabelSelector withNames(String... names) {
        if (names == null || names.length == 0) {
            this.names = null;
        } else {
            this.names = Arrays.asList(names);
        }
        return this;
    }

}
