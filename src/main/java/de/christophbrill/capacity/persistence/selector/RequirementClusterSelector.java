package de.christophbrill.capacity.persistence.selector;

import java.util.List;

import de.christophbrill.capacity.persistence.model.LabelEntity_;
import jakarta.annotation.Nonnull;
import jakarta.persistence.EntityManager;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;

import de.christophbrill.appframework.persistence.selector.AbstractResourceSelector;
import de.christophbrill.capacity.persistence.model.RequirementClusterEntity;
import de.christophbrill.capacity.persistence.model.RequirementClusterEntity_;

public class RequirementClusterSelector extends AbstractResourceSelector<RequirementClusterEntity> {

    private String search;

    public RequirementClusterSelector(EntityManager em) {
        super(em);
    }

    @Override
    @Nonnull
    protected List<Predicate> generatePredicateList(@Nonnull CriteriaBuilder builder,
                                                    @Nonnull Root<RequirementClusterEntity> from,
                                                    @Nonnull CriteriaQuery<?> criteriaQuery) {
        List<Predicate> predicates = super.generatePredicateList(builder, from, criteriaQuery);

        if (StringUtils.isNotEmpty(search)) {
            String term = search.toLowerCase();
            predicates.add(builder.like(builder.lower(from.get(RequirementClusterEntity_.name)), '%' + term + '%'));
        }

        return predicates;
    }

    @Override
    public RequirementClusterSelector withSearch(String search) {
        this.search = search;
        return this;
    }

    @Override
    @Nonnull
    protected Class<RequirementClusterEntity> getEntityClass() {
        return RequirementClusterEntity.class;
    }

}
