package de.christophbrill.capacity.persistence.selector;

import java.time.LocalDate;
import java.util.List;

import jakarta.annotation.Nonnull;
import jakarta.persistence.EntityManager;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;

import de.christophbrill.appframework.persistence.model.DbObject_;
import de.christophbrill.appframework.persistence.selector.AbstractResourceSelector;
import de.christophbrill.capacity.persistence.model.AbsenceEntity;
import de.christophbrill.capacity.persistence.model.AbsenceEntity_;
import de.christophbrill.capacity.persistence.model.EmployeeEntity_;
import de.christophbrill.capacity.persistence.model.IcalImportEntity_;

public class AbsenceSelector extends AbstractResourceSelector<AbsenceEntity> {

	private Integer employeeId;
	private LocalDate startInclusive;
	private LocalDate endInclusive;
	private String search;

	public AbsenceSelector(EntityManager em) {
		super(em);
	}

	@Nonnull
	@Override
	protected Class<AbsenceEntity> getEntityClass() {
		return AbsenceEntity.class;
	}

	@Nonnull
	@Override
	protected List<Predicate> generatePredicateList(@Nonnull CriteriaBuilder builder, @Nonnull Root<AbsenceEntity> from, @Nonnull CriteriaQuery<?> query) {
		List<Predicate> predicates = super.generatePredicateList(builder, from, query);

		if (employeeId != null) {
			predicates.add(builder.equal(from.get(AbsenceEntity_.employee).get(DbObject_.id), employeeId));
		}

		if (startInclusive != null && endInclusive != null) {
			predicates.add(
				builder.or(
					// Absence starts before query range begin and ends after query range begin
					builder.and(
						builder.lessThanOrEqualTo(from.get(AbsenceEntity_.start), startInclusive),
						builder.greaterThanOrEqualTo(from.get(AbsenceEntity_.end), startInclusive)
					),
					// Absence starts before query range end and ends after query range end
					builder.and(
						builder.lessThanOrEqualTo(from.get(AbsenceEntity_.start), endInclusive),
						builder.greaterThanOrEqualTo(from.get(AbsenceEntity_.end), endInclusive)
					),
					// Absence starts in query range
					builder.and(
						builder.greaterThanOrEqualTo(from.get(AbsenceEntity_.start), startInclusive),
						builder.lessThanOrEqualTo(from.get(AbsenceEntity_.start), endInclusive)
					),
					// Absence ends in query range
					builder.and(
						builder.greaterThanOrEqualTo(from.get(AbsenceEntity_.end), startInclusive),
						builder.lessThanOrEqualTo(from.get(AbsenceEntity_.end), endInclusive)
					)
				)
			);
		}

		if (StringUtils.isNotEmpty(search)) {
			query.distinct(true);
			String likePattern = '%' + search + '%';
			predicates.add(builder.or(
					builder.like(from.get(AbsenceEntity_.employee).get(EmployeeEntity_.name), likePattern),
					builder.like(from.get(AbsenceEntity_.reason), likePattern),
					builder.like(from.join(AbsenceEntity_.icalImport).get(IcalImportEntity_.name), likePattern)
					));
		}

		return predicates;
	}

	public AbsenceSelector withEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
		return this;
	}

	public AbsenceSelector withStartInclusive(LocalDate startInclusive) {
		this.startInclusive = startInclusive;
		return this;
	}

	public AbsenceSelector withEndInclusive(LocalDate endInclusive) {
		this.endInclusive = endInclusive;
		return this;
	}

	@Override
	public AbsenceSelector withSearch(String search) {
		this.search = search;
		return this;
	}

}
