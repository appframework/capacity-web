package de.christophbrill.capacity.persistence.selector;

import java.time.LocalDate;
import java.util.List;

import de.christophbrill.capacity.persistence.model.IcalImportEntity;
import jakarta.annotation.Nonnull;
import jakarta.persistence.EntityManager;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.JoinType;
import jakarta.persistence.criteria.ListJoin;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;

import de.christophbrill.appframework.persistence.selector.AbstractResourceSelector;
import de.christophbrill.capacity.persistence.model.HolidayEntity;
import de.christophbrill.capacity.persistence.model.HolidayEntity_;
import de.christophbrill.capacity.persistence.model.LocationEntity;

public class HolidaySelector extends AbstractResourceSelector<HolidayEntity> {

	private IcalImportEntity icalImport;
	private String name;
	private LocalDate date;
	private LocalDate startInclusive;
	private LocalDate endInclusive;
	private LocationEntity onlyLocation;
	private LocationEntity includingLocation;
	private String search;

	public HolidaySelector(EntityManager em) {
		super(em);
	}

	@Nonnull
	@Override
	protected Class<HolidayEntity> getEntityClass() {
		return HolidayEntity.class;
	}

	@Nonnull
	@Override
	protected List<Predicate> generatePredicateList(@Nonnull CriteriaBuilder builder, @Nonnull Root<HolidayEntity> from, @Nonnull CriteriaQuery<?> query) {
		List<Predicate> predicates = super.generatePredicateList(builder, from, query);

		if (icalImport != null) {
			predicates.add(builder.equal(from.get(HolidayEntity_.icalImport), icalImport));
		}

		if (name != null && !name.isEmpty()) {
			predicates.add(builder.equal(from.get(HolidayEntity_.name), name));
		}

		if (date != null) {
			predicates.add(builder.equal(from.get(HolidayEntity_.date), date));
		}

		if (startInclusive != null) {
			predicates.add(builder.greaterThanOrEqualTo(from.get(HolidayEntity_.date), startInclusive));
		}

		if (endInclusive != null) {
			predicates.add(builder.lessThanOrEqualTo(from.get(HolidayEntity_.date), endInclusive));
		}

		if (onlyLocation != null) {
			ListJoin<HolidayEntity, LocationEntity> fromHoliday = from.join(HolidayEntity_.locations);
			predicates.add(builder.equal(fromHoliday, onlyLocation));
		}

		if (includingLocation != null) {
			ListJoin<HolidayEntity, LocationEntity> fromHoliday = from.join(HolidayEntity_.locations, JoinType.LEFT);
			predicates.add(
					builder.or(
							builder.equal(fromHoliday, includingLocation),
							builder.isEmpty(from.get(HolidayEntity_.locations))
					)
			);
		}

		if (StringUtils.isNotEmpty(search)) {
			String likePattern = '%' + search + '%';
			predicates.add(builder.or(
					builder.like(from.get(HolidayEntity_.name), likePattern)
					));
		}

		return predicates;
	}

	public HolidaySelector withIcalImport(IcalImportEntity icalImport) {
		this.icalImport = icalImport;
		return this;
	}

	public HolidaySelector withName(String name) {
		this.name = name;
		return this;
	}

	public HolidaySelector withDate(LocalDate date) {
		this.date = date;
		return this;
	}

	public HolidaySelector withStartInclusive(LocalDate startInclusive) {
		this.startInclusive = startInclusive;
		return this;
	}

	public HolidaySelector withEndInclusive(LocalDate endInclusive) {
		this.endInclusive = endInclusive;
		return this;
	}

	public HolidaySelector withOnlyLocation(LocationEntity onlyLocation) {
		this.onlyLocation = onlyLocation;
		return this;
	}

	public HolidaySelector withIncludingLocation(LocationEntity includingLocation) {
		this.includingLocation = includingLocation;
		return this;
	}

	@Override
	public HolidaySelector withSearch(String search) {
		this.search = search;
		return this;
	}

}
