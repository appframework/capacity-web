package de.christophbrill.capacity.persistence.selector;

import java.util.List;

import jakarta.annotation.Nonnull;
import jakarta.persistence.EntityManager;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.JoinType;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;

import de.christophbrill.appframework.persistence.selector.AbstractResourceSelector;
import de.christophbrill.capacity.persistence.model.EmployeeEntity;
import de.christophbrill.capacity.persistence.model.EmployeeEntity_;
import de.christophbrill.capacity.persistence.model.EmployeeEpisodeEntity_;
import de.christophbrill.capacity.persistence.model.EpisodeEntity;
import de.christophbrill.capacity.persistence.model.EpisodeEntity_;

public class EpisodeSelector extends AbstractResourceSelector<EpisodeEntity> {

	private String nameLike;
	private String search;
    private EmployeeEntity employee;

	public EpisodeSelector(EntityManager em) {
		super(em);
	}

	@Override
	@Nonnull
	protected Class<EpisodeEntity> getEntityClass() {
		return EpisodeEntity.class;
	}

	@Nonnull
	@Override
	protected List<Predicate> generatePredicateList(@Nonnull CriteriaBuilder builder, @Nonnull Root<EpisodeEntity> from,
													@Nonnull CriteriaQuery<?> criteriaQuery) {
		List<Predicate> predicates = super.generatePredicateList(builder, from, criteriaQuery);

		if (StringUtils.isNotEmpty(nameLike)) {
			predicates.add(builder.like(from.get(EpisodeEntity_.name), '%' + nameLike + '%'));
		}
		
		if (StringUtils.isNotEmpty(search)) {
			criteriaQuery.distinct(true);
			String likePattern = '%' + search + '%';
			predicates.add(builder.or(
					builder.like(from.get(EpisodeEntity_.name), likePattern),
					builder.like(from.join(EpisodeEntity_.employeeEpisodes, JoinType.LEFT).get(EmployeeEpisodeEntity_.employee).get(EmployeeEntity_.name), likePattern)
					));
		}
		
		if (employee != null) {
		    predicates.add(builder.equal(from.join(EpisodeEntity_.employeeEpisodes, JoinType.LEFT).get(EmployeeEpisodeEntity_.employee), employee));
		}

		return predicates;
	}

	public EpisodeSelector withNameLike(String nameLike) {
		this.nameLike = nameLike;
		return this;
	}

	@Override
	public EpisodeSelector withSearch(String search) {
		this.search = search;
		return this;
	}

}
