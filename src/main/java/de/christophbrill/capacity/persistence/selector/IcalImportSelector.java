package de.christophbrill.capacity.persistence.selector;

import java.util.List;

import jakarta.annotation.Nonnull;
import jakarta.persistence.EntityManager;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;

import de.christophbrill.appframework.persistence.selector.AbstractResourceSelector;
import de.christophbrill.capacity.persistence.model.IcalImportEntity;
import de.christophbrill.capacity.persistence.model.IcalImportEntity_;

public class IcalImportSelector extends AbstractResourceSelector<IcalImportEntity> {

	private String search;

	public IcalImportSelector(EntityManager em) {
		super(em);
	}

	@Override
	@Nonnull
	protected Class<IcalImportEntity> getEntityClass() {
		return IcalImportEntity.class;
	}
	
	@Nonnull
	@Override
	protected List<Predicate> generatePredicateList(@Nonnull CriteriaBuilder builder, @Nonnull Root<IcalImportEntity> from,
													@Nonnull CriteriaQuery<?> criteriaQuery) {
		List<Predicate> predicates = super.generatePredicateList(builder, from, criteriaQuery);

		if (StringUtils.isNotEmpty(search)) {
			String likePattern = '%' + search + '%';
			predicates.add(builder.or(
					builder.like(from.get(IcalImportEntity_.name), likePattern),
					builder.like(from.get(IcalImportEntity_.url), likePattern)
					));
		}

		return predicates;
	}

	@Override
	public IcalImportSelector withSearch(String search) {
		this.search = search;
		return this;
	}

}
