package de.christophbrill.capacity.persistence.selector;

import de.christophbrill.appframework.persistence.selector.AbstractResourceSelector;
import de.christophbrill.capacity.persistence.model.HolidayEntity_;
import de.christophbrill.capacity.persistence.model.LocationEntity;
import de.christophbrill.capacity.persistence.model.LocationEntity_;
import org.apache.commons.lang3.StringUtils;

import jakarta.annotation.Nonnull;
import jakarta.persistence.EntityManager;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import java.util.List;

public class LocationSelector extends AbstractResourceSelector<LocationEntity> {

    private String search;

    public LocationSelector(EntityManager em) {
        super(em);
    }

    @Nonnull
    @Override
    protected Class<LocationEntity> getEntityClass() {
        return LocationEntity.class;
    }

    @Nonnull
    @Override
    protected List<Predicate> generatePredicateList(@Nonnull CriteriaBuilder builder,
                                                    @Nonnull Root<LocationEntity> from,
                                                    @Nonnull CriteriaQuery<?> criteriaQuery) {
        List<Predicate> predicates = super.generatePredicateList(builder, from, criteriaQuery);

        if (StringUtils.isNotEmpty(search)) {
            criteriaQuery.distinct(true);
            String likePattern = '%' + search + '%';
            predicates.add(builder.or(
                    builder.like(from.get(LocationEntity_.name), likePattern),
                    builder.like(from.join(LocationEntity_.holidays).get(HolidayEntity_.name), likePattern)
                    ));
        }

        return predicates;
    }

    @Override
    public LocationSelector withSearch(String search) {
        this.search = search;
        return this;
    }

}
