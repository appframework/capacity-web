package de.christophbrill.capacity.persistence.selector;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.christophbrill.capacity.persistence.model.LabelEntity;
import io.quarkus.logging.Log;
import jakarta.annotation.Nonnull;
import jakarta.persistence.EntityManager;
import jakarta.persistence.criteria.CommonAbstractCriteria;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Expression;
import jakarta.persistence.criteria.JoinType;
import jakarta.persistence.criteria.ListJoin;
import jakarta.persistence.criteria.Order;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import jakarta.persistence.criteria.SetJoin;
import jakarta.persistence.criteria.Subquery;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import de.christophbrill.appframework.persistence.model.DbObject_;
import de.christophbrill.appframework.persistence.selector.AbstractResourceSelector;
import de.christophbrill.capacity.persistence.model.AbsenceEntity;
import de.christophbrill.capacity.persistence.model.AbsenceEntity_;
import de.christophbrill.capacity.persistence.model.ContractEntity_;
import de.christophbrill.capacity.persistence.model.EmployeeEntity;
import de.christophbrill.capacity.persistence.model.EmployeeEntity_;
import de.christophbrill.capacity.persistence.model.LabelEntity_;
import de.christophbrill.capacity.persistence.model.LocationEntity_;
import de.christophbrill.capacity.persistence.model.WorkingHoursEntity_;

public class EmployeeSelector extends AbstractResourceSelector<EmployeeEntity> {
	
	private static final String TYPE_SKILL = "skill";
	private static final String TYPE_VELOCITY = "velocity";
	private static final String TYPE_COLOR = "color";
	private static final Pattern PATTERN_SEARCH = Pattern.compile('(' + TYPE_SKILL + '|' + TYPE_VELOCITY + '|' + TYPE_COLOR + "):\\s*(\"([^\"]+)\"|([^\\s]+))");

	private LocalDate contractRangeStartDate;
	private LocalDate contractRangeEndDate;
	private String email;
	private LocalDate availableAt;
	private LocalDate absentAt;
	private Long locationId;
	private Collection<String> abilities;
	private String search;

	public EmployeeSelector(EntityManager em) {
		super(em);
	}

	@Nonnull
	@Override
	protected Class<EmployeeEntity> getEntityClass() {
		return EmployeeEntity.class;
	}

	@Nonnull
	@Override
	protected List<Predicate> generatePredicateList(@Nonnull CriteriaBuilder builder, @Nonnull Root<EmployeeEntity> from, @Nonnull CriteriaQuery<?> query) {
		List<Predicate> predicates = super.generatePredicateList(builder, from, query);

		if (contractRangeStartDate != null && contractRangeEndDate != null) {
			predicates.add(
				builder.or(
					// Case 1: The query range starts before the contract starts and ends after the contract is started
					builder.and(
						builder.lessThanOrEqualTo(from.get(EmployeeEntity_.contract).get(ContractEntity_.start), contractRangeStartDate),
						builder.greaterThanOrEqualTo(from.get(EmployeeEntity_.contract).get(ContractEntity_.start), contractRangeEndDate)
					),
					// Case 2: The query range starts before the contract ends and ends after the contract is ended
					builder.and(
						builder.lessThanOrEqualTo(from.get(EmployeeEntity_.contract).get(ContractEntity_.end), contractRangeStartDate),
						builder.greaterThanOrEqualTo(from.get(EmployeeEntity_.contract).get(ContractEntity_.end), contractRangeEndDate)
					),
					// Case 3: The query range starts after the contract starts and ends before the contract is ended
					builder.and(
						builder.lessThanOrEqualTo(from.get(EmployeeEntity_.contract).get(ContractEntity_.start), contractRangeStartDate),
						builder.greaterThanOrEqualTo(from.get(EmployeeEntity_.contract).get(ContractEntity_.end), contractRangeEndDate)
					),
					// Case 4: The contract has neither a start nor an end date
					builder.and(
						from.get(EmployeeEntity_.contract).get(ContractEntity_.start).isNull(),
						from.get(EmployeeEntity_.contract).get(ContractEntity_.end).isNull()
					),
					// Case 5: The contract starts before our query range and the contract does not have an end date
					builder.and(
						builder.lessThanOrEqualTo(from.get(EmployeeEntity_.contract).get(ContractEntity_.start), contractRangeStartDate),
						from.get(EmployeeEntity_.contract).get(ContractEntity_.end).isNull()
					),
					// Case 6: The contract ends after our query range and the contract does not have a start date
					builder.and(
						builder.greaterThanOrEqualTo(from.get(EmployeeEntity_.contract).get(ContractEntity_.end), contractRangeEndDate),
						from.get(EmployeeEntity_.contract).get(ContractEntity_.start).isNull()
					),
					// Case 7: The contract starts in our query range and the contract does not have an end date
					builder.and(
						builder.greaterThanOrEqualTo(from.get(EmployeeEntity_.contract).get(ContractEntity_.start), contractRangeStartDate),
						builder.lessThanOrEqualTo(from.get(EmployeeEntity_.contract).get(ContractEntity_.start), contractRangeEndDate),
						from.get(EmployeeEntity_.contract).get(ContractEntity_.end).isNull()
					),
					// Case 8: The contract ends in our query range and the contract does not have a start date
					builder.and(
						builder.greaterThanOrEqualTo(from.get(EmployeeEntity_.contract).get(ContractEntity_.end), contractRangeStartDate),
						builder.lessThanOrEqualTo(from.get(EmployeeEntity_.contract).get(ContractEntity_.end), contractRangeEndDate),
						from.get(EmployeeEntity_.contract).get(ContractEntity_.start).isNull()
					),
					// Case 9: The contract starts in our query range and the contract ends after our query range
					builder.and(
						builder.greaterThanOrEqualTo(from.get(EmployeeEntity_.contract).get(ContractEntity_.start), contractRangeStartDate),
						builder.lessThanOrEqualTo(from.get(EmployeeEntity_.contract).get(ContractEntity_.start), contractRangeEndDate),
						builder.greaterThanOrEqualTo(from.get(EmployeeEntity_.contract).get(ContractEntity_.end), contractRangeEndDate)
					),
					// Case 10: The contract ends in our query range and the contract starts before our query range
					builder.and(
						builder.greaterThanOrEqualTo(from.get(EmployeeEntity_.contract).get(ContractEntity_.end), contractRangeStartDate),
						builder.lessThanOrEqualTo(from.get(EmployeeEntity_.contract).get(ContractEntity_.end), contractRangeEndDate),
						builder.lessThanOrEqualTo(from.get(EmployeeEntity_.contract).get(ContractEntity_.start), contractRangeStartDate)
					),
					// Case 10: The contract starts and ands in our query range
					builder.and(
						builder.greaterThanOrEqualTo(from.get(EmployeeEntity_.contract).get(ContractEntity_.start), contractRangeStartDate),
						builder.lessThanOrEqualTo(from.get(EmployeeEntity_.contract).get(ContractEntity_.end), contractRangeEndDate)
					)
				)
			);
		}

		if (StringUtils.isNotEmpty(email)) {
			predicates.add(builder.equal(from.get(EmployeeEntity_.email), email));
		}

		if (availableAt != null) {
			// Find all employees that have a working day at the requested date
			Subquery<Long> subqueryWorkingDay = subqueryWorkingDay(builder, query, availableAt);

			// Find all employees that have an absence at the requested date
			Subquery<Long> subqueryAbsence = subqueryAbsence(builder, query, availableAt);

			predicates.add(builder.and(
					from.get(DbObject_.id).in(subqueryWorkingDay),
					builder.not(from.get(DbObject_.id).in(subqueryAbsence))
				));
		}

		if (absentAt != null) {

			// Find all employees that have a working day at the requested date
			Subquery<Long> subqueryWorkingDay = subqueryWorkingDay(builder, query, absentAt);

			// Find all employees that have an absence at the requested date
			Subquery<Long> subqueryAbsence = subqueryAbsence(builder, query, absentAt);

			predicates.add(builder.or(
				builder.not(from.get(DbObject_.id).in(subqueryWorkingDay)),
				from.get(DbObject_.id).in(subqueryAbsence)
			));
		}

		if (locationId != null) {
			predicates.add(builder.equal(from.get(EmployeeEntity_.location).get(LocationEntity_.id), locationId));
		}

		if (CollectionUtils.isNotEmpty(abilities)) {
			List<Predicate> and = new ArrayList<>(abilities.size());
			for (String ability : abilities) {
				and.add(builder.equal(from.join(EmployeeEntity_.abilities).get(LabelEntity_.name), ability));
			}
			predicates.add(builder.and(and.toArray(new Predicate[0])));
		}
		
		if (StringUtils.isNotEmpty(search)) {

			if (search.contains(":")) {
				evaluateSearch(search, predicates, builder, from, query);
			} else {
				applySearch(search, predicates, builder, from, query);
			}
		}

		return predicates;
	}
	
	private static void applySearch(String search, List<Predicate> predicates, @Nonnull CriteriaBuilder builder,
			@Nonnull Root<EmployeeEntity> from, @Nonnull CriteriaQuery<?> criteriaQuery) {
		criteriaQuery.distinct(true);
		String likePattern = '%' + search + '%';
		predicates.add(builder.or(
				builder.like(from.get(EmployeeEntity_.email), likePattern),
				builder.like(from.get(EmployeeEntity_.name), likePattern),
				builder.like(from.join(EmployeeEntity_.abilities, JoinType.LEFT).get(LabelEntity_.name), likePattern)
				));
	}
	
	static void evaluateSearch(String search, List<Predicate> predicates, @Nonnull CriteriaBuilder builder,
							   @Nonnull Root<EmployeeEntity> from, @Nonnull CriteriaQuery<?> query) {
		Matcher matcher = PATTERN_SEARCH.matcher(search);
		boolean found = false;
		int lastIndex = -1;
		while (matcher.find()) {
			found = true;
			String type = matcher.group(1);
			String term = matcher.group(3);
			if (term == null) {
				term = matcher.group(4);
			}
			term = term.toLowerCase();
			if (lastIndex + 1 < matcher.start()) {
				applySearch(search.substring(lastIndex + 1, matcher.start()).trim(), predicates, builder, from, query);
			}
			switch (type) {
				case TYPE_SKILL:
					if (term.startsWith("!")) {
						Subquery<Long> subqueryAbsence = subqueryLabel(builder, query, '%' + term.substring(1) + '%');
						predicates.add(builder.not(from.get(DbObject_.id).in(subqueryAbsence)));
					} else {
						Subquery<Long> subqueryAbsence = subqueryLabel(builder, query, '%' + term + '%');
						predicates.add(from.get(DbObject_.id).in(subqueryAbsence));
					}
					break;
				case TYPE_VELOCITY:
					predicates.add(termToPredicateInteger(builder, term, from.join(EmployeeEntity_.velocity)));
					break;
				case TYPE_COLOR:
					predicates.add(termToPredicate(builder, term, from.get(EmployeeEntity_.color)));
					break;
				default:
					Log.errorv("Matcher found illegal group {0}, skipping", type);
			}
			lastIndex = matcher.end();
		}
		if (lastIndex + 1 < search.length()) {
			applySearch(search.substring(lastIndex + 1), predicates, builder, from, query);
			found = true;
		}
		if (!found) {
			applySearch(search, predicates, builder, from, query);
		}
	}
	
	private static Predicate termToPredicate(CriteriaBuilder builder, String term,
			Expression<String> expression) {
		if (term.startsWith("!")) {
			return builder.notLike(builder.lower(expression), '%' + term.substring(1) + '%');
		} else {
			return builder.like(builder.lower(expression), '%' + term + '%');
		}
	}
	
	private static Predicate termToPredicateInteger(CriteriaBuilder builder, String term,
			Expression<Integer> expression) {
		if (term.startsWith("!")) {
			return builder.notEqual(expression, Integer.valueOf(term.substring(1)));
		} else {
			return builder.equal(expression, Integer.valueOf(term));
		}
	}

	private static Subquery<Long> subqueryWorkingDay(CriteriaBuilder builder, CommonAbstractCriteria query,
														LocalDate at) {
		Subquery<Long> subqueryWorkingDay = query.subquery(Long.class);
		Root<EmployeeEntity> subfrom = subqueryWorkingDay.from(EmployeeEntity.class);
		subqueryWorkingDay.select(subfrom.get(DbObject_.id));
		subqueryWorkingDay.where(
			builder.equal(subfrom.join(EmployeeEntity_.contract).join(ContractEntity_.workingHours).get(WorkingHoursEntity_.dayOfWeek), at.getDayOfWeek().getValue())
		);
		return subqueryWorkingDay;
	}

	private static Subquery<Long> subqueryAbsence(CriteriaBuilder builder, CommonAbstractCriteria query,
													 LocalDate at) {
		Subquery<Long> subqueryAbsence = query.subquery(Long.class);
		Root<EmployeeEntity> subfrom = subqueryAbsence.from(EmployeeEntity.class);
		ListJoin<EmployeeEntity,AbsenceEntity> fromAbsences = subfrom.join(EmployeeEntity_.absences, JoinType.LEFT);
		subqueryAbsence.select(subfrom.get(DbObject_.id));
		subqueryAbsence.where(
				builder.and(
						builder.lessThanOrEqualTo(fromAbsences.get(AbsenceEntity_.start), at),
						builder.greaterThanOrEqualTo(fromAbsences.get(AbsenceEntity_.end), at)
					)
		);
		return subqueryAbsence;
	}

	private static Subquery<Long> subqueryLabel(CriteriaBuilder builder, CommonAbstractCriteria query,
												  String labelLike) {
		Subquery<Long> subqueryLabel = query.subquery(Long.class);
		Root<EmployeeEntity> subfrom = subqueryLabel.from(EmployeeEntity.class);
		SetJoin<EmployeeEntity, LabelEntity> fromAbsences = subfrom.join(EmployeeEntity_.abilities, JoinType.LEFT);
		subqueryLabel.select(subfrom.get(DbObject_.id));
		subqueryLabel.where(
				builder.like(builder.lower(fromAbsences.get(LabelEntity_.name)), labelLike)
		);
		return subqueryLabel;
	}

	public EmployeeSelector withActiveContract(LocalDate contractRangeStartDate, LocalDate contractRangeEndDate) {
		this.contractRangeStartDate = contractRangeStartDate;
		this.contractRangeEndDate = contractRangeEndDate;
		return this;
	}

	public EmployeeSelector withEmail(String email) {
		this.email = email;
		return this;
	}

	public EmployeeSelector withAvailability(LocalDate availableAt) {
		this.availableAt = availableAt;
		return this;
	}

	public EmployeeSelector withAbsence(LocalDate absentAt) {
		this.absentAt = absentAt;
		return this;
	}

	public EmployeeSelector withLocationId(Long locationId) {
		this.locationId = locationId;
		return this;
	}

	public EmployeeSelector withAbilities(Collection<String> abilities) {
		this.abilities = abilities;
		return this;
	}

	@Override
	public EmployeeSelector withSearch(String search) {
		this.search = search;
		return this;
	}

	@Override
	protected List<Order> getDefaultOrderList(CriteriaBuilder builder, Root<EmployeeEntity> from) {
		return Collections.singletonList(builder.asc(from.get(EmployeeEntity_.name)));
	}

}
