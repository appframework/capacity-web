package de.christophbrill.capacity.persistence.selector;

import java.util.List;

import jakarta.annotation.Nonnull;
import jakarta.persistence.EntityManager;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;

import de.christophbrill.appframework.persistence.selector.AbstractResourceSelector;
import de.christophbrill.capacity.persistence.model.WidgetEntity;
import de.christophbrill.capacity.persistence.model.WidgetEntity_;

public class WidgetSelector extends AbstractResourceSelector<WidgetEntity> {

	private String search;

	public WidgetSelector(EntityManager em) {
		super(em);
	}

	@Nonnull
	@Override
	protected Class<WidgetEntity> getEntityClass() {
		return WidgetEntity.class;
	}

	@Nonnull
	@Override
	protected List<Predicate> generatePredicateList(@Nonnull CriteriaBuilder builder, @Nonnull Root<WidgetEntity> from, @Nonnull CriteriaQuery<?> query) {
		List<Predicate> predicates = super.generatePredicateList(builder, from, query);

		if (StringUtils.isNotEmpty(search)) {
			String likePattern = '%' + search + '%';
			predicates.add(builder.or(
					builder.like(from.get(WidgetEntity_.name), likePattern),
					builder.like(from.get(WidgetEntity_.configuration), likePattern)
					));
		}

		return predicates;
	}

	@Override
	public WidgetSelector withSearch(String search) {
		this.search = search;
		return this;
	}
}
