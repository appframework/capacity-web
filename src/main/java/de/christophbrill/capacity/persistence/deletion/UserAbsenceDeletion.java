package de.christophbrill.capacity.persistence.deletion;

import de.christophbrill.appframework.persistence.deletion.AbstractUserCreatedModifiedDeletion;

public class UserAbsenceDeletion extends AbstractUserCreatedModifiedDeletion {

	@Override
	protected String getEntityName() {
		return "Absence";
	}

}
