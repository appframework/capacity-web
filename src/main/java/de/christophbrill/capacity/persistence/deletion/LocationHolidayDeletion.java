package de.christophbrill.capacity.persistence.deletion;

import de.christophbrill.appframework.persistence.deletion.CascadeDeletion;
import de.christophbrill.appframework.ui.dto.DeletionAction;
import de.christophbrill.capacity.persistence.model.HolidayEntity;
import de.christophbrill.capacity.persistence.model.LocationEntity;
import de.christophbrill.capacity.persistence.selector.HolidaySelector;
import jakarta.persistence.EntityManager;
import java.util.List;

public class LocationHolidayDeletion implements CascadeDeletion<LocationEntity> {

	@Override
	public int affectedByDeletion(EntityManager em, LocationEntity location) {
		return em.createQuery("select count(h) from Holiday h where :location in elements(h.locations)", Number.class)
                .setParameter("location", location)
                .getSingleResult()
                .intValue();
	}

	@Override
	public int delete(EntityManager em, LocationEntity location) {
		List<HolidayEntity> holidays = new HolidaySelector(em)
				.withIncludingLocation(location)
				.findAll();
		int count = 0;
		for (HolidayEntity holiday : holidays) {
			holiday.locations.remove(location);
			count++;
		}
		return count;
	}

	@Override
	public DeletionAction getAction() {
        return new DeletionAction("Holiday configured for location", "will remove the location from the holiday");
	}

}
