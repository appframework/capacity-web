package de.christophbrill.capacity.persistence.deletion;

import de.christophbrill.appframework.persistence.deletion.CascadeDeletion;
import de.christophbrill.appframework.ui.dto.DeletionAction;
import de.christophbrill.capacity.persistence.model.EmployeeEntity;
import de.christophbrill.capacity.persistence.model.LabelEntity;
import de.christophbrill.capacity.persistence.selector.EmployeeSelector;
import jakarta.persistence.EntityManager;
import java.util.List;
import java.util.Set;

public class LabelEmployeeDeletion implements CascadeDeletion<LabelEntity> {

	@Override
	public int affectedByDeletion(EntityManager em, LabelEntity label) {
		return em.createQuery("select count(e) from Employee e where :label in elements(e.abilities)", Number.class)
                .setParameter("label", label)
                .getSingleResult()
                .intValue();
	}

	@Override
	public int delete(EntityManager em, LabelEntity label) {
		List<EmployeeEntity> employees = new EmployeeSelector(em)
				.withAbilities(Set.of(label.name))
				.findAll();
		int count = 0;
		for (EmployeeEntity employee : employees) {
			employee.abilities.remove(label);
			count++;
		}
		return count;
	}

	@Override
	public DeletionAction getAction() {
        return new DeletionAction("Employee with this ability", "will remove the ability from the employee");
	}

}
