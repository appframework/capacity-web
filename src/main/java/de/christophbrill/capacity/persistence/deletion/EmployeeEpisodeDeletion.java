package de.christophbrill.capacity.persistence.deletion;

import jakarta.persistence.EntityManager;

import de.christophbrill.appframework.persistence.deletion.CascadeDeletion;
import de.christophbrill.appframework.ui.dto.DeletionAction;
import de.christophbrill.capacity.persistence.model.EmployeeEntity;

public class EmployeeEpisodeDeletion implements CascadeDeletion<EmployeeEntity> {

	@Override
	public int affectedByDeletion(EntityManager em, EmployeeEntity employee) {
		return em.createQuery("select count(ee) from EmployeeEpisode ee where ee.employee = :employee", Number.class)
                .setParameter("employee", employee)
                .getSingleResult()
                .intValue();
	}

	@Override
	public int delete(EntityManager em, EmployeeEntity employee) {
		return em.createQuery("delete from EmployeeEpisode where employee = :employee")
                .setParameter("employee", employee)
                .executeUpdate();
	}

	@Override
	public DeletionAction getAction() {
        return new DeletionAction("Episoded referencing the employee", "will remove the employee from the episodes");
	}

}
