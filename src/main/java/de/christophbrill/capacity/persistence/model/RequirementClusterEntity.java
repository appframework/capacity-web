package de.christophbrill.capacity.persistence.model;

import de.christophbrill.appframework.persistence.model.DbObject;
import org.hibernate.annotations.BatchSize;

import jakarta.annotation.Nullable;
import jakarta.persistence.CollectionTable;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.NoResultException;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import java.io.Serial;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity(name = "RequirementCluster")
@Table(name = "requirement_cluster")
public class RequirementClusterEntity extends DbObject {

	@Serial
	private static final long serialVersionUID = 3663253127148940377L;

	public String name;
	@BatchSize(size = 50)
	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "reference", joinColumns = @JoinColumn(name = "cluster_id"))
	public Set<ReferenceEntity> references = new HashSet<>(0);
	@OneToMany(mappedBy = "cluster")
	public List<RequirementEntity> requirements = new ArrayList<>(0);

	@Nullable
	public static RequirementClusterEntity findByReference(ReferencePoolEntity referencePool, String value) {
		try {
			return getEntityManager().createQuery("select r from RequirementCluster r " +
							"inner join r.references rr " +
							"where rr.pool = :type " +
							"and rr.referenceKey = :value", RequirementClusterEntity.class)
					.setParameter("type", referencePool)
					.setParameter("value", value)
					.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
}
