package de.christophbrill.capacity.persistence.model;

import de.christophbrill.appframework.persistence.model.DbObject;
import de.christophbrill.capacity.persistence.selector.LocationSelector;

import jakarta.persistence.Entity;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import java.io.Serial;
import java.util.List;
import java.util.Set;

@Entity(name = "Location")
@Table(name = "location")
public class LocationEntity extends DbObject {

	@Serial
	private static final long serialVersionUID = 101306763923465235L;

	public String name;
	@ManyToMany(mappedBy = "locations")
	public Set<HolidayEntity> holidays;
	@OneToMany(mappedBy = "location")
	public Set<EmployeeEntity> employees;

	public static List<LocationEntity> findByIds(List<Long> ids) {
		return new LocationSelector(getEntityManager()).withIds(ids).findAll();
	}

}
