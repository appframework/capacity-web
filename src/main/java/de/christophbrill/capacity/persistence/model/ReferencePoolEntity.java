package de.christophbrill.capacity.persistence.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;

import de.christophbrill.appframework.persistence.model.DbObject;
import java.io.Serial;

@Entity(name = "ReferencePool")
@Table(name = "reference_pool")
public class ReferencePoolEntity extends DbObject {

	@Serial
	private static final long serialVersionUID = 6737196979920326394L;

	public String name;
	@Column(name = "template_url", nullable = false, length = 1023)
	public String templateUrl;
	
}
