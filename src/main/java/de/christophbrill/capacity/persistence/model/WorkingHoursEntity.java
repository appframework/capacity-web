package de.christophbrill.capacity.persistence.model;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import java.io.Serial;
import java.io.Serializable;
import java.time.LocalTime;
import java.util.Objects;

@Embeddable
public class WorkingHoursEntity implements Serializable {

	@Serial
	private static final long serialVersionUID = 7957871736341424395L;

	public int dayOfWeek;
	public LocalTime start;
	@Column(name = "`end`")
	public LocalTime end;

	@Override
	public int hashCode() {
		return Objects.hash(dayOfWeek, start, end);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof WorkingHoursEntity that)) return false;
        return dayOfWeek == that.dayOfWeek && Objects.equals(start, that.start) && Objects.equals(end, that.end);
	}
}
