package de.christophbrill.capacity.persistence.model;

import java.io.Serial;
import java.util.Set;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OrderBy;
import jakarta.persistence.Table;

import org.hibernate.annotations.BatchSize;

import de.christophbrill.appframework.persistence.model.DbObject;

@Entity(name = "RequiredAbility")
@Table(name = "required_ability")
public class RequiredAbilityEntity extends DbObject {

	@Serial
	private static final long serialVersionUID = -977737083493540897L;

	@ManyToOne
	@JoinColumn(name = "requirement_id", nullable = false)
	public RequirementEntity requirement;
	public int percentage;
	@BatchSize(size = 50)
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "requiredability_label", joinColumns = @JoinColumn(name = "requiredability_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "label_id", referencedColumnName = "id"))
	@OrderBy("name")
	public Set<LabelEntity> skills;

}
