package de.christophbrill.capacity.persistence.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;

import de.christophbrill.appframework.persistence.model.DbObject;
import java.io.Serial;

@Entity(name = "ImportProcess")
@Table(name = "importprocess")
public class ImportProcessEntity extends DbObject {

    @Serial
    private static final long serialVersionUID = -7086346658736103764L;

    public String filename;
    public String columns;
    @Column(name = "identifier_column")
    public int identifierColumn;
    @Column(name = "identifier_type")
    public String identifierType;
    public Integer sheet;

}
