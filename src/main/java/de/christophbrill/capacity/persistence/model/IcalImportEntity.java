package de.christophbrill.capacity.persistence.model;

import java.io.Serial;
import java.time.LocalDateTime;

import de.christophbrill.capacity.ui.dto.IcalImport;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Table;

import de.christophbrill.appframework.persistence.model.DbObject;
import jakarta.validation.constraints.NotNull;

@Entity(name = "IcalImport")
@Table(name = "ical_import")
public class IcalImportEntity extends DbObject {

    @Serial
    private static final long serialVersionUID = 7125037687341625543L;

    @NotNull
    @Enumerated(EnumType.STRING)
    public IcalImport.Type type;
    @NotNull
    public String name;
    public String url;
    public LocalDateTime lastImported;
    @Enumerated(EnumType.STRING)
    public IcalImport.Auth auth = IcalImport.Auth.NONE;
    public String username;
    public String password;

}
