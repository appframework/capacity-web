package de.christophbrill.capacity.persistence.model;

import java.io.Serial;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OrderBy;
import jakarta.persistence.Table;

import org.hibernate.annotations.BatchSize;

import de.christophbrill.appframework.persistence.model.DbObject;

@Entity(name = "Employee")
@Table(name = "employee")
public class EmployeeEntity extends DbObject {

	@Serial
	private static final long serialVersionUID = 6250519976635138525L;

	public String name;
	public String email;
	@BatchSize(size = 50)
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "employee_label", joinColumns = @JoinColumn(name = "employee_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "label_id", referencedColumnName = "id"))
	@OrderBy("name")
	public Set<LabelEntity> abilities = HashSet.newHashSet(0);
	@Embedded
	public ContractEntity contract;
	@ManyToOne
	@JoinColumn(name = "location_id")
	public LocationEntity location;
	@OneToMany(mappedBy = "employee", cascade = CascadeType.REMOVE)
	public List<AbsenceEntity> absences = new ArrayList<>(0);
	@Column(nullable = false)
	public String color;
	public int velocity;

	public String getEmail() {
		return email;
	}

}
