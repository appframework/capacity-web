package de.christophbrill.capacity.persistence.model;

import org.hibernate.annotations.BatchSize;

import jakarta.persistence.CollectionTable;
import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Embeddable;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

@Embeddable
public class ContractEntity implements Serializable {

	@Serial
	private static final long serialVersionUID = 7165266523117189232L;

	public LocalDate start;
	@Column(name = "`end`")
	public LocalDate end;
	public int vacationDaysPerYear;
	@BatchSize(size = 50)
	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "working_hours", joinColumns = @JoinColumn(name = "employee_id") )
	public List<WorkingHoursEntity> workingHours;

	@Override
	public int hashCode() {
		return Objects.hash(start, end, vacationDaysPerYear, workingHours);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof ContractEntity that)) return false;
        return vacationDaysPerYear == that.vacationDaysPerYear && Objects.equals(start, that.start) && Objects.equals(end, that.end) && Objects.equals(workingHours, that.workingHours);
	}

}
