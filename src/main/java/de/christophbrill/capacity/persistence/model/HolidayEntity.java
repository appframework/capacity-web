package de.christophbrill.capacity.persistence.model;

import java.io.Serial;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;

import jakarta.annotation.Nonnull;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

import de.christophbrill.appframework.persistence.model.DbObject;

@Entity(name = "Holiday")
@Table(name = "holiday")
public class HolidayEntity extends DbObject {

	@Serial
	private static final long serialVersionUID = 3113650984470473152L;

	public LocalDate date;
	public String name;
	@Column(nullable = false)
	public int hoursReduction;
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "holiday_location", joinColumns = {
			@JoinColumn(name = "holiday_id", referencedColumnName = "id") }, inverseJoinColumns = {
			@JoinColumn(name = "location_id", referencedColumnName = "id") })
	public List<LocationEntity> locations;
	public String externalId;
	@ManyToOne
	@JoinColumn(name = "icalimport_id")
	public IcalImportEntity icalImport;

	public static int deleteFromIcalImportExcept(@Nonnull IcalImportEntity icalImport,
												 @Nonnull Set<Long> ids) {
		return getEntityManager()
				.createQuery("delete from Holiday where icalImport = :icalImport and not id in(:ids)")
				.setParameter("icalImport", icalImport)
				.setParameter("ids", ids)
				.executeUpdate();

	}
}
