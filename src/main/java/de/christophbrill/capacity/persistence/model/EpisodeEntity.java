package de.christophbrill.capacity.persistence.model;

import java.io.Serial;
import java.time.LocalDate;
import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

import de.christophbrill.appframework.persistence.model.DbObject;

@Entity(name = "Episode")
@Table(name = "episode")
public class EpisodeEntity extends DbObject {

	@Serial
	private static final long serialVersionUID = -8378906131677316919L;

	public LocalDate start;
	@Column(name = "`end`")
	public LocalDate end;
	public String name;
	@OneToMany(mappedBy = "episode", cascade = CascadeType.ALL, orphanRemoval = true)
	public List<EmployeeEpisodeEntity> employeeEpisodes;

}
