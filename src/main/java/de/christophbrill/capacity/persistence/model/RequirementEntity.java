package de.christophbrill.capacity.persistence.model;

import de.christophbrill.appframework.persistence.model.DbObject;
import de.christophbrill.capacity.ui.dto.Priority;
import jakarta.validation.constraints.NotNull;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import jakarta.annotation.Nullable;
import jakarta.persistence.CollectionTable;
import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NoResultException;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import java.io.Serial;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Entity(name = "Requirement")
@Table(name = "requirement")
public class RequirementEntity extends DbObject {

	@Serial
	private static final long serialVersionUID = -1985264258409919083L;

	public String name;
	@ManyToOne
	@JoinColumn(name = "cluster_id", nullable = false)
	public RequirementClusterEntity cluster;
	@Enumerated(EnumType.STRING)
	@NotNull
	public Priority priority;
	@BatchSize(size = 50)
	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "reference", joinColumns = @JoinColumn(name = "requirement_id"))
	public Set<ReferenceEntity> references = new HashSet<>(0);
	public int effort;
	public Integer remaining;
	@Cascade(CascadeType.ALL)
	@OneToMany(mappedBy = "requirement", orphanRemoval = true)
	public Set<RequiredAbilityEntity> requiredAbilities = new HashSet<>(0);
	@Column(name = "desired_start")
	public LocalDate desiredStart;
	public LocalDate due;


	@Nullable
	public static RequirementEntity findByReference(ReferencePoolEntity referencePool, String value) {
		try {
			return getEntityManager().createQuery("select r from Requirement r " +
							"inner join r.references rr " +
							"where rr.pool = :type " +
							"and rr.referenceKey = :value", RequirementEntity.class)
					.setParameter("type", referencePool)
					.setParameter("value", value)
					.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

}
