package de.christophbrill.capacity.persistence.model;

import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

import de.christophbrill.appframework.persistence.model.DbObject;
import java.io.Serial;

@Entity(name = "EmployeeEpisode")
@Table(name = "employee_episode")
public class EmployeeEpisodeEntity extends DbObject {

	@Serial
	private static final long serialVersionUID = 3638573182102449011L;

	@ManyToOne
	@JoinColumn(name = "employee_id", nullable = false)
	public EmployeeEntity employee;
	@ManyToOne
	@JoinColumn(name = "episode_id", nullable = false)
	public EpisodeEntity episode;
	public Integer velocity;

}
