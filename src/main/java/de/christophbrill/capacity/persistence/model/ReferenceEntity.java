package de.christophbrill.capacity.persistence.model;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import java.io.Serial;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class ReferenceEntity implements Serializable {

	@Serial
	private static final long serialVersionUID = 3717361209586481039L;

	@ManyToOne
	@JoinColumn(name = "pool_id", nullable = false)
	public ReferencePoolEntity pool;
	@Column(name = "reference_key", nullable = false)
	public String referenceKey;

	@Override
	public int hashCode() {
		return Objects.hash(pool, referenceKey);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof ReferenceEntity that)) return false;
        return Objects.equals(pool, that.pool) && Objects.equals(referenceKey, that.referenceKey);
	}
}
