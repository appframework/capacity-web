package de.christophbrill.capacity.persistence.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;

import de.christophbrill.appframework.persistence.model.DbObject;
import java.io.Serial;

@Entity(name = "Widget")
@Table(name = "widget")
public class WidgetEntity extends DbObject {

	@Serial
	private static final long serialVersionUID = 4594848392077369960L;

	public String name;
	@Column(length = 63)
	public String type;
	public String configuration;

}
