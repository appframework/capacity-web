package de.christophbrill.capacity.persistence.model;

import jakarta.persistence.CollectionTable;
import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.Table;

import de.christophbrill.appframework.persistence.model.DbObject;
import jakarta.validation.constraints.NotNull;

import java.io.Serial;
import java.util.List;

@Entity(name = "Label")
@Table(name = "label")
public class LabelEntity extends DbObject {

    @Serial
    private static final long serialVersionUID = 2551508057515756501L;

    public String name;
    @NotNull
    public String color;
    @ElementCollection
    @CollectionTable(name = "label_alias", joinColumns = @JoinColumn(name = "label_id"))
    @Column(name = "alias")
    public List<String> alias;
    public boolean precedence;

}
