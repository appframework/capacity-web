package de.christophbrill.capacity.persistence.model;

import de.christophbrill.appframework.persistence.model.DbObject;

import jakarta.annotation.Nonnull;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import java.io.Serial;
import java.time.LocalDate;
import java.util.Set;

@Entity(name = "Absence")
@Table(name = "absence")
public class AbsenceEntity extends DbObject {

    @Serial
    private static final long serialVersionUID = 1338275104365902115L;

    @ManyToOne
    @JoinColumn(name = "employee_id", nullable = false)
    public EmployeeEntity employee;
    @Column(nullable = false)
    public LocalDate start;
    @Column(nullable = false, name = "`end`")
    public LocalDate end;
    @Column(nullable = false)
    public String reason;
    public String externalId;
    @ManyToOne
    @JoinColumn(name = "icalimport_id")
    public IcalImportEntity icalImport;

    public static int deleteFromIcalImportExcept(@Nonnull IcalImportEntity icalImport,
                                                 @Nonnull Set<Long> absenceIds) {
        return getEntityManager()
                .createQuery("delete from Absence where icalImport = :icalImport and not id in(:absenceIds)")
                .setParameter("icalImport", icalImport)
                .setParameter("absenceIds", absenceIds)
                .executeUpdate();

    }
}
