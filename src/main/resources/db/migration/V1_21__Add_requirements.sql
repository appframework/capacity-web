CREATE TABLE requirement_cluster (
  id SERIAL,
  created timestamp DEFAULT NULL,
  modified timestamp DEFAULT NULL,
  creator_id int DEFAULT NULL,
  modificator_id int DEFAULT NULL,
  title varchar(255) NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT FK_cluster_creator FOREIGN KEY (creator_id) REFERENCES user_ (id),
  CONSTRAINT FK_cluster_modificator FOREIGN KEY (modificator_id) REFERENCES user_ (id)
);

CREATE TABLE requirement (
  id SERIAL,
  created timestamp DEFAULT NULL,
  modified timestamp DEFAULT NULL,
  creator_id int DEFAULT NULL,
  modificator_id int DEFAULT NULL,
  title varchar(255) NOT NULL,
  cluster_id int,
  priority varchar(20) NOT NULL,
  effort int NOT NULL,
  remaining int,
  desired_start date,
  due date,
  PRIMARY KEY (id),
  CONSTRAINT FK_requirement_creator FOREIGN KEY (creator_id) REFERENCES user_ (id),
  CONSTRAINT FK_requirement_modificator FOREIGN KEY (modificator_id) REFERENCES user_ (id),
  CONSTRAINT FK_requirement_cluster FOREIGN KEY (cluster_id) REFERENCES requirement_cluster(id)
);

CREATE TABLE reference_pool (
  id SERIAL,
  created timestamp DEFAULT NULL,
  modified timestamp DEFAULT NULL,
  creator_id int DEFAULT NULL,
  modificator_id int DEFAULT NULL,
  name varchar(255) NOT NULL,
  template_url varchar(1023) NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT FK_referencepool_creator FOREIGN KEY (creator_id) REFERENCES user_ (id),
  CONSTRAINT FK_referencepool_modificator FOREIGN KEY (modificator_id) REFERENCES user_ (id)
);

CREATE TABLE reference (
  requirement_id int,
  cluster_id int,
  pool_id int NOT NULL,
  reference_key varchar(255) NOT NULL,
  CONSTRAINT FK_reference_requirement FOREIGN KEY (requirement_id) REFERENCES requirement(id),
  CONSTRAINT FK_reference_cluster FOREIGN KEY (cluster_id) REFERENCES requirement_cluster(id),
  CONSTRAINT FK_reference_pool FOREIGN KEY (pool_id) REFERENCES reference_pool(id)
);

CREATE TABLE required_ability (
  id SERIAL,
  created timestamp DEFAULT NULL,
  modified timestamp DEFAULT NULL,
  creator_id int DEFAULT NULL,
  modificator_id int DEFAULT NULL,
  requirement_id int NOT NULL,
  percentage int NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT FK_requiredability_creator FOREIGN KEY (creator_id) REFERENCES user_ (id),
  CONSTRAINT FK_requiredability_modificator FOREIGN KEY (modificator_id) REFERENCES user_ (id),
  CONSTRAINT FK_requiredability_requirement FOREIGN KEY (requirement_id) REFERENCES requirement(id)
);

CREATE TABLE required_ability_skill (
  requiredability_id int NOT NULL,
  skill varchar(255) NOT NULL,
  CONSTRAINT FK_requiredabilityskill_requiredability FOREIGN KEY (requiredability_id) REFERENCES required_ability (id)
);
