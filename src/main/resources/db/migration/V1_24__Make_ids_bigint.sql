ALTER TABLE ability
    ALTER COLUMN employee_id TYPE bigint;
ALTER TABLE absence
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint,
    ALTER COLUMN employee_id TYPE bigint,
    ALTER COLUMN icalimport_id TYPE bigint;
ALTER TABLE binary_data
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint;
ALTER TABLE employee
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint,
    ALTER COLUMN location_id TYPE bigint;
ALTER TABLE employee_episode
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint,
    ALTER COLUMN employee_id TYPE bigint,
    ALTER COLUMN episode_id TYPE bigint;
ALTER TABLE employee_label
    ALTER COLUMN employee_id TYPE bigint,
    ALTER COLUMN label_id TYPE bigint;
ALTER TABLE episode
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint;
ALTER TABLE holiday
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint;
ALTER TABLE holiday_location
    ALTER COLUMN holiday_id TYPE bigint,
    ALTER COLUMN location_id TYPE bigint;
ALTER TABLE ical_import
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint;
ALTER TABLE importprocess
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint;
ALTER TABLE label
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint;
ALTER TABLE location
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint;
ALTER TABLE reference
    ALTER COLUMN requirement_id TYPE bigint,
    ALTER COLUMN cluster_id TYPE bigint,
    ALTER COLUMN pool_id TYPE bigint;
ALTER TABLE reference_pool
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint;
ALTER TABLE required_ability
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint,
    ALTER COLUMN requirement_id TYPE bigint;
ALTER TABLE requiredability_label
    ALTER COLUMN requiredability_id TYPE bigint,
    ALTER COLUMN label_id TYPE bigint;
ALTER TABLE requirement
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint,
    ALTER COLUMN cluster_id TYPE bigint;
ALTER TABLE requirement_cluster
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint;
ALTER TABLE role
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint;
ALTER TABLE role_permissions
    ALTER COLUMN role_id TYPE bigint;
ALTER TABLE user_
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint,
    ALTER COLUMN picture_id TYPE bigint;
ALTER TABLE user_role
    ALTER COLUMN user_id TYPE bigint,
    ALTER COLUMN role_id TYPE bigint;
ALTER TABLE widget
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint;
ALTER TABLE working_hours
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN employee_id TYPE bigint;
