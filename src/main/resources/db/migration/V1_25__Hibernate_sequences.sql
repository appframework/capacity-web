create sequence absence_SEQ INCREMENT 50;
select setval('absence_SEQ', (select max(id) from absence));

create sequence binary_data_SEQ INCREMENT 50;
select setval('binary_data_SEQ', (select max(id) from binary_data));

create sequence episode_SEQ INCREMENT 50;
select setval('episode_SEQ', (select max(id) from episode));

create sequence employee_SEQ INCREMENT 50;
select setval('employee_SEQ', (select max(id) from employee));

create sequence employee_episode_SEQ INCREMENT 50;
select setval('employee_episode_SEQ', (select max(id) from employee_episode));

create sequence holiday_SEQ INCREMENT 50;
select setval('holiday_SEQ', (select max(id) from holiday));

create sequence ical_import_SEQ INCREMENT 50;
select setval('ical_import_SEQ', (select max(id) from ical_import));

create sequence importprocess_SEQ INCREMENT 50;
select setval('importprocess_SEQ', (select max(id) from importprocess));

create sequence label_SEQ INCREMENT 50;
select setval('label_SEQ', (select max(id) from label));

create sequence location_SEQ INCREMENT 50;
select setval('location_SEQ', (select max(id) from location));

create sequence reference_pool_SEQ INCREMENT 50;
select setval('reference_pool_SEQ', (select max(id) from reference_pool));

create sequence required_ability_SEQ INCREMENT 50;
select setval('required_ability_SEQ', (select max(id) from required_ability));

create sequence requirement_SEQ INCREMENT 50;
select setval('requirement_SEQ', (select max(id) from requirement));

create sequence requirement_cluster_SEQ INCREMENT 50;
select setval('requirement_cluster_SEQ', (select max(id) from requirement_cluster));

create sequence role_SEQ INCREMENT 50;
select setval('role_SEQ', (select max(id) from role));

create sequence user__SEQ INCREMENT 50;
select setval('user__SEQ', (select max(id) from user_));

create sequence widget_SEQ INCREMENT 50;
select setval('widget_SEQ', (select max(id) from widget));

drop sequence if exists hibernate_sequence;