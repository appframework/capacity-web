CREATE TABLE user_ (
  id SERIAL,
  created timestamp DEFAULT NULL,
  modified timestamp DEFAULT NULL,
  email varchar(255) NOT NULL,
  login varchar(63) NOT NULL,
  name varchar(255) NOT NULL,
  password varchar(40) NOT NULL,
  id_creator int DEFAULT NULL,
  id_modificator int DEFAULT NULL,
  PRIMARY KEY (id),
  CONSTRAINT FK_user_creator FOREIGN KEY (id_creator) REFERENCES user_ (id),
  CONSTRAINT FK_user_modificator FOREIGN KEY (id_modificator) REFERENCES user_ (id)
);

CREATE INDEX FK_user_creator_IX ON user_(id_creator);
CREATE INDEX FK_user_modificator_IX ON user_(id_modificator);

CREATE TABLE role (
  id SERIAL,
  created timestamp DEFAULT NULL,
  modified timestamp DEFAULT NULL,
  name varchar(255) NOT NULL,
  id_creator int DEFAULT NULL,
  id_modificator int DEFAULT NULL,
  PRIMARY KEY (id),
  CONSTRAINT FK_role_creator FOREIGN KEY (id_creator) REFERENCES user_ (id),
  CONSTRAINT FK_role_modificator FOREIGN KEY (id_modificator) REFERENCES user_ (id)
);

CREATE INDEX FK_role_creator_IX ON role(id_creator);
CREATE INDEX FK_role_modificator_IX ON role(id_modificator);

CREATE TABLE role_permissions (
  id_role int NOT NULL,
  permission varchar(255) DEFAULT NULL,
  CONSTRAINT FK_rolepermission_role FOREIGN KEY (id_role) REFERENCES role (id)
);

CREATE INDEX FK_rolepermission_role_IX ON role_permissions(id_role);

CREATE TABLE user_role (
  id_user int NOT NULL,
  id_role int NOT NULL,
  CONSTRAINT FK_userrole_role FOREIGN KEY (id_role) REFERENCES role (id),
  CONSTRAINT FK_userrole_user FOREIGN KEY (id_user) REFERENCES user_ (id)
);

CREATE INDEX FK_userrole_role ON user_role(id_role);
CREATE INDEX FK_userrole_user ON user_role(id_user);
