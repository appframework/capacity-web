ALTER TABLE employee
ADD velocity INT NOT NULL DEFAULT 100;

CREATE TABLE episode (
  id SERIAL,
  created timestamp DEFAULT NULL,
  modified timestamp DEFAULT NULL,
  start date NOT NULL,
  "end" date NOT NULL,
  name varchar(255) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE employee_episode (
  id SERIAL,
  created timestamp DEFAULT NULL,
  modified timestamp DEFAULT NULL,
  employee_id int NOT NULL,
  episode_id int NOT NULL,
  velocity INT,
  PRIMARY KEY (id)
);

ALTER TABLE employee_episode
ADD CONSTRAINT FK_employeeepisode_employee FOREIGN KEY (employee_id) REFERENCES employee(id);

CREATE INDEX FK_employeeepisode_employee_IX ON employee_episode(employee_id);

ALTER TABLE employee_episode
ADD CONSTRAINT FK_employeeepisode_episode FOREIGN KEY (episode_id) REFERENCES episode(id);

CREATE INDEX FK_employeeepisode_episode_IX ON employee_episode(episode_id);