CREATE TABLE working_hours (
  id SERIAL,
  dayOfWeek int NOT NULL,
  start time NOT NULL,
  "end" time NOT NULL,
  employee_id int NOT NULL,
  PRIMARY KEY (id)
);

ALTER TABLE working_hours
ADD CONSTRAINT FK_workinghours_employee FOREIGN KEY (employee_id) REFERENCES employee(id);

CREATE INDEX FK_workinghours_employee_IX ON working_hours(employee_id);

ALTER TABLE employee
DROP COLUMN workHoursPerDay;
