ALTER TABLE ical_import
ADD COLUMN type varchar(10);

UPDATE ical_import SET type = 'ABSENCES';

ALTER TABLE ical_import
ALTER type SET NOT NULL;