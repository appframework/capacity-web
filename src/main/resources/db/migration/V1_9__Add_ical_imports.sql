CREATE TABLE ical_import (
  id SERIAL,
  created timestamp DEFAULT NULL,
  modified timestamp DEFAULT NULL,
  name varchar(255) NOT NULL,
  url varchar(1023) NOT NULL,
  PRIMARY KEY (id)
);
