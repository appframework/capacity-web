ALTER TABLE absence RENAME id_creator TO creator_id;
ALTER TABLE absence RENAME id_modificator TO modificator_id;

ALTER TABLE employee RENAME id_creator TO creator_id;
ALTER TABLE employee RENAME id_modificator TO modificator_id;

ALTER TABLE employee_episode RENAME id_creator TO creator_id;
ALTER TABLE employee_episode RENAME id_modificator TO modificator_id;

ALTER TABLE episode RENAME id_creator TO creator_id;
ALTER TABLE episode RENAME id_modificator TO modificator_id;

ALTER TABLE holiday RENAME id_creator TO creator_id;
ALTER TABLE holiday RENAME id_modificator TO modificator_id;

ALTER TABLE ical_import RENAME id_creator TO creator_id;
ALTER TABLE ical_import RENAME id_modificator TO modificator_id;

ALTER TABLE location RENAME id_creator TO creator_id;
ALTER TABLE location RENAME id_modificator TO modificator_id;

ALTER TABLE role RENAME id_creator TO creator_id;
ALTER TABLE role RENAME id_modificator TO modificator_id;

ALTER TABLE role_permissions RENAME id_role TO role_id;

ALTER TABLE user_ RENAME id_creator TO creator_id;
ALTER TABLE user_ RENAME id_modificator TO modificator_id;

ALTER TABLE user_role RENAME id_user TO user_id;
ALTER TABLE user_role RENAME id_role TO role_id;

