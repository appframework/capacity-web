ALTER TABLE holiday
    ADD externalId varchar(255),
    ADD icalimport_id bigint,
    ADD CONSTRAINT FK_holiday_icalimport FOREIGN KEY (icalimport_id) REFERENCES ical_import(id);

CREATE INDEX FK_holiday_icalimport_IX ON holiday(icalimport_id);
