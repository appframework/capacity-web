CREATE TABLE label_alias
(
    label_id bigint NOT NULL,
    alias    varchar(255) DEFAULT NULL,
    CONSTRAINT FK_labelalias_label FOREIGN KEY (label_id) REFERENCES label (id)
);

CREATE INDEX FK_labelalias_label_IX ON label_alias (label_id);