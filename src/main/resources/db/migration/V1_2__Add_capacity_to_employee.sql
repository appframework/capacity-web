CREATE TABLE holiday (
  id SERIAL,
  created timestamp DEFAULT NULL,
  modified timestamp DEFAULT NULL,
  date date NOT NULL,
  hoursReduction int NOT NULL,
  name varchar(255) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE location (
  id SERIAL,
  created timestamp DEFAULT NULL,
  modified timestamp DEFAULT NULL,
  name varchar(255) NOT NULL,
  PRIMARY KEY (id)
);

ALTER TABLE employee
ADD COLUMN start date DEFAULT NULL,
ADD COLUMN "end" date DEFAULT NULL,
ADD COLUMN workHoursPerDay int NOT NULL,
ADD COLUMN location_id int NULL,
ADD CONSTRAINT FK_employee_location FOREIGN KEY (location_id) REFERENCES location(id);

CREATE INDEX FK_employee_location_IX ON employee(location_id);

CREATE TABLE holiday_location (
  id SERIAL,
  holiday_id int NOT NULL,
  location_id int NOT NULL,
  PRIMARY KEY (id)
);

ALTER TABLE holiday_location
ADD CONSTRAINT FK_holidaylocation_location FOREIGN KEY (location_id) REFERENCES location(id),
ADD CONSTRAINT FK_holidaylocation_holiday FOREIGN KEY (holiday_id) REFERENCES holiday(id);

CREATE INDEX FK_holidaylocation_location_IX ON holiday_location(location_id);
CREATE INDEX FK_holidaylocation_holiday_IX ON holiday_location(holiday_id);
