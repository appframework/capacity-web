ALTER TABLE ical_import
ADD auth VARCHAR(20) DEFAULT 'NONE',
ADD username VARCHAR(50),
ADD password VARCHAR(50);

UPDATE ical_import
SET auth = 'NONE' WHERE auth IS NULL;

ALTER TABLE ical_import
ALTER COLUMN auth SET NOT NULL;
