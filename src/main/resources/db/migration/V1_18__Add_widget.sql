CREATE TABLE widget (
  id SERIAL,
  created timestamp DEFAULT NULL,
  modified timestamp DEFAULT NULL,
  name varchar(255) NOT NULL,
  type varchar(63) NOT NULL,
  configuration text NOT NULL,
  creator_id int DEFAULT NULL,
  modificator_id int DEFAULT NULL,
  PRIMARY KEY (id),
  CONSTRAINT FK_widget_creator FOREIGN KEY (creator_id) REFERENCES user_ (id),
  CONSTRAINT FK_widget_modificator FOREIGN KEY (modificator_id) REFERENCES user_ (id)
);

CREATE INDEX FK_widget_creator_IX ON widget(creator_id);
CREATE INDEX FK_widget_modificator_IX ON widget(modificator_id);
