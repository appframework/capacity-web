CREATE TABLE label (
  id SERIAL,
  created timestamp NULL,
  modified timestamp NULL,
  creator_id INT NULL,
  modificator_id INT NULL,
  name VARCHAR(255) NOT NULL,
  color VARCHAR(127) NOT NULL,
  PRIMARY KEY(id)
);

ALTER TABLE label ADD CONSTRAINT FK_label_creator FOREIGN KEY (creator_id) REFERENCES user_ (id);
ALTER TABLE label ADD CONSTRAINT FK_label_modificator FOREIGN KEY (modificator_id) REFERENCES user_ (id);

CREATE TABLE employee_label (
  employee_id INT NOT NULL,
  label_id INT NOT NULL,
  PRIMARY KEY (employee_id, label_id)
);

ALTER TABLE employee_label ADD CONSTRAINT FK_employeelabel_employee FOREIGN KEY (employee_id) REFERENCES employee (id);
ALTER TABLE employee_label ADD CONSTRAINT FK_employeelabel_label FOREIGN KEY (label_id) REFERENCES label (id);

DELETE FROM ability WHERE employee_id NOT IN (SELECT id FROM employee);

CREATE TABLE requiredability_label (
  requiredability_id INT NOT NULL,
  label_id INT NOT NULL,
  PRIMARY KEY (requiredability_id, label_id)
);

ALTER TABLE requiredability_label ADD CONSTRAINT FK_requiredabilitylabel_requiredability FOREIGN KEY (requiredability_id) REFERENCES required_ability (id);
ALTER TABLE requiredability_label ADD CONSTRAINT FK_requiredabilitylabel_label FOREIGN KEY (label_id) REFERENCES label (id);