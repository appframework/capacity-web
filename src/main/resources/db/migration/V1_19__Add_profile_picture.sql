CREATE TABLE binary_data (
  id SERIAL,
  created timestamp DEFAULT NULL,
  modified timestamp DEFAULT NULL,
  contentType varchar(511) DEFAULT NULL,
  data BYTEA,
  filename varchar(255) DEFAULT NULL,
  size bigint NOT NULL,
  creator_id int DEFAULT NULL,
  modificator_id int DEFAULT NULL,
  PRIMARY KEY (id),
  CONSTRAINT FK_binarydata_creator FOREIGN KEY (creator_id) REFERENCES user_ (id),
  CONSTRAINT FK_binarydata_modificator FOREIGN KEY (modificator_id) REFERENCES user_ (id)
);

CREATE INDEX FK_binarydata_creator_IX ON binary_data(creator_id);
CREATE INDEX FK_binarydata_modificator_IX ON binary_data(modificator_id);

ALTER TABLE user_
  ADD picture_id int DEFAULT NULL;
ALTER TABLE user_
  ADD CONSTRAINT FK_user_binarydata_picture FOREIGN KEY (picture_id) REFERENCES binary_data (id);
CREATE INDEX FK_user_binarydata_picture_IX ON user_ (picture_id);
