INSERT INTO role(name,created,modified,creator_id,modificator_id)
VALUES ('Administrators',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,NULL,NULL),
       ('Users',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,NULL,NULL);

INSERT INTO role_permissions(role_id,permission)
VALUES ((SELECT id FROM role WHERE name = 'Administrators'),'SHOW_USERS'),
       ((SELECT id FROM role WHERE name = 'Administrators'),'ADMIN_USERS'),
       ((SELECT id FROM role WHERE name = 'Administrators'),'SHOW_ROLES'),
       ((SELECT id FROM role WHERE name = 'Administrators'),'ADMIN_ROLES'),
       ((SELECT id FROM role WHERE name = 'Administrators'),'SHOW_CAPACITY'),
       ((SELECT id FROM role WHERE name = 'Administrators'),'ADMIN_CAPACITY'),
       ((SELECT id FROM role WHERE name = 'Administrators'),'VIEW_WITHOUT_CONTRACT'),
       ((SELECT id FROM role WHERE name = 'Administrators'),'SHOW_ICAL_IMPORTS'),
       ((SELECT id FROM role WHERE name = 'Administrators'),'ADMIN_ICAL_IMPORTS'),
       ((SELECT id FROM role WHERE name = 'Administrators'),'SHOW_ABSENCES'),
       ((SELECT id FROM role WHERE name = 'Administrators'),'ADMIN_ABSENCES'),
       ((SELECT id FROM role WHERE name = 'Administrators'),'SHOW_EMPLOYEES'),
       ((SELECT id FROM role WHERE name = 'Administrators'),'ADMIN_EMPLOYEES'),
       ((SELECT id FROM role WHERE name = 'Administrators'),'SHOW_LOCATIONS'),
       ((SELECT id FROM role WHERE name = 'Administrators'),'ADMIN_LOCATIONS'),
       ((SELECT id FROM role WHERE name = 'Administrators'),'SHOW_EPISODES'),
       ((SELECT id FROM role WHERE name = 'Administrators'),'ADMIN_EPISODES'),
       ((SELECT id FROM role WHERE name = 'Administrators'),'SHOW_HOLIDAYS'),
       ((SELECT id FROM role WHERE name = 'Administrators'),'ADMIN_HOLIDAYS'),
       ((SELECT id FROM role WHERE name = 'Administrators'),'SHOW_WIDGETS'),
       ((SELECT id FROM role WHERE name = 'Administrators'),'ADMIN_WIDGETS'),
       ((SELECT id FROM role WHERE name = 'Administrators'),'SHOW_REFERENCEPOOLS'),
       ((SELECT id FROM role WHERE name = 'Administrators'),'ADMIN_REFERENCEPOOLS'),
       ((SELECT id FROM role WHERE name = 'Administrators'),'SHOW_REQUIREMENTCLUSTERS'),
       ((SELECT id FROM role WHERE name = 'Administrators'),'ADMIN_REQUIREMENTCLUSTERS'),
       ((SELECT id FROM role WHERE name = 'Administrators'),'SHOW_REQUIREMENTS'),
       ((SELECT id FROM role WHERE name = 'Administrators'),'ADMIN_REQUIREMENTS'),
       ((SELECT id FROM role WHERE name = 'Administrators'),'SHOW_LABELS'),
       ((SELECT id FROM role WHERE name = 'Administrators'),'ADMIN_LABELS'),
       ((SELECT id FROM role WHERE name = 'Users'),'SHOW_CAPACITY');

INSERT INTO user_(name,login,password,email,created,modified,creator_id,modificator_id)
VALUES ('Administrator','admin','d033e22ae348aeb5660fc2140aec35850c4da997','admin@localhost',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,NULL,NULL);

INSERT INTO user_role(user_id,role_id)
VALUES ((SELECT id FROM user_ WHERE name = 'Administrator'),(SELECT id FROM role WHERE name = 'Administrators'));
