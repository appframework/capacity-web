ALTER TABLE absence
ADD COLUMN id_creator int DEFAULT NULL,
ADD COLUMN id_modificator int DEFAULT NULL,
ADD CONSTRAINT FK_absence_creator FOREIGN KEY (id_creator) REFERENCES user_(id),
ADD CONSTRAINT FK_absence_modificator FOREIGN KEY (id_modificator) REFERENCES user_(id);

CREATE INDEX FK_absence_creator_IX ON absence(id_creator);
CREATE INDEX FK_absence_modificator_IX ON absence(id_modificator);

ALTER TABLE employee
ADD COLUMN id_creator int DEFAULT NULL,
ADD COLUMN id_modificator int DEFAULT NULL,
ADD CONSTRAINT FK_employee_creator FOREIGN KEY (id_creator) REFERENCES user_(id),
ADD CONSTRAINT FK_employee_modificator FOREIGN KEY (id_modificator) REFERENCES user_(id);

CREATE INDEX FK_employee_creator_IX ON employee(id_creator);
CREATE INDEX FK_employee_modificator_IX ON employee(id_modificator);

ALTER TABLE employee_episode
ADD COLUMN id_creator int DEFAULT NULL,
ADD COLUMN id_modificator int DEFAULT NULL,
ADD CONSTRAINT FK_employeeepisode_creator FOREIGN KEY (id_creator) REFERENCES user_(id),
ADD CONSTRAINT FK_employeeepisode_modificator FOREIGN KEY (id_modificator) REFERENCES user_(id);

CREATE INDEX FK_employeeepisode_creator_IX ON employee_episode(id_creator);
CREATE INDEX FK_employeeepisode_modificator_IX ON employee_episode(id_modificator);

ALTER TABLE episode
ADD COLUMN id_creator int DEFAULT NULL,
ADD COLUMN id_modificator int DEFAULT NULL,
ADD CONSTRAINT FK_episode_creator FOREIGN KEY (id_creator) REFERENCES user_(id),
ADD CONSTRAINT FK_episode_modificator FOREIGN KEY (id_modificator) REFERENCES user_(id);

CREATE INDEX FK_episode_creator_IX ON episode(id_creator);
CREATE INDEX FK_episode_modificator_IX ON episode(id_modificator);

ALTER TABLE holiday
ADD COLUMN id_creator int DEFAULT NULL,
ADD COLUMN id_modificator int DEFAULT NULL,
ADD CONSTRAINT FK_holiday_creator FOREIGN KEY (id_creator) REFERENCES user_(id),
ADD CONSTRAINT FK_holiday_modificator FOREIGN KEY (id_modificator) REFERENCES user_(id);

CREATE INDEX FK_holiday_creator_IX ON holiday(id_creator);
CREATE INDEX FK_holiday_modificator_IX ON holiday(id_modificator);    
    
ALTER TABLE ical_import
ADD COLUMN id_creator int DEFAULT NULL,
ADD COLUMN id_modificator int DEFAULT NULL,
ADD CONSTRAINT FK_icalimport_creator FOREIGN KEY (id_creator) REFERENCES user_(id),
ADD CONSTRAINT FK_icalimport_modificator FOREIGN KEY (id_modificator) REFERENCES user_(id);

CREATE INDEX FK_icalimport_creator_IX ON ical_import(id_creator);
CREATE INDEX FK_icalimport_modificator_IX ON ical_import(id_modificator);

ALTER TABLE location
ADD COLUMN id_creator int DEFAULT NULL,
ADD COLUMN id_modificator int DEFAULT NULL,
ADD CONSTRAINT FK_location_creator FOREIGN KEY (id_creator) REFERENCES user_(id),
ADD CONSTRAINT FK_location_modificator FOREIGN KEY (id_modificator) REFERENCES user_(id);

CREATE INDEX FK_location_creator_IX ON location(id_creator);
CREATE INDEX FK_location_modificator_IX ON location(id_modificator);
