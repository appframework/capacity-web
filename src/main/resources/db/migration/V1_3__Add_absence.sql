CREATE TABLE absence (
  id SERIAL,
  created timestamp DEFAULT NULL,
  modified timestamp DEFAULT NULL,
  start date NOT NULL,
  "end" date NOT NULL,
  reason varchar(255) NOT NULL,
  employee_id int NOT NULL,
  PRIMARY KEY (id)
);

ALTER TABLE absence
ADD CONSTRAINT FK_absence_employee FOREIGN KEY (employee_id) REFERENCES employee(id);

CREATE INDEX FK_absence_employee_IX ON absence(employee_id);
