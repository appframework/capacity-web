ALTER TABLE absence
ADD icalimport_id int,
ADD CONSTRAINT FK_absence_icalimport FOREIGN KEY (icalimport_id) REFERENCES ical_import(id);

CREATE INDEX FK_absence_icalimport_IX ON absence(icalimport_id);

DELETE FROM absence WHERE externalId IS NOT NULL;
