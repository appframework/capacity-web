TRUNCATE TABLE
    absence,
    binary_data,
    episode,
    employee,
    employee_episode,
    employee_label,
    holiday,
    holiday_location,
    ical_import,
    importprocess,
    label,
    label_alias,
    location,
    reference,
    reference_pool,
    required_ability,
    required_ability_skill,
    requiredability_label,
    requirement,
    requirement_cluster,
    role,
    role_permissions,
    user_,
    user_role,
    widget,
    working_hours;

INSERT INTO user_(id,name,login,password,email,created,modified,creator_id,modificator_id) VALUES (1,'Tom Tester','tom','ab4d8d2a5f480a137067da17100271cd176607a1','tom@localhost',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,NULL);
INSERT INTO user_(id,name,login,password,email,created,modified,creator_id,modificator_id) VALUES (2,'XXX','xxx','b60d121b438a380c343d5ec3c2037564b82ffef3','xxx@localhost',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,NULL);
INSERT INTO user_(id,name,login,password,email,created,modified,creator_id,modificator_id) VALUES (3,'YYY','yyy','186154712b2d5f6791d85b9a0987b98fa231779c','yyy@localhost',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,NULL);
ALTER SEQUENCE user__SEQ RESTART WITH 4;

INSERT INTO role(id,name,created,modified,creator_id,modificator_id) VALUES (1,'Administrators',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,NULL);
INSERT INTO role(id,name,created,modified,creator_id,modificator_id) VALUES (2,'Users',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,NULL);
INSERT INTO role(id,name,created,modified,creator_id,modificator_id) VALUES (3,'No Permissions',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,NULL);
ALTER SEQUENCE role_SEQ RESTART WITH 4;

INSERT INTO role_permissions(role_id,permission) VALUES (1, 'SHOW_USERS'), (1, 'ADMIN_USERS'), (1, 'SHOW_ROLES'), (1, 'ADMIN_ROLES'), (1, 'SHOW_CAPACITY'), (1, 'ADMIN_CAPACITY'), (1, 'VIEW_WITHOUT_CONTRACT'), (1, 'SHOW_ICAL_IMPORTS'), (1, 'ADMIN_ICAL_IMPORTS'), (1, 'ADMIN_ABSENCES'), (1, 'SHOW_ABSENCES'), (1, 'ADMIN_EMPLOYEES'), (1, 'SHOW_EMPLOYEES'), (1, 'ADMIN_LOCATIONS'), (1, 'SHOW_LOCATIONS'), (1, 'ADMIN_EPISODES'), (1, 'SHOW_EPISODES'), (1, 'ADMIN_HOLIDAYS'), (1, 'SHOW_HOLIDAYS'), (1, 'ADMIN_WIDGETS'), (1, 'SHOW_WIDGETS'), (1, 'ADMIN_REFERENCEPOOLS'), (1, 'SHOW_REFERENCEPOOLS'), (1, 'ADMIN_REQUIREMENTCLUSTERS'), (1, 'SHOW_REQUIREMENTCLUSTERS'), (1, 'ADMIN_REQUIREMENTS'), (1, 'SHOW_REQUIREMENTS'), (1, 'ADMIN_LABELS'), (1, 'SHOW_LABELS');

INSERT INTO user_role(user_id,role_id) VALUES (1,1);
INSERT INTO user_role(user_id,role_id) VALUES (2,2);
INSERT INTO user_role(user_id,role_id) VALUES (3,3);

INSERT INTO location (id,created,modified,creator_id,modificator_id,name) VALUES (1,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,1,'Somewhere');
INSERT INTO location (id,created,modified,creator_id,modificator_id,name) VALUES (2,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,1,'Elsewhere');
ALTER SEQUENCE location_SEQ RESTART WITH 3;

INSERT INTO employee (id,created,modified,creator_id,modificator_id,name,email,start,"end",location_id,color,velocity,vacationDaysPerYear) VALUES ( 1,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,1,'User 2015 complete','2015@complete','2015-01-01','2015-12-31',NULL,'ff0000','50',28);
INSERT INTO employee (id,created,modified,creator_id,modificator_id,name,email,start,"end",location_id,color,velocity,vacationDaysPerYear) VALUES ( 2,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,1,'User always','always',NULL,NULL,NULL,'ff0000','50',28);
INSERT INTO employee (id,created,modified,creator_id,modificator_id,name,email,start,"end",location_id,color,velocity,vacationDaysPerYear) VALUES ( 3,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,1,'User until end september','until@september',NULL,'2015-09-30',NULL,'ff0000','50',28);
INSERT INTO employee (id,created,modified,creator_id,modificator_id,name,email,start,"end",location_id,color,velocity,vacationDaysPerYear) VALUES ( 4,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,1,'User from start october','from@october','2015-10-01',NULL,NULL,'ff0000','50',28);
INSERT INTO employee (id,created,modified,creator_id,modificator_id,name,email,start,"end",location_id,color,velocity,vacationDaysPerYear) VALUES ( 5,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,1,'User until end october','until@october',NULL,'2015-10-31',NULL,'ff0000','50',28);
INSERT INTO employee (id,created,modified,creator_id,modificator_id,name,email,start,"end",location_id,color,velocity,vacationDaysPerYear) VALUES ( 6,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,1,'User from start november','from@november','2015-11-01',NULL,NULL,'ff0000','50',28);
INSERT INTO employee (id,created,modified,creator_id,modificator_id,name,email,start,"end",location_id,color,velocity,vacationDaysPerYear) VALUES ( 7,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,1,'User until mid october','until.mid@october',NULL,'2015-10-15',NULL,'ff0000','50',28);
INSERT INTO employee (id,created,modified,creator_id,modificator_id,name,email,start,"end",location_id,color,velocity,vacationDaysPerYear) VALUES ( 8,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,1,'User from mid october','from.mid@october','2015-10-15',NULL,NULL,'ff0000','50',28);
INSERT INTO employee (id,created,modified,creator_id,modificator_id,name,email,start,"end",location_id,color,velocity,vacationDaysPerYear) VALUES ( 9,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,1,'User complete october','october@complete','2015-10-01','2015-10-31',NULL,'ff0000','50',28);
INSERT INTO employee (id,created,modified,creator_id,modificator_id,name,email,start,"end",location_id,color,velocity,vacationDaysPerYear) VALUES (10,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,1,'User partial october','october@partial','2015-10-10','2015-10-20',NULL,'ff0000','50',28);
INSERT INTO employee (id,created,modified,creator_id,modificator_id,name,email,start,"end",location_id,color,velocity,vacationDaysPerYear) VALUES (13,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,1,'User from jan until end september','until@september','2015-01-01','2015-09-30',NULL,'ff0000','50',28);
INSERT INTO employee (id,created,modified,creator_id,modificator_id,name,email,start,"end",location_id,color,velocity,vacationDaysPerYear) VALUES (14,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,1,'User from start october until dec','from@october','2015-10-01','2015-12-31',NULL,'ff0000','50',28);
INSERT INTO employee (id,created,modified,creator_id,modificator_id,name,email,start,"end",location_id,color,velocity,vacationDaysPerYear) VALUES (15,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,1,'User from jan until end october','until@october','2015-01-01','2015-10-31',NULL,'ff0000','50',28);
INSERT INTO employee (id,created,modified,creator_id,modificator_id,name,email,start,"end",location_id,color,velocity,vacationDaysPerYear) VALUES (16,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,1,'User from start november until dec','from@november','2015-11-01','2015-12-31',NULL,'ff0000','50',28);
INSERT INTO employee (id,created,modified,creator_id,modificator_id,name,email,start,"end",location_id,color,velocity,vacationDaysPerYear) VALUES (17,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,1,'User from jan until mid october','until.mid@october','2015-01-01','2015-10-15',1,'ff0000','50',28);
INSERT INTO employee (id,created,modified,creator_id,modificator_id,name,email,start,"end",location_id,color,velocity,vacationDaysPerYear) VALUES (18,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,1,'User from mid october until dec','from.mid@november','2015-10-15','2015-12-31',2,'ff0000','50',28);
ALTER SEQUENCE employee_SEQ RESTART WITH 19;

INSERT INTO holiday (id,created,modified,creator_id,modificator_id,name,date,hoursReduction) VALUES (1,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,1,'Some vacation 14','2015-01-14',8);
INSERT INTO holiday (id,created,modified,creator_id,modificator_id,name,date,hoursReduction) VALUES (2,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,1,'Some vacation 15','2015-01-15',8);
INSERT INTO holiday (id,created,modified,creator_id,modificator_id,name,date,hoursReduction) VALUES (3,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,1,'Some vacation 16','2015-01-16',8);
INSERT INTO holiday (id,created,modified,creator_id,modificator_id,name,date,hoursReduction) VALUES (4,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,1,'Some vacation 13','2015-01-13',8);
ALTER SEQUENCE holiday_SEQ RESTART WITH 5;

INSERT INTO holiday_location (location_id,holiday_id) VALUES (1,1);
INSERT INTO holiday_location (location_id,holiday_id) VALUES (2,2);
INSERT INTO holiday_location (location_id,holiday_id) VALUES (1,3);
INSERT INTO holiday_location (location_id,holiday_id) VALUES (2,3);

INSERT INTO working_hours (dayOfWeek,start,"end",employee_id) VALUES (2,'08:00:00','16:00:00',1);
INSERT INTO working_hours (dayOfWeek,start,"end",employee_id) VALUES (2,'08:00:00','16:00:00',2);
INSERT INTO working_hours (dayOfWeek,start,"end",employee_id) VALUES (2,'08:00:00','16:00:00',3);
INSERT INTO working_hours (dayOfWeek,start,"end",employee_id) VALUES (2,'08:00:00','16:00:00',5);
INSERT INTO working_hours (dayOfWeek,start,"end",employee_id) VALUES (2,'08:00:00','16:00:00',7);
INSERT INTO working_hours (dayOfWeek,start,"end",employee_id) VALUES (2,'08:00:00','16:00:00',13);
INSERT INTO working_hours (dayOfWeek,start,"end",employee_id) VALUES (2,'08:00:00','16:00:00',15);

INSERT INTO label (id,created,modified,creator_id,modificator_id,name,color) VALUES (1,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,1,'Working','112233');
INSERT INTO label (id,created,modified,creator_id,modificator_id,name,color) VALUES (2,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,1,'Relaxing','123456');
ALTER SEQUENCE label_SEQ RESTART WITH 5;

INSERT INTO employee_label(employee_id,label_id) VALUES (1,1);
INSERT INTO employee_label(employee_id,label_id) VALUES (2,1);
INSERT INTO employee_label(employee_id,label_id) VALUES (2,2);
INSERT INTO employee_label(employee_id,label_id) VALUES (3,2);

INSERT INTO absence (id,created,modified,creator_id,modificator_id,employee_id,start,"end",reason,externalId) VALUES (1,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,1,1,'2015-02-01','2015-02-28','Whole February',NULL);
ALTER SEQUENCE absence_SEQ RESTART WITH 2;

INSERT INTO episode (id,created,modified,creator_id,modificator_id,name,start,"end") VALUES (1,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,1,'The Wrath of Khan','2015-01-01','2015-06-30');
INSERT INTO episode (id,created,modified,creator_id,modificator_id,name,start,"end") VALUES (2,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,1,'Into Darkness','2015-07-01','2015-12-31');
ALTER SEQUENCE episode_SEQ RESTART WITH 3;

INSERT INTO employee_episode (id,created,modified,creator_id,modificator_id,employee_id,episode_id,velocity) VALUES (1,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,1,1,1,NULL);
INSERT INTO employee_episode (id,created,modified,creator_id,modificator_id,employee_id,episode_id,velocity) VALUES (2,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,1,1,2,NULL);
INSERT INTO employee_episode (id,created,modified,creator_id,modificator_id,employee_id,episode_id,velocity) VALUES (3,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,1,2,1,33);
INSERT INTO employee_episode (id,created,modified,creator_id,modificator_id,employee_id,episode_id,velocity) VALUES (4,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,1,2,2,66);
INSERT INTO employee_episode (id,created,modified,creator_id,modificator_id,employee_id,episode_id,velocity) VALUES (5,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,1,3,1,NULL);
INSERT INTO employee_episode (id,created,modified,creator_id,modificator_id,employee_id,episode_id,velocity) VALUES (6,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,1,3,2,15);
INSERT INTO employee_episode (id,created,modified,creator_id,modificator_id,employee_id,episode_id,velocity) VALUES (7,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,1,4,2,15);
ALTER SEQUENCE employee_episode_SEQ RESTART WITH 8;

INSERT INTO ical_import (id,created,modified,creator_id,modificator_id,name,url,lastImported,type) VALUES (1,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,1,'Test','http://localhost',NULL,'HOLIDAYS');
ALTER SEQUENCE ical_import_SEQ RESTART WITH 2;

INSERT INTO widget (id,created,modified,creator_id,modificator_id,name,type,configuration) VALUES (1,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,1,'Widget 01','ability_export','{"color": "123456","ability": "Working","topleft": "Relaxing","topmiddle": "Relaxing","topright": "Relaxing","middleleft": "Relaxing","middleright": "Relaxing|Relaxing|Relaxing","bottom": "Relaxing"}');
ALTER SEQUENCE widget_SEQ RESTART WITH 2;

INSERT INTO requirement_cluster (id,created,modified,creator_id,modificator_id,name) VALUES (1,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,1,'Cluster 1');
ALTER SEQUENCE requirement_cluster_SEQ RESTART WITH 2;

