package de.christophbrill.capacity.ui;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import de.christophbrill.capacity.ui.dto.Employee;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
@TestSecurity(user = "tom", roles = {"SHOW_EMPLOYEES"})
public class AbsentServiceTest {

	@Test
	public void checkAbsencesTenthOfFebruary() throws IOException {
		String absences = given().queryParam("date", "2015-02-10").when().get("/rest/absent").asString();

		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());

		List<Employee> employees = mapper.readValue(absences, new TypeReference<>() {
		});

		assertThat(employees).overridingErrorMessage("Two employees expected: ID 1 has an absence, ID 17 has non-working day").hasSize(2);

		assertThat(employees)
				.hasSize(2)
				.extracting((e) -> e.id)
				.containsExactlyInAnyOrder(1L, 17L);
	}

	@Test
	public void checkAbsencesThirdOfMarch() throws IOException {
		String absences = given().queryParam("date", "2015-03-03").when().get("/rest/absent").asString();

		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());

		List<Employee> employees = mapper.readValue(absences, new TypeReference<>() {
		});

		assertThat(employees).overridingErrorMessage("One employee expected: ID 17 has non-working day").hasSize(1);

		assertThat(employees)
				.hasSize(1)
				.extracting((e) -> e.id)
				.contains(17L);

	}

	@Test
	public void checkAbsencesToday() throws IOException {
		String absences = given().when().get("/rest/absent").asString();

		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());

		List<Employee> employees = mapper.readValue(absences, new TypeReference<>() {
		});

		if (DayOfWeek.TUESDAY == LocalDate.now().getDayOfWeek()) {
			assertThat(employees).overridingErrorMessage("Three employees expected: all have non-working day").hasSize(3);
		} else {
			assertThat(employees).overridingErrorMessage("Four employees expected: all have non-working day").hasSize(4);

			assertThat(employees).extracting((e) -> e.id).contains(2L);
		}
		assertThat(employees).extracting((e) -> e.id).contains(8L, 6L, 4L);
	}
}
