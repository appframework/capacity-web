package de.christophbrill.capacity.ui;

import de.christophbrill.appframeworktesting.ui.AbstractCRUDTest;
import de.christophbrill.capacity.ui.dto.Absence;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;

import java.time.LocalDate;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
@TestSecurity(user = "tom", roles = {"ADMIN_ABSENCES", "SHOW_ABSENCES"})
public class AbsenceCRUDTest extends AbstractCRUDTest<Absence> {

	@Override
	protected Absence createFixture() {
		Absence fixture = new Absence();
		fixture.start = LocalDate.now();
		fixture.end = LocalDate.now();
		fixture.reason = UUID.randomUUID().toString();
		fixture.employeeId = 17L;
		return fixture;
	}

	@Override
	protected String getPath() {
		return "absence";
	}

	@Override
	protected Class<Absence> getFixtureClass() {
		return Absence.class;
	}

	@Override
	protected void modifyFixture(Absence fixture) {
		fixture.reason = UUID.randomUUID().toString();
	}

	@Override
	protected void compareDtos(Absence fixture, Absence created) {
		assertThat(created.start).isEqualTo(fixture.start);
		assertThat(created.end).isEqualTo(fixture.end);
		assertThat(created.reason).isEqualTo(fixture.reason);
		assertThat(created.employeeId).isEqualTo(fixture.employeeId);
	}

}
