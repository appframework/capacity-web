package de.christophbrill.capacity.ui;

import de.christophbrill.appframeworktesting.ui.AbstractCRUDTest;
import de.christophbrill.capacity.ui.dto.Contract;
import de.christophbrill.capacity.ui.dto.Employee;
import de.christophbrill.capacity.ui.dto.ExportConfiguration;
import de.christophbrill.capacity.ui.dto.Label;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@QuarkusTest
@TestSecurity(user = "tom", roles = {"ADMIN_EMPLOYEES", "SHOW_EMPLOYEES", "VIEW_WITHOUT_CONTRACT"})
public class EmployeeCRUDTest extends AbstractCRUDTest<Employee> {

	@Override
	protected Employee createFixture() {
		Employee fixture = new Employee();
		fixture.name = UUID.randomUUID().toString();
		fixture.email = UUID.randomUUID().toString();
		Label o = new Label();
		o.name = "Working";
		o.id = 1L;
		fixture.abilities = Collections.singleton(o);
		fixture.color = "#00ff00";
		Contract contract = new Contract();
		contract.vacationDaysPerYear = 2;
		fixture.contract = contract;
		return fixture;
	}

	@Override
	protected String getPath() {
		return "employee";
	}

	@Override
	protected Class<Employee> getFixtureClass() {
		return Employee.class;
	}

	@Override
	protected void modifyFixture(Employee fixture) {
		fixture.name = UUID.randomUUID().toString();
	}

	@Override
	protected void compareDtos(Employee fixture, Employee created) {
		assertThat(created.name).isEqualTo(fixture.name);
		assertThat(created.email).isEqualTo(fixture.email);
		assertThat(created.abilities.size()).isEqualTo(fixture.abilities.size());
		assertThat(created.abilities.iterator().next().name).isEqualTo(fixture.abilities.iterator().next().name);
	}

	@Test
	public void testGetByLocation() {
		String path = getPath();

		// when: We try to load the employees at location "1"
		List<Employee> result = given().when().get("/rest/" + path + "/location/1").then().extract().body().jsonPath().getList(".", getFixtureClass());
		// Then: We get one employee with ID 17
		assertThat(result).hasSize(1);
		assertThat(result.getFirst().id).isEqualTo(17L);

		// when: We try to load the employees at location "2"
		result = given().when().get("/rest/" + path + "/location/2").then().extract().body().jsonPath().getList(".", getFixtureClass());
		// Then: We get one employee with ID 18
		assertThat(result).hasSize(1);
		assertThat(result.getFirst().id).isEqualTo(18L);

		// when: We try to load the employees at location "3"
		result = given().when().get("/rest/" + path + "/location/3").then().extract().body().jsonPath().getList(".", getFixtureClass());
		// Then: We get no employees
		assertThat(result).isEmpty();
	}
	
	@Test
	public void testExportConfluence() {
		String path = getPath();
		
		// when: we load the ability matrix
		ExportConfiguration configuration = new ExportConfiguration();
		configuration.abilitiesX = Collections.singletonList("Working");
		Map<String, List<String>> abilitiesY = new HashMap<>();
		abilitiesY.put("Not actual work", Collections.singletonList("Relaxing"));
		configuration.abilitiesY = abilitiesY;
		String result = given().contentType(ContentType.JSON).when().body(configuration).post("/rest/" + path + "/export/confluence").asString();
		// then: we get a perfectly formatted one
		assertThat(result).isEqualTo("""
                || Ability || Working ||
                || Not actual work | [~always] |\s
                """);
	}

}
