package de.christophbrill.capacity.ui;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import de.christophbrill.capacity.ui.dto.WorkingHoursPerEmployee;
import de.christophbrill.capacity.ui.dto.WorkingHoursRequest;
import io.quarkus.security.runtime.QuarkusPrincipal;
import io.quarkus.security.runtime.QuarkusSecurityIdentity;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestAuthController;
import io.quarkus.test.security.TestIdentityAssociation;
import io.quarkus.test.security.TestSecurity;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import jakarta.enterprise.inject.spi.CDI;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
@TestSecurity(user = "tom", roles = {"ADMIN_REQUIREMENTS", "SHOW_REQUIREMENTS"})
public class CapacityServiceTest {
	
	@BeforeEach
	public void before() {
		QuarkusSecurityIdentity user = QuarkusSecurityIdentity.builder()
				.setPrincipal(new QuarkusPrincipal("tom"))
				.addRoles(new HashSet<>(Arrays.asList("ADMIN_REQUIREMENTS", "SHOW_REQUIREMENTS")))
				.build();
		CDI.current().select(TestAuthController.class).get().setEnabled(true);
		CDI.current().select(TestIdentityAssociation.class).get().setTestIdentity(user);
	}

	@Test
	public void testEpisodeOne_withAbilities_And() throws IOException {
		WorkingHoursRequest request = new WorkingHoursRequest();
		request.episodeId = 1L;
		request.filter = Collections.singletonList(Arrays.asList(1L, 2L));
		String workingHours = given().contentType(ContentType.JSON).when().body(request).post("/rest/capacity/workinghours").asString();

		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());

		List<WorkingHoursPerEmployee> workingHoursPerEmployee = mapper.readValue(workingHours,
				new TypeReference<>() {
				});

		assertThat(workingHoursPerEmployee)
				.overridingErrorMessage("One employees assigned to episode 1 having both Working and Relaxing abilities")
				.hasSize(1)
				.extracting((e) -> e.employee)
				.extracting((e) -> e.id)
				.contains(2L);
	}

	@Test
	public void testEpisodeOne_withAbilities_Or() throws IOException {
		WorkingHoursRequest request = new WorkingHoursRequest();
		request.episodeId = 1L;
		request.filter = Arrays.asList(Collections.singletonList(1L), Collections.singletonList(2L));
		String workingHours = given().contentType(ContentType.JSON).when().body(request).post("/rest/capacity/workinghours").asString();

		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());

		List<WorkingHoursPerEmployee> workingHoursPerEmployee = mapper.readValue(workingHours,
				new TypeReference<>() {
				});

		assertThat(workingHoursPerEmployee)
				.overridingErrorMessage("Three employees are assigned to episode 1, having either Working or Relaxing as ability")
				.hasSize(3)
				.extracting((e) -> e.employee)
				.extracting((e) -> e.id)
				.containsExactlyInAnyOrder(1L, 2L, 3L);
	}

	@Test
	public void testEpisodeOne_withAbilities_OnlyOne() throws IOException {
		WorkingHoursRequest request = new WorkingHoursRequest();
		request.episodeId = 1L;
		request.filter = Collections.singletonList(Collections.singletonList(1L));
		String workingHours = given().contentType(ContentType.JSON).when().body(request).post("/rest/capacity/workinghours").asString();

		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());

		List<WorkingHoursPerEmployee> workingHoursPerEmployee = mapper.readValue(workingHours,
				new TypeReference<>() {
				});

		assertThat(workingHoursPerEmployee)
				.overridingErrorMessage("Two employees are assigned to episode 1, having either Working ability")
				.hasSize(2)
				.extracting((e) -> e.employee)
				.extracting((e) -> e.id)
				.containsExactlyInAnyOrder(1L, 2L);
	}

	@Test
	public void testEpisodeOne() throws IOException {
		WorkingHoursRequest request = new WorkingHoursRequest();
		request.episodeId = 1L;
		String workingHours = given().contentType(ContentType.JSON).when().body(request).post("/rest/capacity/workinghours").asString();

		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());

		List<WorkingHoursPerEmployee> workingHoursPerEmployee = mapper.readValue(workingHours,
				new TypeReference<>() {
				});

		assertThat(workingHoursPerEmployee)
				.overridingErrorMessage("Three employees are assigned to episode 1")
				.hasSize(3)
				.extracting((e) -> e.employee)
				.extracting((e) -> e.id)
				.containsExactlyInAnyOrder(1L, 2L, 3L);
	}

	@Test
	public void testEpisodeTwo() throws IOException {
		WorkingHoursRequest request = new WorkingHoursRequest();
		request.episodeId = 2L;
		String workingHours = given().contentType(ContentType.JSON).when().body(request).post("/rest/capacity/workinghours").asString();

		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());

		List<WorkingHoursPerEmployee> workingHoursPerEmployee = mapper.readValue(workingHours,
				new TypeReference<>() {
				});

		assertThat(workingHoursPerEmployee)
				.overridingErrorMessage("Four employees are assigned to episode 2, one does not have a valid contract")
				.hasSize(4)
				.extracting((e) -> e.employee)
				.extracting((e) -> e.id)
				.containsExactlyInAnyOrder(1L, 2L, 3L, 4L);
	}

	@Test
	public void testEpisodeThree() {
		WorkingHoursRequest request = new WorkingHoursRequest();
		request.episodeId = 3L;

		assertThat(given().contentType(ContentType.JSON).when().body(request).post("/rest/capacity/workinghours").statusCode()).isEqualTo(400);
	}

	@Test
	public void testDuration() throws IOException {
		WorkingHoursRequest request = new WorkingHoursRequest();
		request.start = LocalDate.parse("2015-01-01");
		request.end = LocalDate.parse("2015-06-30");
		String workingHours = given().contentType(ContentType.JSON).when().body(request).post("/rest/capacity/workinghours").asString();

		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());

		List<WorkingHoursPerEmployee> horkingHoursPerEmployee = mapper.readValue(workingHours,
				new TypeReference<>() {
				});

		assertThat(horkingHoursPerEmployee)
				.overridingErrorMessage("Eight employees are working from 2015-01-01 to 2015-06-30")
				.hasSize(8)
				.extracting((e) -> e.employee)
				.extracting((e) -> e.id)
				.containsExactlyInAnyOrder(1L, 2L, 15L, 13L, 17L, 5L, 3L, 7L);
	}

	@Test
	public void testDurationEmpty() throws IOException {
		WorkingHoursRequest request = new WorkingHoursRequest();
		String workingHours = given().contentType(ContentType.JSON).when().body(request).post("/rest/capacity/workinghours").asString();

		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());

		List<WorkingHoursPerEmployee> horkingHoursPerEmployee = mapper.readValue(workingHours,
				new TypeReference<>() {
				});

		assertThat(horkingHoursPerEmployee)
				.overridingErrorMessage("Four employees are working indefinitely")
				.hasSize(4)
				.extracting((e) -> e.employee)
				.extracting((e) -> e.id)
				.containsExactlyInAnyOrder(2L, 8L, 6L, 4L);
	}

}
