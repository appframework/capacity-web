package de.christophbrill.capacity.ui;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
@TestSecurity(user = "tom", roles = {"SHOW_EMPLOYEES"})
public class AbilityServiceTest {

	@Test
	public void testExportSvg() {
		String actual = given().when().get("rest/abilities/export/1.svg").then().statusCode(200).extract().asString();
		assertThat(actual).isNotNull();
	}

}
