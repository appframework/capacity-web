package de.christophbrill.capacity.ui;

import de.christophbrill.appframeworktesting.ui.AbstractCRUDTest;
import de.christophbrill.capacity.ui.dto.Holiday;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;

import java.time.LocalDate;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
@TestSecurity(user = "tom", roles = {"ADMIN_HOLIDAYS", "SHOW_HOLIDAYS"})
public class HolidayCRUDTest  extends AbstractCRUDTest<Holiday> {

	@Override
	protected Holiday createFixture() {
		Holiday fixture = new Holiday();
		fixture.date = LocalDate.now();
		fixture.name = UUID.randomUUID().toString();
		return fixture;
	}

	@Override
	protected String getPath() {
		return "holiday";
	}

	@Override
	protected Class<Holiday> getFixtureClass() {
		return Holiday.class;
	}

	@Override
	protected void modifyFixture(Holiday fixture) {
		fixture.name = UUID.randomUUID().toString();
	}

	@Override
	protected void compareDtos(Holiday fixture, Holiday created) {
		assertThat(created.date).isEqualTo(fixture.date);
		assertThat(created.name).isEqualTo(fixture.name);
	}

}
