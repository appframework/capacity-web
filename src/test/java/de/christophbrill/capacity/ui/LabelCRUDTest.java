package de.christophbrill.capacity.ui;

import de.christophbrill.appframeworktesting.ui.AbstractCRUDTest;
import de.christophbrill.capacity.ui.dto.Label;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;

import java.util.UUID;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@QuarkusTest
@TestSecurity(user = "tom", roles = {"ADMIN_LABELS", "SHOW_LABELS"})
public class LabelCRUDTest extends AbstractCRUDTest<Label> {

    @Override
    protected Label createFixture() {
        Label fixture = new Label();
        fixture.name = UUID.randomUUID().toString();
        fixture.color = "123456";
        return fixture;
    }

    @Override
    protected String getPath() {
        return "label";
    }

    @Override
    protected Class<Label> getFixtureClass() {
        return Label.class;
    }

    @Override
    protected void modifyFixture(Label fixture) {
        fixture.name = UUID.randomUUID().toString();
    }

    @Override
    protected void compareDtos(Label fixture, Label created) {
        assertThat(created.name).isEqualTo(fixture.name);
    }
}
