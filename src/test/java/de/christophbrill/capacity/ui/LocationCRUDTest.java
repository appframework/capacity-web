package de.christophbrill.capacity.ui;

import de.christophbrill.appframeworktesting.ui.AbstractCRUDTest;
import de.christophbrill.capacity.ui.dto.Location;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
@TestSecurity(user = "tom", roles = {"ADMIN_LOCATIONS", "SHOW_LOCATIONS"})
public class LocationCRUDTest extends AbstractCRUDTest<Location> {

	@Override
	protected Location createFixture() {
		Location fixture = new Location();
		fixture.name = UUID.randomUUID().toString();
		return fixture;
	}

	@Override
	protected String getPath() {
		return "location";
	}

	@Override
	protected Class<Location> getFixtureClass() {
		return Location.class;
	}

	@Override
	protected void modifyFixture(Location fixture) {
		fixture.name = UUID.randomUUID().toString();
	}

	@Override
	protected void compareDtos(Location fixture, Location created) {
		assertThat(created.name).isEqualTo(fixture.name);
	}

}
