package de.christophbrill.capacity.ui;

import de.christophbrill.appframeworktesting.ui.AbstractCRUDTest;
import de.christophbrill.capacity.ui.dto.Episode;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;

import java.time.LocalDate;
import java.util.Collections;
import java.util.UUID;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@QuarkusTest
@TestSecurity(user = "tom", roles = {"ADMIN_EPISODES", "SHOW_EPISODES"})
public class EpisodeCRUDTest extends AbstractCRUDTest<Episode> {

	// For testing purposes
	/*
	static {
		RestAssured.defaultParser = Parser.JSON;
		RestAssured.filters(new RequestLoggingFilter(), new ResponseLoggingFilter());
	}
	*/

	@Override
	protected Episode createFixture() {
		Episode fixture = new Episode();
		fixture.start = LocalDate.now();
		fixture.end = LocalDate.now();
		fixture.name = UUID.randomUUID().toString();
		fixture.employeeEpisodes = Collections.emptyList();
		return fixture;
	}

	@Override
	protected String getPath() {
		return "episode";
	}

	@Override
	protected Class<Episode> getFixtureClass() {
		return Episode.class;
	}

	@Override
	protected void modifyFixture(Episode fixture) {
		fixture.name = UUID.randomUUID().toString();
	}

	@Override
	protected void compareDtos(Episode fixture, Episode created) {
		assertThat(created.start).isEqualTo(fixture.start);
		assertThat(created.end).isEqualTo(fixture.end);
		assertThat(created.name).isEqualTo(fixture.name);
	}

}
