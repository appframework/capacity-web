package de.christophbrill.capacity.ui;

import biweekly.Biweekly;
import biweekly.ICalendar;
import biweekly.component.VEvent;
import biweekly.util.DateTimeComponents;
import biweekly.util.ICalDate;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
@TestSecurity(user = "tom", roles = {"SHOW_HOLIDAYS", "SHOW_ABSENCES"})
public class CalendarServiceTest {

	@Test
	public void testHolidaysIcs() {
		String ics = given().when().get("/rest/calendar/holidays.ics").asString();

		ICalendar calendar = Biweekly.parse(ics).first();
		List<VEvent> components = calendar.getEvents();

		assertThat(components).hasSize(4);
	}

	@SuppressWarnings("deprecation")
	@Test
	public void testAbsencesIcs() {
		String ics = given().when().get("/rest/calendar/absences.ics").asString();

		ICalendar calendar = Biweekly.parse(ics).first();
		List<VEvent> components = calendar.getEvents();

		assertThat(components)
				.overridingErrorMessage("One ansence defined in import.sql, 'Whole February: User 2015 complete'")
				.hasSizeGreaterThanOrEqualTo(1);

		// Check properties
		VEvent component = components.getFirst();
		assertThat(component.getSummary().getValue())
				.isNotNull()
				.isEqualTo("Whole February: User 2015 complete");
		assertThat(component.getDateStart().getValue().toGMTString())
				.isNotNull()
				.isEqualTo(new ICalDate(DateTimeComponents.parse("20150201Z"), false).toGMTString());
		assertThat(component.getDateEnd().getValue().toGMTString())
				.isNotNull()
				.isEqualTo(new ICalDate(DateTimeComponents.parse("20150228Z"), false).toGMTString());
	}

	@Test
	public void testInvalid() throws IOException {
		assertThat(given().when().get("/rest/calendar/invalid.ics").statusCode()).isEqualTo(404);
	}
}
