package de.christophbrill.capacity.ui;

import de.christophbrill.appframeworktesting.ui.AbstractCRUDTest;
import de.christophbrill.capacity.ui.dto.Priority;
import de.christophbrill.capacity.ui.dto.Requirement;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
@TestSecurity(user = "tom", roles = {"ADMIN_REQUIREMENTS", "SHOW_REQUIREMENTS"})
public class RequirementCRUDTest extends AbstractCRUDTest<Requirement> {

    @Override
    protected Requirement createFixture() {
        Requirement fixture = new Requirement();
        fixture.name = UUID.randomUUID().toString();
        fixture.priority = Priority.MED_LOW;
        fixture.clusterId = 1;
        return fixture;
    }

    @Override
    protected String getPath() {
        return "requirement";
    }

    @Override
    protected Class<Requirement> getFixtureClass() {
        return Requirement.class;
    }

    @Override
    protected void modifyFixture(Requirement fixture) {
        fixture.name = UUID.randomUUID().toString();
    }

    @Override
    protected void compareDtos(Requirement fixture, Requirement created) {
        assertThat(created.name).isEqualTo(fixture.name);
    }

}
