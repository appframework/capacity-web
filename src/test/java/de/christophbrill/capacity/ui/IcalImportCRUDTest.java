package de.christophbrill.capacity.ui;

import de.christophbrill.appframework.ui.dto.ImportResult;
import de.christophbrill.appframeworktesting.ui.AbstractCRUDTest;
import de.christophbrill.appframework.ui.dto.Progress;
import de.christophbrill.capacity.ui.dto.IcalImport;
import de.christophbrill.capacity.ui.dto.IcalImport.Auth;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.restassured.http.ContentType;
import org.apache.commons.io.IOUtils;
import org.awaitility.Awaitility;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.time.Duration;
import java.util.UUID;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@QuarkusTest
@TestSecurity(user = "tom", roles = {"ADMIN_ICAL_IMPORTS", "SHOW_ICAL_IMPORTS"})
public class IcalImportCRUDTest extends AbstractCRUDTest<IcalImport> {
	
	@Override
	protected IcalImport createFixture() {
		IcalImport fixture = new IcalImport();
		fixture.type = IcalImport.Type.HOLIDAYS;
		fixture.name = UUID.randomUUID().toString();
		fixture.url = UUID.randomUUID().toString();
		fixture.auth = Auth.NONE;
		return fixture;
	}

	@Override
	protected String getPath() {
		return "ical_import";
	}

	@Override
	protected Class<IcalImport> getFixtureClass() {
		return IcalImport.class;
	}

	@Override
	protected void modifyFixture(IcalImport fixture) {
		fixture.url = UUID.randomUUID().toString();
	}

	@Override
	protected void compareDtos(IcalImport fixture, IcalImport created) {
		assertThat(created.name).isEqualTo(fixture.name);
		assertThat(created.url).isEqualTo(fixture.url);
	}

	private static final class Holder<T> {
		public T value;
	}

	@Test
	@Disabled("NPE due to lacking injection when invoking an async process via Uni (runImport2)")
	public void testUpload() throws IOException {
		// Given: Our file we want to import
		String filename = "sabre.ics";

		// Given: We build a ICAL import without an URL (so we can call the upload method)
		IcalImport fixture = createFixture();
		fixture.url = null;
		fixture = given()
				.contentType(ContentType.JSON)
				.when()
				.body(fixture)
				.post("/rest/" + getPath())
				.as(getFixtureClass());

		// When: We upload the file
		InputStream file = this.getClass().getResourceAsStream("/ical/" + filename);
		assertThat(file).isNotNull();
		String s = given()
				.multiPart("file", filename, IOUtils.toByteArray(file))
				.when()
				.post("/rest/ical_import/upload/" + fixture.id)
				.asString();
		try {
			// Then: We get a UUID in return
			UUID.fromString(s);
		} catch (IllegalArgumentException e) {
			throw new AssertionError(e);
		}

		// When: We wait five seconds to complete the asynchronous import
		Holder<Progress<ImportResult>> progress = new Holder<>();
		Awaitility.with().pollInterval(Duration.ofSeconds(1)).await().atMost(Duration.ofSeconds(5)).until(() -> {
			progress.value = given().when().get("/rest/progress/" + s).as(Progress.class);
			return progress.value.completed;
		});

		// Then: We have successfully imported the file
		assertThat(progress.value).isNotNull();
		assertThat(progress.value.completed).isTrue();
		assertThat(progress.value.success).isTrue();
		assertThat(progress.value.result.skipped).isEqualTo(1);
	}

}
