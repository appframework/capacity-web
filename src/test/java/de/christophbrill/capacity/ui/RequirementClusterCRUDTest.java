package de.christophbrill.capacity.ui;

import de.christophbrill.appframeworktesting.ui.AbstractCRUDTest;
import de.christophbrill.capacity.ui.dto.RequirementCluster;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
@TestSecurity(user = "tom", roles = {"ADMIN_REQUIREMENTCLUSTERS", "SHOW_REQUIREMENTCLUSTERS"})
public class RequirementClusterCRUDTest extends AbstractCRUDTest<RequirementCluster> {

    @Override
    protected RequirementCluster createFixture() {
        RequirementCluster fixture = new RequirementCluster();
        fixture.name = UUID.randomUUID().toString();
        return fixture;
    }

    @Override
    protected String getPath() {
        return "requirementcluster";
    }

    @Override
    protected Class<RequirementCluster> getFixtureClass() {
        return RequirementCluster.class;
    }

    @Override
    protected void modifyFixture(RequirementCluster fixture) {
        fixture.name = UUID.randomUUID().toString();
    }

    @Override
    protected void compareDtos(RequirementCluster fixture, RequirementCluster created) {
        assertThat(created.name).isEqualTo(fixture.name);
    }

}
