package de.christophbrill.capacity.ui;

import de.christophbrill.appframeworktesting.ui.AbstractCRUDTest;
import de.christophbrill.capacity.ui.dto.ReferencePool;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
@TestSecurity(user = "tom", roles = {"ADMIN_REFERENCEPOOLS", "SHOW_REFERENCEPOOLS"})
public class ReferencePoolCRUDTest extends AbstractCRUDTest<ReferencePool> {

    @Override
    protected ReferencePool createFixture() {
        ReferencePool fixture = new ReferencePool();
        fixture.name = UUID.randomUUID().toString();
        fixture.templateUrl = UUID.randomUUID().toString();
        return fixture;
    }

    @Override
    protected String getPath() {
        return "referencepool";
    }

    @Override
    protected Class<ReferencePool> getFixtureClass() {
        return ReferencePool.class;
    }

    @Override
    protected void modifyFixture(ReferencePool fixture) {
        fixture.name = UUID.randomUUID().toString();
    }

    @Override
    protected void compareDtos(ReferencePool fixture, ReferencePool created) {
        assertThat(created.name).isEqualTo(fixture.name);
    }

}
