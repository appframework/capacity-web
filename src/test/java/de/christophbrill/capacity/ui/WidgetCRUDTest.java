package de.christophbrill.capacity.ui;

import de.christophbrill.appframeworktesting.ui.AbstractCRUDTest;
import de.christophbrill.capacity.ui.dto.Widget;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
@TestSecurity(user = "tom", roles = {"ADMIN_WIDGETS", "SHOW_WIDGETS"})
public class WidgetCRUDTest extends AbstractCRUDTest<Widget> {

	@Override
	protected Widget createFixture() {
		Widget widget = new Widget();
		widget.name = UUID.randomUUID().toString();
		widget.type = "dummy";
		widget.configuration = UUID.randomUUID().toString();
		return widget;
	}

	@Override
	protected String getPath() {
		return "widget";
	}

	@Override
	protected Class<Widget> getFixtureClass() {
		return Widget.class;
	}

	@Override
	protected void compareDtos(Widget fixture, Widget created) {
		assertThat(created.name).isEqualTo(fixture.name);
		assertThat(created.configuration).isEqualTo(fixture.configuration);
		assertThat(created.type).isEqualTo(fixture.type);
	}

	@Override
	protected void modifyFixture(Widget fixture) {
		fixture.configuration = UUID.randomUUID().toString();
	}

}
