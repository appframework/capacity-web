package de.christophbrill.capacity.ui;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import de.christophbrill.capacity.ui.dto.Employee;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
@TestSecurity(user = "tom", roles = {"ADMIN_ABILITIES", "SHOW_ABILITIES"})
public class AvailabilityServiceTest {

	@Test
	public void checkAvailabilitiesTenthOfFebruary() throws IOException {
		String availabilities = given().queryParam("date", "2015-02-10").when().get("/rest/available").asString();

		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());

		List<Employee> employees = mapper.readValue(availabilities, new TypeReference<>() {
		});

		assertThat(employees)
				.overridingErrorMessage("6 employees expected: ID 1 has an absence, all others have working day, no absence and a valid contract")
				.hasSize(6);

		assertThat(employees)
				.extracting((e) -> e.id)
				.containsExactlyInAnyOrder(2L, 15L, 13L, 5L, 3L, 7L);
	}

	@Test
	public void checkAvailabilitiesThirdOfMarch() throws IOException {
		String availabilities = given().queryParam("date", "2015-03-03").when().get("/rest/available").asString();

		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());

		List<Employee> employees = mapper.readValue(availabilities, new TypeReference<>() {
		});

		assertThat(employees)
				.overridingErrorMessage("Seven employees expected: all have working day, no absence and a valid contract")
				.hasSize(7);

		assertThat(employees)
				.extracting((e) -> e.id)
				.containsExactlyInAnyOrder(1L, 2L, 15L, 13L, 5L, 3L, 7L);
	}

	@Test
	public void checkAvailabilitiesToday() throws IOException {
		String availabilities = given().when().get("/rest/available").asString();

		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());

		List<Employee> employees = mapper.readValue(availabilities, new TypeReference<>() {
		});

		if (DayOfWeek.TUESDAY == LocalDate.now().getDayOfWeek()) {
			assertThat(employees)
					.overridingErrorMessage("One employee expected: ID 2 has a working day and a valid contract")
					.hasSize(1)
					.extracting((e) -> e.id)
					.contains(2L);
		} else {
			assertThat(employees)
					.overridingErrorMessage("No employees expected: noone has a working day")
					.isEmpty();
		}
	}
}
