package de.christophbrill.capacity.util.importer;

import de.christophbrill.appframework.ui.dto.Credentials;
import de.christophbrill.appframework.ui.dto.ImportResult;
import de.christophbrill.appframework.ui.dto.Progress;
import de.christophbrill.capacity.persistence.model.AbsenceEntity;
import de.christophbrill.capacity.persistence.model.IcalImportEntity;
import io.quarkus.security.runtime.QuarkusPrincipal;
import io.quarkus.security.runtime.QuarkusSecurityIdentity;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestAuthController;
import io.quarkus.test.security.TestIdentityAssociation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import jakarta.enterprise.inject.spi.CDI;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.transaction.HeuristicMixedException;
import jakarta.transaction.HeuristicRollbackException;
import jakarta.transaction.NotSupportedException;
import jakarta.transaction.RollbackException;
import jakarta.transaction.SystemException;
import jakarta.transaction.Transactional;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
@Disabled("ARJUNA016108: Wrong transaction on thread")
public class IcalImporterTest {

	@BeforeEach
	public void before() {
		QuarkusSecurityIdentity user = QuarkusSecurityIdentity.builder()
				.setPrincipal(new QuarkusPrincipal("tom"))
				.build();
		CDI.current().select(TestAuthController.class).get().setEnabled(true);
		CDI.current().select(TestIdentityAssociation.class).get().setTestIdentity(user);
	}

	@Inject
    EntityManager entityManager;
	@Inject
	IcalImporter icalImporter;

	@Test
	public void testSabre() throws IOException, HeuristicRollbackException, SystemException, HeuristicMixedException, RollbackException, NotSupportedException, URISyntaxException {
		IcalImportEntity icalImport = new IcalImportEntity();
		icalImport.url = this.getClass().getResource("/ical/sabre.ics").toString();
		Progress<ImportResult> progress = new Progress<>();
		icalImporter.importAbsencesFromIcal(icalImport, icalImporter.loadCalendar(progress, icalImport, null), progress);
		assertThat(progress.completed).isTrue();
		assertThat(progress.result.created).isEqualTo(0);
		assertThat(progress.result.deleted).isEqualTo(0);
		assertThat(progress.result.updated).isEqualTo(0);
		assertThat(progress.result.skipped).isEqualTo(1);
	}

	@Test
	public void testWikipedia() throws IOException, HeuristicRollbackException, SystemException, HeuristicMixedException, RollbackException, NotSupportedException, URISyntaxException {
		IcalImportEntity icalImport = new IcalImportEntity();
		icalImport.url = this.getClass().getResource("/ical/wikipedia.ics").toString();
		Progress<ImportResult> progress = new Progress<>();
		icalImporter.importAbsencesFromIcal(icalImport, icalImporter.loadCalendar(progress, icalImport, null), progress);
		assertThat(progress.completed).isTrue();
		assertThat(progress.result.created).isEqualTo(0);
		assertThat(progress.result.deleted).isEqualTo(0);
		assertThat(progress.result.updated).isEqualTo(0);
		assertThat(progress.result.skipped).isEqualTo(1);
	}

	@Test
	public void testNoAttendee() throws IOException, HeuristicRollbackException, SystemException, HeuristicMixedException, RollbackException, NotSupportedException, URISyntaxException {
		IcalImportEntity icalImport = new IcalImportEntity();
		icalImport.url = this.getClass().getResource("/ical/atendees/no_attendee.ics").toString();
		icalImport.id = 1L;
		Progress<ImportResult> progress = new Progress<>();
		icalImporter.importAbsencesFromIcal(icalImport, icalImporter.loadCalendar(progress, icalImport, null), progress);
		assertThat(progress.completed).isTrue();
		assertThat(progress.result.created).isEqualTo(0);
		assertThat(progress.result.deleted).isEqualTo(0);
		assertThat(progress.result.updated).isEqualTo(0);
		assertThat(progress.result.skipped).isEqualTo(1);
	}

	@Test
	public void testNoUUID() throws IOException, HeuristicRollbackException, SystemException, HeuristicMixedException, RollbackException, NotSupportedException, URISyntaxException {
		IcalImportEntity icalImport = new IcalImportEntity();
		icalImport.url = this.getClass().getResource("/ical/no_uuid.ics").toString();
		icalImport.id = 1L;
		Progress<ImportResult> progress = new Progress<>();
		icalImporter.importAbsencesFromIcal(icalImport, icalImporter.loadCalendar(progress, icalImport, null), progress);
		assertThat(progress.completed).isTrue();
		assertThat(progress.result.created).isEqualTo(0);
		assertThat(progress.result.deleted).isEqualTo(0);
		assertThat(progress.result.updated).isEqualTo(0);
		assertThat(progress.result.skipped).isEqualTo(1);
	}

	@Test
	public void testEmptyUUID() throws IOException, HeuristicRollbackException, SystemException, HeuristicMixedException, RollbackException, NotSupportedException, URISyntaxException {
		IcalImportEntity icalImport = new IcalImportEntity();
		icalImport.url = this.getClass().getResource("/ical/empty_uuid.ics").toString();
		icalImport.id = 1L;
		Progress<ImportResult> progress = new Progress<>();
		icalImporter.importAbsencesFromIcal(icalImport, icalImporter.loadCalendar(progress, icalImport, null), progress);
		assertThat(progress.completed).isTrue();
		assertThat(progress.result.created).isEqualTo(0);
		assertThat(progress.result.deleted).isEqualTo(0);
		assertThat(progress.result.updated).isEqualTo(0);
		assertThat(progress.result.skipped).isEqualTo(1);
	}

	@Test
	public void testWhitespaceUUID() throws IOException, HeuristicRollbackException, SystemException, HeuristicMixedException, RollbackException, NotSupportedException, URISyntaxException {
		IcalImportEntity icalImport = new IcalImportEntity();
		icalImport.url = this.getClass().getResource("/ical/whitespace_uuid.ics").toString();
		icalImport.id = 1L;
		Progress<ImportResult> progress = new Progress<>();
		icalImporter.importAbsencesFromIcal(icalImport, icalImporter.loadCalendar(progress, icalImport, null), progress);
		assertThat(progress.completed).isTrue();
		assertThat(progress.result.created).isEqualTo(0);
		assertThat(progress.result.deleted).isEqualTo(0);
		assertThat(progress.result.updated).isEqualTo(0);
		assertThat(progress.result.skipped).isEqualTo(1);
	}

	@Transactional
	IcalImportEntity createEntity(String resource) {
		IcalImportEntity icalImport = new IcalImportEntity();
		icalImport.name = UUID.randomUUID().toString();
		icalImport.url = this.getClass().getResource(resource).toString();
		icalImport.persist();
		return icalImport;
	}

    @Transactional
    IcalImportEntity updateEntity(String resource, IcalImportEntity icalImport) {
        icalImport.url = this.getClass().getResource(resource).toString();
        icalImport.persist();
        return icalImport;
    }

	@Test
	@Transactional
	public void testOneAttendee() throws IOException, HeuristicRollbackException, SystemException, HeuristicMixedException, RollbackException, NotSupportedException, URISyntaxException {
		IcalImportEntity icalImport = createEntity("/ical/atendees/one_attendee.ics");

		long sizeBefore = AbsenceEntity.count();

		// When: We import a calendar for a single attendee (2015@complete)
		Progress<ImportResult> progress = new Progress<>();
		icalImporter.importAbsencesFromIcal(icalImport, icalImporter.loadCalendar(progress, icalImport, null), progress);
		// Then: We get a creation
		assertThat(progress.completed).isTrue();
		assertThat(progress.result.created).isEqualTo(1);
		assertThat(progress.result.deleted).isEqualTo(0);
		assertThat(progress.result.updated).isEqualTo(0);
		assertThat(progress.result.skipped).isEqualTo(0);
		// Then: The employee with the ID one has an absence on 2010-01-01 with externalId one_attendee-unittest@capacity-web
		assertThat(AbsenceEntity.count()).isEqualTo(sizeBefore + 1);

		// When: We import the same calendar again
		progress = new Progress<>();
		icalImporter.importAbsencesFromIcal(icalImport, icalImporter.loadCalendar(progress, icalImport, null), progress);
		// Then: We get an update
		assertThat(progress.completed).isTrue();
		assertThat(progress.result.created).isEqualTo(0);
		assertThat(progress.result.deleted).isEqualTo(0);
		assertThat(progress.result.updated).isEqualTo(1);
		assertThat(progress.result.skipped).isEqualTo(0);
        assertThat(AbsenceEntity.count()).isEqualTo(sizeBefore + 1);

		// When: We import a calendar for a two attendees (2015@complete and
		// always)
		icalImport = updateEntity("/ical/atendees/one_attendee_appended.ics", icalImport);
		progress = new Progress<>();
		// Then: We get a creation and an update
		icalImporter.importAbsencesFromIcal(icalImport, icalImporter.loadCalendar(progress, icalImport, null), progress);
		assertThat(progress.completed).isTrue();
		assertThat(progress.result.created).isEqualTo(1);
		assertThat(progress.result.deleted).isEqualTo(0);
		assertThat(progress.result.updated).isEqualTo(1);
		assertThat(progress.result.skipped).isEqualTo(0);
        assertThat(AbsenceEntity.count()).isEqualTo(sizeBefore + 2);

		removeAbsences();
	}

	@Transactional
	void removeAbsences() {
		entityManager.createQuery("delete from Absence where icalImport.id = 1").executeUpdate();
	}

	@Test
	public void testTwoAttendee() throws IOException, HeuristicRollbackException, SystemException, HeuristicMixedException, RollbackException, NotSupportedException, URISyntaxException {
		IcalImportEntity icalImport = createEntity("/ical/atendees/two_attendee.ics");

		Progress<ImportResult> progress = new Progress<>();
		icalImporter.importAbsencesFromIcal(icalImport, icalImporter.loadCalendar(progress, icalImport, null), progress);
		assertThat(progress.completed).isTrue();
		assertThat(progress.result.created).isEqualTo(2);
		assertThat(progress.result.deleted).isEqualTo(0);
		assertThat(progress.result.updated).isEqualTo(0);
		assertThat(progress.result.skipped).isEqualTo(0);

		removeAbsences();
	}

	@Test
	public void testDateWithTime() throws IOException, HeuristicRollbackException, SystemException, HeuristicMixedException, RollbackException, NotSupportedException, URISyntaxException {
		IcalImportEntity icalImport = new IcalImportEntity();
		icalImport.url = this.getClass().getResource("/ical/date_with_time.ics").toString();
		icalImport.id = 1L;
		Progress<ImportResult> progress = new Progress<>();
		icalImporter.importAbsencesFromIcal(icalImport, icalImporter.loadCalendar(progress, icalImport, null), progress);
		assertThat(progress.completed).isTrue();
		assertThat(progress.result.created).isEqualTo(0);
		assertThat(progress.result.deleted).isEqualTo(0);
		assertThat(progress.result.updated).isEqualTo(0);
		assertThat(progress.result.skipped).isEqualTo(1); // TODO this should be handled
	}

	@Test
	public void testCreateBasicAuth() {
		IcalImportEntity empty = new IcalImportEntity();

		IcalImportEntity useronly = new IcalImportEntity();
		useronly.username = "test";

		IcalImportEntity passwordonly = new IcalImportEntity();
		passwordonly.password = "test";

		IcalImportEntity usernameAndPassword = new IcalImportEntity();
		usernameAndPassword.username = "test";
		usernameAndPassword.password = "test";

		Credentials cempty = new Credentials();

		Credentials cuseronly = new Credentials();
		cuseronly.username = "test";

		Credentials cpasswordonly = new Credentials();
		cpasswordonly.password = "test";

		Credentials cusernameAndPassword = new Credentials();
		cusernameAndPassword.username = "test";
		cusernameAndPassword.password = "test";

		assertThat(IcalImporter.createBasicAuth(empty, null)).isEqualTo("bnVsbDpudWxs");
		assertThat(IcalImporter.createBasicAuth(useronly, null)).isEqualTo("dGVzdDpudWxs");
		assertThat(IcalImporter.createBasicAuth(passwordonly, null)).isEqualTo("bnVsbDp0ZXN0");
		assertThat(IcalImporter.createBasicAuth(usernameAndPassword, null)).isEqualTo("dGVzdDp0ZXN0");

		assertThat(IcalImporter.createBasicAuth(empty, cempty)).isEqualTo("bnVsbDpudWxs");
		assertThat(IcalImporter.createBasicAuth(empty, cuseronly)).isEqualTo("dGVzdDpudWxs");
		assertThat(IcalImporter.createBasicAuth(empty, cpasswordonly)).isEqualTo("bnVsbDp0ZXN0");
		assertThat(IcalImporter.createBasicAuth(empty, cusernameAndPassword)).isEqualTo("dGVzdDp0ZXN0");
	}
}
