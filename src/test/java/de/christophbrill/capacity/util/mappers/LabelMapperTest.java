package de.christophbrill.capacity.util.mappers;

import de.christophbrill.capacity.persistence.model.LabelEntity;
import de.christophbrill.capacity.ui.dto.Label;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
public class LabelMapperTest {

    @Test
    public void testNullColorMapping() {
        var dto = new Label();
        dto.id = 1L;

        LabelEntity entity = LabelMapper.INSTANCE.mapDtoToEntity(dto);

        assertThat(entity.id).isEqualTo(1L);
        assertThat(entity.color).isEqualTo("112233");
    }

}
