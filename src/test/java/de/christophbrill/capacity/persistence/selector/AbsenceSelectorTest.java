package de.christophbrill.capacity.persistence.selector;

import de.christophbrill.appframeworktesting.persistence.selector.AbstractResourceSelectorTest;
import de.christophbrill.capacity.persistence.model.AbsenceEntity;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
public class AbsenceSelectorTest extends AbstractResourceSelectorTest<AbsenceEntity> {

	@Override
	protected AbsenceSelector getSelector() {
		return new AbsenceSelector(em);
	}

	@Test
	public void testPartialContractRanges() {
		LocalDate beforeRange = LocalDate.parse("2015-01-01");
		LocalDate afterRange = LocalDate.parse("2015-10-31");

		int all = getSelector().count().intValue();

		assertThat(getSelector().withStartInclusive(null).withEndInclusive(afterRange).findAll()).hasSize(all);

		assertThat(getSelector().withStartInclusive(beforeRange).withEndInclusive(null).findAll()).hasSize(all);

		assertThat(getSelector().withStartInclusive(null).withEndInclusive(null).findAll()).hasSize(all);
	}

	@Test
	public void smoketest() {
		LocalDate wayBeforeRange = LocalDate.parse("2014-01-01");
		LocalDate beforeRange = LocalDate.parse("2015-01-01");
		LocalDate inRange = LocalDate.parse("2015-02-14");
		LocalDate afterRange = LocalDate.parse("2015-10-31");
		LocalDate wayAfterRange = LocalDate.parse("2016-10-31");

		assertThat(getSelector().withEmployeeId(1).withStartInclusive(wayBeforeRange)
				.withEndInclusive(beforeRange).findAll()).isEmpty();

		assertThat(getSelector().withEmployeeId(1).withStartInclusive(beforeRange).withEndInclusive(inRange)
				.findAll()).hasSize(1);

		assertThat(getSelector().withEmployeeId(1).withStartInclusive(inRange).withEndInclusive(afterRange)
				.findAll()).hasSize(1);

		assertThat(getSelector().withEmployeeId(1).withStartInclusive(afterRange)
				.withEndInclusive(wayAfterRange).findAll()).isEmpty();
	}

}
