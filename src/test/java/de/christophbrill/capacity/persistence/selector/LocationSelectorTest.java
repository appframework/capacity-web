package de.christophbrill.capacity.persistence.selector;

import de.christophbrill.appframeworktesting.persistence.selector.AbstractResourceSelectorTest;
import de.christophbrill.capacity.persistence.model.LocationEntity;
import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class LocationSelectorTest extends AbstractResourceSelectorTest<LocationEntity> {

	@Override
	protected LocationSelector getSelector() {
		return new LocationSelector(em);
	}

}
