package de.christophbrill.capacity.persistence.selector;

import de.christophbrill.appframeworktesting.persistence.selector.AbstractResourceSelectorTest;
import de.christophbrill.capacity.persistence.model.EmployeeEntity;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
public class EmployeeSelectorTest extends AbstractResourceSelectorTest<EmployeeEntity> {

	@Override
	protected EmployeeSelector getSelector() {
		return new EmployeeSelector(em);
	}

	@Test
	public void testPartialContractRanges() {
		LocalDate contractRangeStartDate = LocalDate.parse("2015-02-10");
		LocalDate contractRangeEndDate = LocalDate.parse("2015-02-10");

		int all = getSelector().count().intValue();

		assertThat(getSelector().withActiveContract(null, contractRangeEndDate).findAll()).hasSize(all);

		assertThat(getSelector().withActiveContract(contractRangeStartDate, null).findAll()).hasSize(all);

		assertThat(getSelector().withActiveContract(null, null).findAll()).hasSize(all);
	}

	@Test
	public void testContractRange_workingOnTenthOfFebruary() {
		LocalDate contractRangeStartDate = LocalDate.parse("2015-02-10");
		LocalDate contractRangeEndDate = LocalDate.parse("2015-02-10");
		List<EmployeeEntity> employees = getSelector()
				.withActiveContract(contractRangeStartDate, contractRangeEndDate).findAll();

		assertThat(employees).hasSizeGreaterThanOrEqualTo(8);

		assertThat(employees)
				.extracting((e) -> e.id)
				.containsExactlyInAnyOrder(1L, 2L, 3L, 5L, 7L, 13L, 15L, 17L);
	}

	@Test
	public void testContractRange_workingInOctober() {
		LocalDate contractRangeStartDate = LocalDate.parse("2015-10-01");
		LocalDate contractRangeEndDate = LocalDate.parse("2015-10-31");
		List<EmployeeEntity> employees = getSelector()
				.withActiveContract(contractRangeStartDate, contractRangeEndDate).findAll();

		assertThat(employees).hasSizeGreaterThanOrEqualTo(12);

		assertThat(employees)
				.extracting((e) -> e.id)
				.containsExactlyInAnyOrder(1L, 2L, 4L, 5L, 7L, 8L, 9L, 10L, 14L, 15L, 17L, 18L);
	}

	@Test
	public void testContractRange_workingInSeptember() {
		LocalDate contractRangeStartDate = LocalDate.parse("2015-09-01");
		LocalDate contractRangeEndDate = LocalDate.parse("2015-09-30");
		List<EmployeeEntity> employees = getSelector()
				.withActiveContract(contractRangeStartDate, contractRangeEndDate).findAll();

		assertThat(employees).hasSizeGreaterThanOrEqualTo(8);

		assertThat(employees)
				.extracting((e) -> e.id)
				.containsExactlyInAnyOrder(1L, 2L, 3L, 5L, 7L, 13L, 15L, 17L);
	}

	@Test
	public void testContractRange_workingInNovember() {
		LocalDate contractRangeStartDate = LocalDate.parse("2015-11-01");
		LocalDate contractRangeEndDate = LocalDate.parse("2015-11-30");
		List<EmployeeEntity> employees = getSelector()
				.withActiveContract(contractRangeStartDate, contractRangeEndDate).findAll();

		assertThat(employees).hasSizeGreaterThanOrEqualTo(8);

		assertThat(employees)
				.extracting((e) -> e.id)
				.containsExactlyInAnyOrder(1L, 2L, 4L, 6L, 8L, 14L, 16L, 18L);
	}

	@Test
	public void testSorting() {
		List<EmployeeEntity> employees = getSelector().withEmail("from@october").withSortColumn("name", true).findAll();

		assertThat(employees)
				.extracting((e) -> e.name)
				.containsExactlyInAnyOrder("User from start october", "User from start october until dec");

		employees = getSelector().withEmail("from@october").withSortColumn("name", false)
				.findAll();

		assertThat(employees)
				.extracting((e) -> e.name)
				.containsExactlyInAnyOrder("User from start october until dec", "User from start october");
	}

	@Test
	public void testWithAbilities() {
		long max = getSelector().count();

		assertThat(getSelector().withAbilities(null).count()).isEqualTo(max);

		assertThat(getSelector().withAbilities(Collections.singleton("")).count()).isEqualTo(0L);

		assertThat(getSelector().withAbilities(Collections.singleton("Working")).count()).isEqualTo(2L);
		assertThat(getSelector().withAbilities(Collections.singleton("Relaxing")).count()).isEqualTo(2L);

		assertThat(getSelector().withAbilities(Collections.singleton("Work")).count()).isEqualTo(0L);
		assertThat(getSelector().withAbilities(Collections.singleton("Relax")).count()).isEqualTo(0L);

		assertThat(getSelector().withAbilities(Collections.singleton(UUID.randomUUID().toString())).count()).isEqualTo(0L);
	}
}
