package de.christophbrill.capacity.persistence.selector;

import de.christophbrill.appframeworktesting.persistence.selector.AbstractResourceSelectorTest;
import de.christophbrill.capacity.persistence.model.RequirementClusterEntity;
import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class RequirementClusterSelectorTest extends AbstractResourceSelectorTest<RequirementClusterEntity> {

    @Override
    protected RequirementClusterSelector getSelector() {
        return new RequirementClusterSelector(em);
    }

}
