package de.christophbrill.capacity.persistence.selector;

import de.christophbrill.appframeworktesting.persistence.selector.AbstractResourceSelectorTest;
import de.christophbrill.capacity.persistence.model.RequirementEntity;
import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class RequirementSelectorTest extends AbstractResourceSelectorTest<RequirementEntity> {

    @Override
    protected RequirementSelector getSelector() {
        return new RequirementSelector(em);
    }

}
