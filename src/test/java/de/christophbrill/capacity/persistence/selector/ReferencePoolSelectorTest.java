package de.christophbrill.capacity.persistence.selector;

import de.christophbrill.appframeworktesting.persistence.selector.AbstractResourceSelectorTest;
import de.christophbrill.capacity.persistence.model.ReferencePoolEntity;
import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class ReferencePoolSelectorTest extends AbstractResourceSelectorTest<ReferencePoolEntity> {

    @Override
    protected ReferencePoolSelector getSelector() {
        return new ReferencePoolSelector(em);
    }

}
