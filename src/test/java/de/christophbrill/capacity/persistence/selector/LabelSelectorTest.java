package de.christophbrill.capacity.persistence.selector;

import de.christophbrill.appframeworktesting.persistence.selector.AbstractResourceSelectorTest;
import de.christophbrill.capacity.persistence.model.LabelEntity;
import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class LabelSelectorTest extends AbstractResourceSelectorTest<LabelEntity> {

    @Override
    protected LabelSelector getSelector() {
        return new LabelSelector(em);
    }

}
