package de.christophbrill.capacity.persistence.selector;

import de.christophbrill.appframeworktesting.persistence.selector.AbstractResourceSelectorTest;
import de.christophbrill.capacity.persistence.model.EpisodeEntity;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
public class EpisodeSelectorTest extends AbstractResourceSelectorTest<EpisodeEntity> {

	@Override
	protected EpisodeSelector getSelector() {
		return new EpisodeSelector(em);
	}

	@Test
	public void testNameLike() {
		long count = getSelector().count();

		assertThat(getSelector().withNameLike(null).count()).isEqualTo(count);

		assertThat(getSelector().withNameLike("").count()).isEqualTo(count);

		assertThat(getSelector().withNameLike("Darkness").count()).isEqualTo(1L);
		assertThat(getSelector().withNameLike("Khan").count()).isEqualTo(1L);

		assertThat(getSelector().withNameLike(UUID.randomUUID().toString()).count()).isEqualTo(0L);
	}

}
