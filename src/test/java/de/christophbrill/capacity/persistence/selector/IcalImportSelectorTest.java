package de.christophbrill.capacity.persistence.selector;

import de.christophbrill.appframeworktesting.persistence.selector.AbstractResourceSelectorTest;
import de.christophbrill.capacity.persistence.model.IcalImportEntity;
import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class IcalImportSelectorTest extends AbstractResourceSelectorTest<IcalImportEntity> {

	@Override
	protected IcalImportSelector getSelector() {
		return new IcalImportSelector(em);
	}

}
