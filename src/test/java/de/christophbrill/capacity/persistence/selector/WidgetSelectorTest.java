package de.christophbrill.capacity.persistence.selector;

import de.christophbrill.appframeworktesting.persistence.selector.AbstractResourceSelectorTest;
import de.christophbrill.capacity.persistence.model.WidgetEntity;
import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class WidgetSelectorTest extends AbstractResourceSelectorTest<WidgetEntity> {

	@Override
	protected WidgetSelector getSelector() {
		return new WidgetSelector(em);
	}

}
