package de.christophbrill.capacity.persistence.selector;

import de.christophbrill.appframeworktesting.persistence.selector.AbstractResourceSelectorTest;
import de.christophbrill.capacity.persistence.model.HolidayEntity;
import de.christophbrill.capacity.persistence.model.LocationEntity;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
public class HolidaySelectorTest extends AbstractResourceSelectorTest<HolidayEntity> {
	
	@Override
	protected HolidaySelector getSelector() {
		return new HolidaySelector(em);
	}

	@Test
	public void testHolidaysAtLocation1() {
		LocationEntity location = LocationEntity.findById(1L);
		List<HolidayEntity> holidays = getSelector().withOnlyLocation(location).findAll();
		assertThat(holidays)
				.hasSize(2)
				.extracting((h) -> h.name)
				.containsExactlyInAnyOrder("Some vacation 14", "Some vacation 16");
	}

	@Test
	public void testHolidaysAtLocation2() {
		LocationEntity location = LocationEntity.findById(2L);
		List<HolidayEntity> holidays = getSelector().withOnlyLocation(location).findAll();

		assertThat(holidays)
				.hasSize(2)
				.extracting((h) -> h.name)
				.containsExactlyInAnyOrder("Some vacation 15", "Some vacation 16");
	}

	@Test
	public void testHolidaysAtLocation1Including() {
		LocationEntity location = LocationEntity.findById(1L);
		List<HolidayEntity> holidays = getSelector().withIncludingLocation(location).findAll();

		assertThat(holidays)
				.hasSize(3)
				.extracting((h) -> h.name)
				.containsExactlyInAnyOrder("Some vacation 14", "Some vacation 16", "Some vacation 13");
	}

	@Test
	public void testHolidaysAtLocation2Including() {
		LocationEntity location = LocationEntity.findById(2L);
		List<HolidayEntity> holidays = getSelector().withIncludingLocation(location).findAll();

		assertThat(holidays)
				.hasSize(3)
				.extracting((h) -> h.name)
				.containsExactlyInAnyOrder("Some vacation 15", "Some vacation 16", "Some vacation 13");
	}

}
