package de.christophbrill.capacity.persistence.dao;

import de.christophbrill.appframeworktesting.persistence.dao.AbstractDaoCRUDTest;
import de.christophbrill.capacity.persistence.model.LocationEntity;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
public class LocationDaoTest extends AbstractDaoCRUDTest<LocationEntity> {

	@Override
	protected LocationEntity createFixture() {
		LocationEntity location = new LocationEntity();
		location.name = UUID.randomUUID().toString();
		return location;
	}

	@Override
	protected void modifyFixture(LocationEntity location) {
		location.name = UUID.randomUUID().toString();
	}

	@Override
	protected LocationEntity findById(Long id) {
		return LocationEntity.findById(id);
	}

	@Override
	protected Long count() {
		return LocationEntity.count();
	}

	@Test
	public void testGetByIds() {
		List<LocationEntity> locations = LocationEntity.findByIds(Collections.singletonList(1L));
		assertThat(locations).hasSize(1);

		locations = LocationEntity.findByIds(Collections.singletonList(2L));
		assertThat(locations).hasSize(1);

		locations = LocationEntity.findByIds(Collections.singletonList(3L));
		assertThat(locations).isEmpty();

		locations = LocationEntity.findByIds(Arrays.asList(1L, 2L));
		assertThat(locations).hasSize(2);

		locations = LocationEntity.findByIds(Arrays.asList(1L, 3L));
		assertThat(locations).hasSize(1);

		locations = LocationEntity.findByIds(Arrays.asList(3L, 4L));
		assertThat(locations).isEmpty();
	}

}
