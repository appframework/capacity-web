package de.christophbrill.capacity.persistence.dao;

import de.christophbrill.appframeworktesting.persistence.dao.AbstractDaoCRUDTest;
import de.christophbrill.capacity.persistence.model.RequirementClusterEntity;
import de.christophbrill.capacity.persistence.model.RequirementEntity;
import de.christophbrill.capacity.ui.dto.Priority;
import io.quarkus.test.junit.QuarkusTest;

import java.util.UUID;

@QuarkusTest
public class RequirementDaoTest extends AbstractDaoCRUDTest<RequirementEntity> {

	@Override
    protected RequirementEntity createFixture() {
        RequirementEntity requirement = new RequirementEntity();
        requirement.name = UUID.randomUUID().toString();
        requirement.cluster = (RequirementClusterEntity) RequirementClusterEntity.findAll().list().getFirst();
        requirement.priority = Priority.HIGH;
        return requirement;
    }

    @Override
    protected void modifyFixture(RequirementEntity requirement) {
        requirement.name = UUID.randomUUID().toString();
    }

    @Override
    protected RequirementEntity findById(Long id) {
        return RequirementEntity.findById(id);
    }

    @Override
    protected Long count() {
        return RequirementEntity.count();
    }

}
