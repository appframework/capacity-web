package de.christophbrill.capacity.persistence.dao;

import de.christophbrill.appframeworktesting.persistence.dao.AbstractDaoCRUDTest;
import de.christophbrill.capacity.persistence.model.WidgetEntity;
import io.quarkus.test.junit.QuarkusTest;

import java.util.UUID;

@QuarkusTest
public class WidgetDaoTest extends AbstractDaoCRUDTest<WidgetEntity> {

	@Override
	protected WidgetEntity createFixture() {
		WidgetEntity widget = new WidgetEntity();
		widget.name = UUID.randomUUID().toString();
		widget.type = "dummy";
		widget.configuration = UUID.randomUUID().toString();
		return widget;
	}

	@Override
	protected void modifyFixture(WidgetEntity widget) {
		widget.configuration = UUID.randomUUID().toString();
	}

	@Override
	protected WidgetEntity findById(Long id) {
		return WidgetEntity.findById(id);
	}

	@Override
	protected Long count() {
		return WidgetEntity.count();
	}

}
