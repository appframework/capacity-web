package de.christophbrill.capacity.persistence.dao;

import de.christophbrill.appframeworktesting.persistence.dao.AbstractDaoCRUDTest;
import de.christophbrill.capacity.persistence.model.EpisodeEntity;
import io.quarkus.test.junit.QuarkusTest;

import java.time.LocalDate;
import java.util.UUID;

@QuarkusTest
public class EpisodeDaoTest extends AbstractDaoCRUDTest<EpisodeEntity> {
	
	@Override
	protected EpisodeEntity createFixture() {
		EpisodeEntity episode = new EpisodeEntity();
		episode.start = LocalDate.now();
		episode.end = LocalDate.now();
		episode.name = UUID.randomUUID().toString();
		return episode;
	}

	@Override
	protected void modifyFixture(EpisodeEntity episode) {
		episode.name = UUID.randomUUID().toString();
	}

	@Override
	protected EpisodeEntity findById(Long id) {
		return EpisodeEntity.findById(id);
	}

	@Override
	protected Long count() {
		return EpisodeEntity.count();
	}

}
