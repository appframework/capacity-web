package de.christophbrill.capacity.persistence.dao;

import de.christophbrill.appframeworktesting.persistence.dao.AbstractDaoCRUDTest;
import de.christophbrill.capacity.persistence.model.ReferencePoolEntity;
import io.quarkus.test.junit.QuarkusTest;

import java.util.UUID;

@QuarkusTest
public class ReferencePoolDaoTest extends AbstractDaoCRUDTest<ReferencePoolEntity> {

	@Override
    protected ReferencePoolEntity createFixture() {
        ReferencePoolEntity referencepool = new ReferencePoolEntity();
        referencepool.name = UUID.randomUUID().toString();
        referencepool.templateUrl = UUID.randomUUID().toString();
        return referencepool;
    }

    @Override
    protected void modifyFixture(ReferencePoolEntity referencepool) {
        referencepool.name = UUID.randomUUID().toString();
    }

    @Override
    protected ReferencePoolEntity findById(Long id) {
        return ReferencePoolEntity.findById(id);
    }

    @Override
    protected Long count() {
        return ReferencePoolEntity.count();
    }


}
