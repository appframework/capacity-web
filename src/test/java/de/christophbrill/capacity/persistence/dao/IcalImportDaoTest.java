package de.christophbrill.capacity.persistence.dao;

import de.christophbrill.appframeworktesting.persistence.dao.AbstractDaoCRUDTest;
import de.christophbrill.capacity.persistence.model.IcalImportEntity;
import de.christophbrill.capacity.ui.dto.IcalImport;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;

import java.util.UUID;

@QuarkusTest
@TestSecurity(user = "tom", roles = {"ADMIN_ICAL_IMPORTS", "SHOW_ICAL_IMPORTS"})
public class IcalImportDaoTest extends AbstractDaoCRUDTest<IcalImportEntity> {
	
	@Override
	protected IcalImportEntity createFixture() {
		IcalImportEntity icalImport = new IcalImportEntity();
		icalImport.type = IcalImport.Type.HOLIDAYS;
		icalImport.name = UUID.randomUUID().toString();
		icalImport.url = "file:///dev/null";
		return icalImport;
	}

	@Override
	protected void modifyFixture(IcalImportEntity icalImport) {
		icalImport.name = UUID.randomUUID().toString();
	}

	@Override
	protected IcalImportEntity findById(Long id) {
		return IcalImportEntity.findById(id);
	}

	@Override
	protected Long count() {
		return IcalImportEntity.count();
	}

}
