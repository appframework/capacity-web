package de.christophbrill.capacity.persistence.dao;

import de.christophbrill.appframeworktesting.persistence.dao.AbstractDaoCRUDTest;
import de.christophbrill.capacity.persistence.model.ContractEntity;
import de.christophbrill.capacity.persistence.model.EmployeeEntity;
import io.quarkus.test.junit.QuarkusTest;

import java.util.UUID;

@QuarkusTest
public class EmployeeDaoTest extends AbstractDaoCRUDTest<EmployeeEntity> {

	@Override
	protected EmployeeEntity createFixture() {
		EmployeeEntity employee = new EmployeeEntity();
		employee.email = UUID.randomUUID().toString();
		employee.name = UUID.randomUUID().toString();
		employee.color = "#00ff00";
		employee.contract = new ContractEntity();
		employee.contract.vacationDaysPerYear = 100;
		return employee;
	}

	@Override
	protected void modifyFixture(EmployeeEntity employee) {
		employee.name = UUID.randomUUID().toString();
	}

	@Override
	protected EmployeeEntity findById(Long id) {
		return EmployeeEntity.findById(id);
	}

	@Override
	protected Long count() {
		return EmployeeEntity.count();
	}

}
