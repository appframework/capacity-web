package de.christophbrill.capacity.persistence.dao;

import de.christophbrill.appframeworktesting.persistence.dao.AbstractDaoCRUDTest;
import de.christophbrill.capacity.persistence.model.RequirementClusterEntity;
import io.quarkus.test.junit.QuarkusTest;

import java.util.UUID;

@QuarkusTest
public class RequirementClusterDaoTest extends AbstractDaoCRUDTest<RequirementClusterEntity> {

	@Override
    protected RequirementClusterEntity createFixture() {
        RequirementClusterEntity requirementcluster = new RequirementClusterEntity();
        requirementcluster.name = UUID.randomUUID().toString();
        return requirementcluster;
    }

    @Override
    protected void modifyFixture(RequirementClusterEntity requirementcluster) {
        requirementcluster.name = UUID.randomUUID().toString();
    }

    @Override
    protected RequirementClusterEntity findById(Long id) {
        return RequirementClusterEntity.findById(id);
    }

    @Override
    protected Long count() {
        return RequirementClusterEntity.count();
    }


}
