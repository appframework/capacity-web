package de.christophbrill.capacity.persistence.dao;

import de.christophbrill.appframeworktesting.persistence.dao.AbstractDaoCRUDTest;
import de.christophbrill.capacity.persistence.model.AbsenceEntity;
import de.christophbrill.capacity.persistence.model.EmployeeEntity;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;

import java.time.LocalDate;
import java.util.UUID;

@QuarkusTest
@TestSecurity(user = "tom", roles = { "ADMIN_ABSENCES", "SHOW_ABSENCES" })
public class AbsenceDaoTest extends AbstractDaoCRUDTest<AbsenceEntity> {

	@Override
	protected AbsenceEntity createFixture() {
		AbsenceEntity absence = new AbsenceEntity();
		absence.start = LocalDate.now();
		absence.end = LocalDate.now();
		absence.reason = UUID.randomUUID().toString();
		absence.employee = EmployeeEntity.findById(1L);
		return absence;
	}

	@Override
	protected void modifyFixture(AbsenceEntity absence) {
		absence.reason = UUID.randomUUID().toString();
	}

	@Override
	protected AbsenceEntity findById(Long id) {
		return AbsenceEntity.findById(id);
	}

	@Override
	protected Long count() {
		return AbsenceEntity.count();
	}

}
