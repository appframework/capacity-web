package de.christophbrill.capacity.persistence.dao;

import de.christophbrill.appframeworktesting.persistence.dao.AbstractDaoCRUDTest;
import de.christophbrill.capacity.persistence.model.LabelEntity;
import io.quarkus.test.junit.QuarkusTest;

import java.util.UUID;

@QuarkusTest
public class LabelDaoTest extends AbstractDaoCRUDTest<LabelEntity> {
	
    @Override
    protected LabelEntity createFixture() {
        LabelEntity label = new LabelEntity();
        label.name = UUID.randomUUID().toString();
        label.color = "#112233";
        return label;
    }

    @Override
    protected void modifyFixture(LabelEntity label) {
        label.name = UUID.randomUUID().toString();
    }

    @Override
    protected LabelEntity findById(Long id) {
        return LabelEntity.findById(id);
    }

    @Override
    protected Long count() {
        return LabelEntity.count();
    }

}
