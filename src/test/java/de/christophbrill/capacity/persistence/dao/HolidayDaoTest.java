package de.christophbrill.capacity.persistence.dao;

import de.christophbrill.appframeworktesting.persistence.dao.AbstractDaoCRUDTest;
import de.christophbrill.capacity.persistence.model.HolidayEntity;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;

import java.time.LocalDate;
import java.util.UUID;

@QuarkusTest
@TestSecurity(user = "tom", roles = {"ADMIN_HOLIDAYS", "SHOW_HOLIDAYS"})
public class HolidayDaoTest extends AbstractDaoCRUDTest<HolidayEntity> {
	
	@Override
	protected HolidayEntity createFixture() {
		HolidayEntity holiday = new HolidayEntity();
		holiday.date = LocalDate.now();
		modifyFixture(holiday);
		return holiday;
	}

	@Override
	protected void modifyFixture(HolidayEntity created) {
		created.name = UUID.randomUUID().toString();
	}

	@Override
	protected HolidayEntity findById(Long id) {
		return HolidayEntity.findById(id);
	}

	@Override
	protected Long count() {
		return HolidayEntity.count();
	}

}
