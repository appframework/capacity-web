## [1.0.50](https://gitlab.com/appframework/capacity-web/compare/1.0.49...1.0.50) (2025-03-14)


### Bug Fixes

* **deps:** update dependency @vueuse/core to v13 ([1ce3b9f](https://gitlab.com/appframework/capacity-web/commit/1ce3b9fe44f1df029d7e39722d75385c71016470))

## [1.0.49](https://gitlab.com/appframework/capacity-web/compare/1.0.48...1.0.49) (2025-03-14)


### Bug Fixes

* **deps:** update dependency primelocale to v2 ([ec09cd5](https://gitlab.com/appframework/capacity-web/commit/ec09cd5084ca70c29c19a876b47edcbbd0fef7e3))
* **deps:** update dependency primelocale to v2.1.1 ([cc360ab](https://gitlab.com/appframework/capacity-web/commit/cc360abc7fa947950e44f666395594e5b9e3579f))
* **deps:** update primevue and friends to v4.3.2 ([f7c8116](https://gitlab.com/appframework/capacity-web/commit/f7c81168ee54034942c926c2b8b61baeebadfd67))
* **deps:** update quarkus.platform.version to v3.19.2 ([aa22baf](https://gitlab.com/appframework/capacity-web/commit/aa22baf9e2d45c3f3d09a2feb7473b0a2e6789ec))
* **deps:** update quarkus.platform.version to v3.19.3 ([3cc194c](https://gitlab.com/appframework/capacity-web/commit/3cc194cc462cb0ea6b75bd28e57a48e230038b34))
* **deps:** update quinoa.version to v2.5.3 ([d69c035](https://gitlab.com/appframework/capacity-web/commit/d69c035f2c8a908639a8cb8b5e7e394100ab604b))

## [1.0.48](https://gitlab.com/appframework/capacity-web/compare/1.0.47...1.0.48) (2025-03-11)


### Bug Fixes

* **deps:** update dependency @vueuse/core to v12.8.2 ([08b8df0](https://gitlab.com/appframework/capacity-web/commit/08b8df0e3f9037b2aeb9a6b347c9c78470a4a178))
* **deps:** update dependency bootstrap-vue-next to ^0.28.0 ([7310fa2](https://gitlab.com/appframework/capacity-web/commit/7310fa28ce0ca2e605dc38d6d39f59fa72fed66e))
* **deps:** update primevue and friends to v4.3.1 ([6bc7170](https://gitlab.com/appframework/capacity-web/commit/6bc71701846bd5058a2b57babd38db81a075a320))

## [1.0.47](https://gitlab.com/appframework/capacity-web/compare/1.0.46...1.0.47) (2025-03-11)


### Bug Fixes

* **deps:** update dependency chart.js to v4.4.8 ([3482517](https://gitlab.com/appframework/capacity-web/commit/3482517f3520ff4698fb2285b94da6fec601816e))
* **deps:** update dependency vue-i18n to v11.1.2 ([5f922a1](https://gitlab.com/appframework/capacity-web/commit/5f922a1c114af1d7fcd0e280f3fb0c4bc9878d40))

## [1.0.46](https://gitlab.com/appframework/capacity-web/compare/1.0.45...1.0.46) (2025-02-26)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.26.28 ([d9d22e5](https://gitlab.com/appframework/capacity-web/commit/d9d22e5b4b79fac8da1f921fb9673e162dae7726))
* **deps:** update dependency bootstrap-vue-next to v0.26.30 ([7402c4d](https://gitlab.com/appframework/capacity-web/commit/7402c4dda7aac1ab1d2aed544e7fd6aa53312a10))
* **deps:** update quarkus.platform.version to v3.18.3 ([b102748](https://gitlab.com/appframework/capacity-web/commit/b102748a91b7c58aa3b5842104926a09fc43204c))

## [1.0.45](https://gitlab.com/appframework/capacity-web/compare/1.0.44...1.0.45) (2025-02-12)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.26.26 ([4068071](https://gitlab.com/appframework/capacity-web/commit/406807101523c55c89ae27359d987b03ac9b5428))
* **deps:** update dependency primelocale to v1.6.0 ([0511393](https://gitlab.com/appframework/capacity-web/commit/05113930b11cc316a23b06629b9b3851a3a7ffd5))
* **deps:** update dependency vue-i18n to v11.1.1 ([e98154c](https://gitlab.com/appframework/capacity-web/commit/e98154c90a0447a18b43ef0e959b618333c9f1e5))
* **deps:** update quarkus.platform.version to v3.18.2 ([3eb3674](https://gitlab.com/appframework/capacity-web/commit/3eb36743ff6d8f200841d291e5e079bd6aa09a85))

## [1.0.44](https://gitlab.com/appframework/capacity-web/compare/1.0.43...1.0.44) (2025-02-04)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.26.22 ([aeada60](https://gitlab.com/appframework/capacity-web/commit/aeada60dfb41b1b7ff44388b0c295a2faa6f1a92))
* **deps:** update dependency primelocale to v1.5.0 ([8ac1df8](https://gitlab.com/appframework/capacity-web/commit/8ac1df8a93c8fc918f474bab31b5a14e6d3ebbd7))
* **deps:** update dependency vue-i18n to v11.1.0 ([a82c481](https://gitlab.com/appframework/capacity-web/commit/a82c48171e0ebb5e277b42c09020a6bd471d87d0))
* **deps:** update quarkus.platform.version to v3.18.1 ([ca319ed](https://gitlab.com/appframework/capacity-web/commit/ca319ed4ef85b5f3390e5b2f2813ea735f9285fa))

## [1.0.43](https://gitlab.com/appframework/capacity-web/compare/1.0.42...1.0.43) (2025-01-27)


### Bug Fixes

* **deps:** update dependency @vueuse/core to v12.5.0 ([e27d1eb](https://gitlab.com/appframework/capacity-web/commit/e27d1eb9db95c533119779e3520761b1cc0b3403))
* **deps:** update dependency bootstrap-vue-next to v0.26.21 ([25de90d](https://gitlab.com/appframework/capacity-web/commit/25de90d5fb05921f85353d8e2c1fc990036af23b))
* **deps:** update quarkus.platform.version to v3.18.0 ([bfaf4fa](https://gitlab.com/appframework/capacity-web/commit/bfaf4fa23f387a64dbeb1710dafe0521e26dc093))

## [1.0.42](https://gitlab.com/appframework/capacity-web/compare/1.0.41...1.0.42) (2025-01-22)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.26.20 ([7e6e3f0](https://gitlab.com/appframework/capacity-web/commit/7e6e3f0b4552a27af68ecd7c0b8eb519ad9b3da2))
* **deps:** update dependency primelocale to v1.4.0 ([cabbc56](https://gitlab.com/appframework/capacity-web/commit/cabbc561104ea933b9aa94069f2b4d8170c9d1c0))
* **deps:** update quarkus.platform.version to v3.17.7 ([381046f](https://gitlab.com/appframework/capacity-web/commit/381046f7798678b01fe6b1501120d6e0e08ba325))

## [1.0.41](https://gitlab.com/appframework/capacity-web/compare/1.0.40...1.0.41) (2025-01-15)

## [1.0.40](https://gitlab.com/appframework/capacity-web/compare/1.0.39...1.0.40) (2025-01-15)


### Bug Fixes

* **deps:** update dependency @vueuse/core to v12.4.0 ([10c2865](https://gitlab.com/appframework/capacity-web/commit/10c2865e96a9c9650072e04fb98b99988143cd52))
* **deps:** update dependency org.apache.poi:poi-ooxml to v5.4.0 ([6f91c0c](https://gitlab.com/appframework/capacity-web/commit/6f91c0c25fa346f08e05698d11c6ab3efd424ad3))
* **deps:** update dependency primelocale to v1.3.0 ([962ae01](https://gitlab.com/appframework/capacity-web/commit/962ae0158f5780a67206a07e8798761df58794bb))
* **deps:** update quarkus.platform.version to v3.17.6 ([210b344](https://gitlab.com/appframework/capacity-web/commit/210b3445ee67820d253fd80cdff81407fb1c1f38))

## [1.0.39](https://gitlab.com/appframework/capacity-web/compare/1.0.38...1.0.39) (2025-01-09)


### Bug Fixes

* **deps:** update dependency vue-i18n to v11 ([0922e2b](https://gitlab.com/appframework/capacity-web/commit/0922e2b62f33ab93be1f59f9c0450accb2cda91a))

## [1.0.38](https://gitlab.com/appframework/capacity-web/compare/1.0.37...1.0.38) (2025-01-08)


### Bug Fixes

* **deps:** update dependency @vueuse/core to v12.3.0 ([af81514](https://gitlab.com/appframework/capacity-web/commit/af81514912bfd6a45fb8929c53d02144672bff65))
* **deps:** update dependency bootstrap-vue-next to v0.26.19 ([c3a8cf9](https://gitlab.com/appframework/capacity-web/commit/c3a8cf9748f84c52be5401a8bc6efefb032e5a20))
* **deps:** update dependency primelocale to v1.2.3 ([2e8df9d](https://gitlab.com/appframework/capacity-web/commit/2e8df9db6c696a6f726ed5f5093d0379b557a25b))

## [1.0.37](https://gitlab.com/appframework/capacity-web/compare/1.0.36...1.0.37) (2024-12-31)


### Bug Fixes

* **deps:** update dependency @vueuse/core to v12.2.0 ([8168094](https://gitlab.com/appframework/capacity-web/commit/816809423422af65b29dba8b488c063280735117))

## [1.0.36](https://gitlab.com/appframework/capacity-web/compare/1.0.35...1.0.36) (2024-12-25)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.26.18 ([d90d284](https://gitlab.com/appframework/capacity-web/commit/d90d2841e9b423f2eda0ca9f64af3129cd05ff64))
* **deps:** update quarkus.platform.version to v3.17.5 ([3d4000e](https://gitlab.com/appframework/capacity-web/commit/3d4000e8f2f135308fc62377513c095a834297d4))

## [1.0.35](https://gitlab.com/appframework/capacity-web/compare/1.0.34...1.0.35) (2024-12-18)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.26.15 ([9cd4175](https://gitlab.com/appframework/capacity-web/commit/9cd417512a04aaea1c353cb80b27cb7ac6d7da13))
* **deps:** update dependency org.apache.logging.log4j:log4j-to-slf4j to v2.24.3 ([d7f231a](https://gitlab.com/appframework/capacity-web/commit/d7f231a64c11e62f6af7474a0e5d095ee01ea690))

## [1.0.34](https://gitlab.com/appframework/capacity-web/compare/1.0.33...1.0.34) (2024-12-13)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.26.14 ([74e77b1](https://gitlab.com/appframework/capacity-web/commit/74e77b1db386f69fb9218dec73e8a767fa30e309))

## [1.0.33](https://gitlab.com/appframework/capacity-web/compare/1.0.32...1.0.33) (2024-12-11)


### Bug Fixes

* **deps:** update dependency @vueuse/core to v12 ([693b81c](https://gitlab.com/appframework/capacity-web/commit/693b81c698f9138ddbb6c9f3b82b129380324b75))

## [1.0.32](https://gitlab.com/appframework/capacity-web/compare/1.0.31...1.0.32) (2024-12-11)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.26.11 ([3a829a6](https://gitlab.com/appframework/capacity-web/commit/3a829a6a9506fae58ef9060433ce8a1c1ed902f6))

## [1.0.31](https://gitlab.com/appframework/capacity-web/compare/1.0.30...1.0.31) (2024-12-06)


### Bug Fixes

* **deps:** update dependency @vueuse/core to v11.3.0 ([fc5abc1](https://gitlab.com/appframework/capacity-web/commit/fc5abc12c97baeedbc7fbfae7eb473897ab8282a))
* **deps:** update dependency vue-router to v4.5.0 ([321ab2c](https://gitlab.com/appframework/capacity-web/commit/321ab2cdb407a18a4c37ae500f0413ac7d56c7d7))

## [1.0.30](https://gitlab.com/appframework/capacity-web/compare/1.0.29...1.0.30) (2024-12-05)

## [1.0.29](https://gitlab.com/appframework/capacity-web/compare/1.0.28...1.0.29) (2024-12-04)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.26.8 ([43868fe](https://gitlab.com/appframework/capacity-web/commit/43868fe4e37594fe0b7c2021749f1f01bb068532))
* **deps:** update dependency chart.js to v4.4.7 ([61208da](https://gitlab.com/appframework/capacity-web/commit/61208da56d8c7b3a082d4017ae5344aa399ee636))

## [1.0.28](https://gitlab.com/appframework/capacity-web/compare/1.0.27...1.0.28) (2024-12-01)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.26.7 ([5980c73](https://gitlab.com/appframework/capacity-web/commit/5980c732d939527fa1626e48a66e56604b416c4a))
* **deps:** update dependency sortablejs to v1.15.6 ([a1c5465](https://gitlab.com/appframework/capacity-web/commit/a1c546504a97c080204c8b400820a86c594d56c2))
* **deps:** update dependency vue-i18n to v10.0.5 ([8187912](https://gitlab.com/appframework/capacity-web/commit/8187912ab0956a10c0491c3f4d35b72fa188734c))

## [1.0.27](https://gitlab.com/appframework/capacity-web/compare/1.0.26...1.0.27) (2024-11-28)

## [1.0.26](https://gitlab.com/appframework/capacity-web/compare/1.0.25...1.0.26) (2024-11-27)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.26.5 ([3ac8315](https://gitlab.com/appframework/capacity-web/commit/3ac83150fcd6eaf75b2a03f03ea742e4be2bbe8b))
* **deps:** update dependency org.apache.logging.log4j:log4j-to-slf4j to v2.24.2 ([aca183d](https://gitlab.com/appframework/capacity-web/commit/aca183d1bed10056c10e3525b2125cdf366f4f13))
* **deps:** update dependency primelocale to v1.2.2 ([53d040f](https://gitlab.com/appframework/capacity-web/commit/53d040f5c19f7b8b42a61ea009e7ea0500d7e023))
* **deps:** update dependency sortablejs to v1.15.4 ([14a708c](https://gitlab.com/appframework/capacity-web/commit/14a708c3d12fb3ac5e6e91a7c67c6c92792f56b1))

## [1.0.25](https://gitlab.com/appframework/capacity-web/compare/1.0.24...1.0.25) (2024-11-20)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to ^0.26.0 ([9880179](https://gitlab.com/appframework/capacity-web/commit/988017945033273985752390e507c54e2900e420))
* **deps:** update dependency primelocale to v1.2.0 ([71b8573](https://gitlab.com/appframework/capacity-web/commit/71b85731c3b32045a7d62c709dbd771d8b8f546e))
* **deps:** update dependency vue to v3.5.13 ([589292b](https://gitlab.com/appframework/capacity-web/commit/589292b4472f9b528130edda7d2013dafb44a813))

## [1.0.24](https://gitlab.com/appframework/capacity-web/compare/1.0.23...1.0.24) (2024-11-15)


### Bug Fixes

* **deps:** update dependency primelocale to v1.1.1 ([88ead4a](https://gitlab.com/appframework/capacity-web/commit/88ead4a1b1539b1f054eb1f5ec1a39fcdaef5b00))

## [1.0.23](https://gitlab.com/appframework/capacity-web/compare/1.0.22...1.0.23) (2024-11-14)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.25.14 ([f8fe0c9](https://gitlab.com/appframework/capacity-web/commit/f8fe0c9764c3aa5a35481c57557ef0e9fffc6117))

## [1.0.22](https://gitlab.com/appframework/capacity-web/compare/1.0.21...1.0.22) (2024-11-07)


### Bug Fixes

* **deps:** update dependency @vueuse/core to v11.2.0 ([9457f6b](https://gitlab.com/appframework/capacity-web/commit/9457f6be8d45b4beacf4d9af5d8d7bb1de9dd9f1))

## [1.0.21](https://gitlab.com/appframework/capacity-web/compare/1.0.20...1.0.21) (2024-11-06)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.25.13 ([61f6e21](https://gitlab.com/appframework/capacity-web/commit/61f6e217ddb22821d14311c4d697bc256c9dd8cf))
* **deps:** update dependency vue-chartjs to v5.3.2 ([5e13513](https://gitlab.com/appframework/capacity-web/commit/5e135134db60cbfb6683089f2117ac93f6faeaf7))

## [1.0.20](https://gitlab.com/appframework/capacity-web/compare/1.0.19...1.0.20) (2024-10-29)


### Bug Fixes

* **deps:** update dependency chart.js to v4.4.6 ([8042c91](https://gitlab.com/appframework/capacity-web/commit/8042c912908de644c225d7e8751c5615eddf5f73))

## [1.0.19](https://gitlab.com/appframework/capacity-web/compare/1.0.18...1.0.19) (2024-10-29)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.25.10 ([6d72dcf](https://gitlab.com/appframework/capacity-web/commit/6d72dcfcbdd2c4013a5c7818dd33da5180678e6b))

## [1.0.18](https://gitlab.com/appframework/capacity-web/compare/1.0.17...1.0.18) (2024-10-25)

## [1.0.17](https://gitlab.com/appframework/capacity-web/compare/1.0.16...1.0.17) (2024-10-24)

## [1.0.16](https://gitlab.com/appframework/capacity-web/compare/1.0.15...1.0.16) (2024-10-24)

## [1.0.15](https://gitlab.com/appframework/capacity-web/compare/1.0.14...1.0.15) (2024-10-23)

## [1.0.14](https://gitlab.com/appframework/capacity-web/compare/1.0.13...1.0.14) (2024-10-22)

## [1.0.13](https://gitlab.com/appframework/capacity-web/compare/1.0.12...1.0.13) (2024-10-22)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.25.7 ([f1eeafa](https://gitlab.com/appframework/capacity-web/commit/f1eeafa92d6297ca7e865752a6e996a51c5d255c))
* **deps:** update dependency primelocale to v1.0.4 ([7e0f2ec](https://gitlab.com/appframework/capacity-web/commit/7e0f2ecd87ce295cf588327261842693b83d300d))

## [1.0.12](https://gitlab.com/appframework/capacity-web/compare/1.0.11...1.0.12) (2024-10-21)

## [1.0.11](https://gitlab.com/appframework/capacity-web/compare/1.0.10...1.0.11) (2024-10-18)

## [1.0.10](https://gitlab.com/appframework/capacity-web/compare/1.0.9...1.0.10) (2024-10-15)


### Bug Fixes

* **deps:** update dependency chart.js to v4.4.5 ([bc8d35f](https://gitlab.com/appframework/capacity-web/commit/bc8d35f4b0d8862a5f5ffe98805c4c7b469d5c42))

## [1.0.9](https://gitlab.com/appframework/capacity-web/compare/1.0.8...1.0.9) (2024-10-14)

## [1.0.8](https://gitlab.com/appframework/capacity-web/compare/1.0.7...1.0.8) (2024-10-13)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to ^0.25.0 ([cb2ab05](https://gitlab.com/appframework/capacity-web/commit/cb2ab0541619cf0a466f4b5c456846f34ceae6cb))
* **deps:** update dependency primevue to v4.1.0 ([b452dff](https://gitlab.com/appframework/capacity-web/commit/b452dff3378c5089a45b451383bec2d29ff386e3))

## [1.0.7](https://gitlab.com/appframework/capacity-web/compare/v1.0.6...1.0.7) (2024-10-11)


### Bug Fixes

* **deps:** update dependency vue-i18n to v10.0.4 ([decb54e](https://gitlab.com/appframework/capacity-web/commit/decb54e9c1b9024976d65f3361a5336f076883d3))

## [1.0.6](https://gitlab.com/appframework/capacity-web/compare/v1.0.5...v1.0.6) (2024-10-09)

## [1.0.5](https://gitlab.com/appframework/capacity-web/compare/v1.0.4...v1.0.5) (2024-10-05)

## [1.0.4](https://gitlab.com/appframework/capacity-web/compare/v1.0.3...v1.0.4) (2024-10-05)

## [1.0.3](https://gitlab.com/appframework/capacity-web/compare/v1.0.2...v1.0.3) (2024-10-05)

## [1.0.2](https://gitlab.com/appframework/capacity-web/compare/v1.0.1...v1.0.2) (2024-10-05)

## [1.0.1](https://gitlab.com/appframework/capacity-web/compare/v1.0.0...v1.0.1) (2024-10-04)

# 1.0.0 (2024-10-04)


### Bug Fixes

* Add SPA routing on production builds ([e3919bf](https://gitlab.com/appframework/capacity-web/commit/e3919bf774afb60453bf726e98c40321ea8d8f9e))
* Add two missing cascading deletions ([5e013d6](https://gitlab.com/appframework/capacity-web/commit/5e013d67bd3c02ac1766ef9876970215750b688d))
* Copy&pasto ([301314f](https://gitlab.com/appframework/capacity-web/commit/301314f9ac1f33002e858eecb96f9708f18c0d79))
* **deps:** update dependency bootstrap-vue-next to v0.24.23 ([6f6bc1e](https://gitlab.com/appframework/capacity-web/commit/6f6bc1e7e4cbfa4379727cb2365f8a1b0f9cdcf1))
* **deps:** update dependency io.quarkiverse.quinoa:quarkus-quinoa to v2.4.9 ([520cb99](https://gitlab.com/appframework/capacity-web/commit/520cb99dba471f5feb580b390448eadc70cd1e99))
* **deps:** update dependency org.apache.logging.log4j:log4j-to-slf4j to v2.24.1 ([63e4f21](https://gitlab.com/appframework/capacity-web/commit/63e4f2164716d0fa32b44a58852f37b427658745))
* **deps:** update dependency org.apache.poi:poi to v5.3.0 ([ea9b546](https://gitlab.com/appframework/capacity-web/commit/ea9b546f251144f736e90612b87e50b6ff22399d))
* **deps:** update dependency org.apache.poi:poi-ooxml to v5.3.0 ([3dad15c](https://gitlab.com/appframework/capacity-web/commit/3dad15c623524b44d942d422a77ae8ec49211628))
* **deps:** update dependency org.webjars.npm:angular-translate to v2.19.1 ([c647899](https://gitlab.com/appframework/capacity-web/commit/c647899af4f9c32a949bb7ce2b10b5f8c1cc46b5))
* **deps:** update dependency org.webjars.npm:angular-translate-loader-static-files to v2.19.1 ([b813a10](https://gitlab.com/appframework/capacity-web/commit/b813a10726c6cfeac024ecd3ad7a22bdf6102991))
* **deps:** update dependency org.webjars.npm:bootstrap to v5.3.3 ([27f6399](https://gitlab.com/appframework/capacity-web/commit/27f639993d0bce8df46e22314f2837c993856313))
* **deps:** update dependency org.webjars.npm:bootstrap-slider to v11 ([8a7bbf4](https://gitlab.com/appframework/capacity-web/commit/8a7bbf4eb18d045fd1636744dfbca71b5029515e))
* **deps:** update dependency org.webjars.npm:dayjs to v1.11.11 ([4ae8a6d](https://gitlab.com/appframework/capacity-web/commit/4ae8a6df0906e5bba5716d894465f6338c15500e))
* **deps:** update dependency org.webjars.npm:dayjs to v1.11.12 ([c0a57de](https://gitlab.com/appframework/capacity-web/commit/c0a57de97e347a9e856923862b8a9835ff1b947f))
* **deps:** update dependency org.webjars.npm:dayjs to v1.11.13 ([be63e9b](https://gitlab.com/appframework/capacity-web/commit/be63e9b0ab1f6a9a366dca8f43ff64a2afba9696))
* **deps:** update dependency org.webjars.npm:fullcalendar to v5.11.3 ([93529e5](https://gitlab.com/appframework/capacity-web/commit/93529e5a1791b4c2306cc70c128265ff146ed6df))
* **deps:** update dependency org.webjars.npm:fullcalendar to v5.11.5 ([9a3f8b5](https://gitlab.com/appframework/capacity-web/commit/9a3f8b50059a814629af3824a5a43416ae97c18c))
* **deps:** update dependency org.webjars.npm:jquery to v3.7.1 ([458cd71](https://gitlab.com/appframework/capacity-web/commit/458cd71c95bdc4403bffff78220718cfed80c0a4))
* Don't drop existing abilities during import ([53575af](https://gitlab.com/appframework/capacity-web/commit/53575af8d44d73e115589380950e608f248e0701))
* Fix playwright based tests ([dcb86e7](https://gitlab.com/appframework/capacity-web/commit/dcb86e7e167534bf467ee9a6d7361e8b02121250))
* Fix TODO regarding REST failures ([0a9c1b4](https://gitlab.com/appframework/capacity-web/commit/0a9c1b49bd0ba7bc78aaec8909af877ba331ad33))
* Properly restore previous configuration ([e1a59db](https://gitlab.com/appframework/capacity-web/commit/e1a59dbbe9af64152e1bbe83e30f2f900e1aaf7d))
* Repair reload after changing homepage ([36973d5](https://gitlab.com/appframework/capacity-web/commit/36973d5ff616dde2b8b4265228a99ad9cd9ab391))
* Rework requirement import ([1b9215e](https://gitlab.com/appframework/capacity-web/commit/1b9215e0cc5f6ee06295aa22b90a33ce1db92284))
* Switch null-check to actual field ([f3dc63b](https://gitlab.com/appframework/capacity-web/commit/f3dc63b29dc39fcf776d38a83bc68ea95d7a9898))
* Use correct scripts ([0b636f6](https://gitlab.com/appframework/capacity-web/commit/0b636f65a580ec1ba8e148bef08dec6da9e75438))


### Features

* Add color history support ([0712536](https://gitlab.com/appframework/capacity-web/commit/071253673748c633df9d4d2a7fb3774263c75f6b))
* Add employee import ([f59925a](https://gitlab.com/appframework/capacity-web/commit/f59925a8c20f8169b208cedf6b6d4c42614c8035))
* Add foreground-color support ([658121b](https://gitlab.com/appframework/capacity-web/commit/658121b98628812736a5516c422be05889a4bda5))
* Add label precedence setting ([262a237](https://gitlab.com/appframework/capacity-web/commit/262a23722b9387fe02e5abc1f368243fff7e3106))
* Add support for FTE import ([d0a2897](https://gitlab.com/appframework/capacity-web/commit/d0a2897a84b1d542477fea3290d3ca37a635863b))
* Allow importing employees multiple times ([d3c9dd0](https://gitlab.com/appframework/capacity-web/commit/d3c9dd0b1efbb7f5e38d19dfe47bca1cb1ada74e))
* **ci:** Add default .gitlab-ci.yml ([3323bd7](https://gitlab.com/appframework/capacity-web/commit/3323bd7657587870ed5c40f3d6a060ef8b8e695d))
* **ci:** Use default quarkus pipeline ([baf07f2](https://gitlab.com/appframework/capacity-web/commit/baf07f2cf492e398f08c9164ff3325f444786f05))
* Create favicon.ico ([6eb72c6](https://gitlab.com/appframework/capacity-web/commit/6eb72c630b4d1cfbf780d7f72b846896ba33dbda))
* Implement merging labels ([b50df3a](https://gitlab.com/appframework/capacity-web/commit/b50df3af59a1af0b9b3572ced2e095739ea69e25))
* Introduce common ListComponent for tables ([0d89de9](https://gitlab.com/appframework/capacity-web/commit/0d89de9d618a6df00ffb6a717149c1c87d0c65c1))
* Replace angularjs UI by Vue 3 ([cdb71f3](https://gitlab.com/appframework/capacity-web/commit/cdb71f31aa9391d2edde377303f1b8ac2e491b3b))
* Update to latest appframework ([c2d21d0](https://gitlab.com/appframework/capacity-web/commit/c2d21d0e52649e422aedf075b3a9ddb8e6bd0652))
* Use merge dialog component ([89dfed3](https://gitlab.com/appframework/capacity-web/commit/89dfed31d814d929c2610b4394e95941abbc7851))
